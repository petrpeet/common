package cz.kb.bi.domain.dto.settingstore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Stored setting entity.
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoredSettingDTO {
    private String userName;
    private String domainName;
    private String domainItemId;
    private Integer order;
    private String applicationName;
    /**
     * Should never be set manualy.
     */
    private LocalDateTime updatedAt;
    private String jsonValue;
}
