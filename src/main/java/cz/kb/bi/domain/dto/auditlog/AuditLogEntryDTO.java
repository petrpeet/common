package cz.kb.bi.domain.dto.auditlog;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

/**
 * Data Transfer Object for acceptiong audit log informations
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuditLogEntryDTO {

    /**
     * Name of the application
     */
    private String applicationName;
    /**
     * Date of access done in application
     */
    private ZonedDateTime accessDate;
    /**
     * Name of user
     */
    private String userName;
    /**
     * Name of class which was used
     */
    private String className;
    /**
     * Name of method which was used
     */
    private String methodName;
    /**
     * Name of parameters used in method
     */
    private String parametersValue;

    /**
     * Name of bean in Spring context
     */
    private String beanName;

}
