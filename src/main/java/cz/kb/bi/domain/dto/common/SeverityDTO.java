package cz.kb.bi.domain.dto.common;


public enum SeverityDTO {
    SUCCESS(0), ERROR(3), FATAL(4), INFO(1), WARN(2);

    private int level;

    SeverityDTO(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
