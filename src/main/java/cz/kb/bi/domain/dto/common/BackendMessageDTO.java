package cz.kb.bi.domain.dto.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BackendMessageDTO {
    private String summary;
    private String detail;
    private List<String> additionalDetails = new ArrayList<>();
    private SeverityDTO severity;
}