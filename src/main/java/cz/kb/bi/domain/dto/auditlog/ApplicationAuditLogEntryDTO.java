package cz.kb.bi.domain.dto.auditlog;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

/**
 * Data transfer object used for getting data from auditlog
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationAuditLogEntryDTO {
    /**
     * Name of the application
     */
    @NotNull
    private String applicationName;
    /**
     * Date of log entry
     */
    @NotNull
    private ZonedDateTime logDate;
}
