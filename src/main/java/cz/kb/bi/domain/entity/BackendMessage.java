package cz.kb.bi.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;

@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"summary", "detail", "severity"})
public class BackendMessage {
    @NonNull
    private String summary;
    private String detail;

    @Builder.Default
    private List<String> additionalDetails = Collections.emptyList();

    @NonNull
    private Severity severity;
    @Builder.Default
    private Integer order = 1;

    public BackendMessage(BackendMessage message) {
        this.summary = message.getSummary();
        this.detail = message.getDetail();
        this.severity = this.getSeverity();
    }

    public static BackendMessage cloneMessage(BackendMessage message){
        BackendMessage messageNew = new BackendMessage();
        messageNew.setDetail(message.getDetail());
        messageNew.setSummary(message.getSummary());
        messageNew.setSeverity(message.getSeverity());
        return messageNew;
    }
}