package cz.kb.bi.domain.entity.sqlTransform;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Kind of activity for which will the analysis to be used (Project/SME)
 */
@AllArgsConstructor
@Getter
public enum TransformationForEnum {

    PROJECT("PROJECT"),

    SME("SME"),

    READ("READ");

    private String value;
}
