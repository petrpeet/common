package cz.kb.bi.domain.entity.common;

import lombok.Data;

/**
 * Pair used some stream operations
 *
 * @param <T> first type of tuple
 * @param <E> second type of tuple
 */
@Data
public class Tuple<T, E> {

    /**
     * First value of tuple
     */
    private final T first;
    /**
     * Second value of tuple
     */
    private final E second;
}