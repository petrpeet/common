package cz.kb.bi.domain.entity.sqlTransform;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Setting of environment
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnvironmentSetting {

    private String prestage;
    private String stage;
    private String poststage;
    private String tmp;
    private String errors;
    private String aux;
    private String core;
    private String mart;
    private String defaults;
}
