package cz.kb.bi.domain.entity.sqlTransform;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Final environment for the transformed scripts (PROD / DEV)
 */
@AllArgsConstructor
@Getter
public enum EnvironmentEnum {
    PROD("PROD"),

    DEV("DEV");

    private String value;
}
