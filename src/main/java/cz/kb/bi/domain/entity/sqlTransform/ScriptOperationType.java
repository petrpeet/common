package cz.kb.bi.domain.entity.sqlTransform;

public enum ScriptOperationType {
    REWRITE, ANALYZE
}
