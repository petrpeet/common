package cz.kb.bi.domain.entity.sqlTransform;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Object in database
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"databaseName", "tableName"})
public class DatabaseTable {

    private String databaseName;
    private String tableName;

    @Override
    public String toString() {
        return databaseName + "." + tableName;
    }
}
