package cz.kb.bi.domain.entity.sqlTransform;

import cz.kb.bi.domain.entity.BackendMessage;
import cz.kb.bi.domain.entity.common.TextFile;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * Wrapper for information about script Transformation.
 */
@Data
@EqualsAndHashCode(of = {"rules", "originalScript", "newScript", "scriptObjects", "projectSuffix", "transformationFor", "environmentEnum", "operationType", "transformMessages", "environmentSetting", "mergeFiles"})
public class TransformationEvent {

    private List<String> rules;
    private List<TextFile> originalScript;
    private List<TextFile> newScript;
    private List<DatabaseTable> scriptObjects;
    private String projectSuffix;
    private TransformationForEnum transformationFor;
    private EnvironmentEnum environmentEnum;
    private ScriptOperationType operationType;
    private List<BackendMessage> transformMessages;
    private EnvironmentSetting environmentSetting;
    private boolean mergeFiles;

    public TransformationEvent() {
        this.rules = new ArrayList<>();
        this.originalScript = new ArrayList<>();
        this.newScript = new ArrayList<>();
        this.scriptObjects = new ArrayList<>();
        this.transformMessages = new ArrayList<>();
    }

    /**
     * Constructor for clone
     *
     * @param event event to clone
     */
    public TransformationEvent(TransformationEvent event) {
        this.rules = event.getRules().stream().collect(toList());
        this.originalScript = event.getOriginalScript()
                .stream()
                .map(i -> new TextFile(i.getScriptName(), i.getScriptContent()))
                .collect(Collectors.toList());
        this.newScript = event.getNewScript()
                .stream()
                .map(i -> new TextFile(i.getScriptName(), i.getScriptContent()))
                .collect(Collectors.toList());
        this.scriptObjects = event.getScriptObjects().stream()
                .map(i -> new DatabaseTable(i.getDatabaseName(), i.getTableName()))
                .collect(Collectors.toList());
        this.projectSuffix = event.getProjectSuffix();
        this.transformationFor = event.getTransformationFor();
        this.environmentEnum = event.getEnvironmentEnum();
        this.operationType = event.getOperationType();
        this.transformMessages = event.getTransformMessages().stream()
                .map(BackendMessage::cloneMessage)
                .collect(Collectors.toList());
        this.environmentSetting = event.getEnvironmentSetting();
        this.mergeFiles = event.isMergeFiles();
    }


}
