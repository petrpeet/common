package cz.kb.bi.domain.entity;


public enum Severity {
    SUCCESS(0), ERROR(3), FATAL(4), INFO(1), WARN(2), DEBUG(5);

    private int level;

    Severity(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
