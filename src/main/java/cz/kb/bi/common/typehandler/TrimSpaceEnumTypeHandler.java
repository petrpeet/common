package cz.kb.bi.common.typehandler;

import org.apache.ibatis.type.EnumTypeHandler;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * TrimSpaceEnumTypeHandler is extension of {@link EnumTypeHandler} that trims spaces before calling value
 * of on given enum.
 */
public class TrimSpaceEnumTypeHandler<E extends Enum<E>> extends EnumTypeHandler<E> {

    private Class<E> type;

    public TrimSpaceEnumTypeHandler(Class<E> type) {
        super(type);
        this.type = type;
    }

    /**
     * Overrides default nullable result getter due to string from result set having excessive spaces.
     * Those are trimmed and value of {@link E} is called.
     *
     * @param rs         result set
     * @param columnName col name to convert
     * @return null if result is does not contain string | {@link E}
     * @throws SQLException when accessing result set fails
     */
    @Override
    public E getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String s = rs.getString(columnName);
        return trimValueOf(s);
    }

    /**
     * Returns nullable result for index
     *
     * @param rs          result set
     * @param columnIndex index of col
     * @return status
     * @throws SQLException when accessing result set fails
     */
    @Override
    public E getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String s = rs.getString(columnIndex);
        return trimValueOf(s);
    }

    /**
     * Returns nullable result for index
     *
     * @param cs          result set
     * @param columnIndex index of col
     * @return status
     * @throws SQLException when accessing result set fails
     */
    @Override
    public E getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String s = cs.getString(columnIndex);
        return trimValueOf(s);
    }

    /**
     * Gets
     *
     * @param s string to use for value of
     * @return null or enum
     */
    private E trimValueOf(String s) {
        return s == null ? null : Enum.valueOf(type, s.trim());
    }
}
