package cz.kb.bi.common.typehandler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * TypeHandler for dealing with char-type cols which often result in trailing whitespace characters.
 */
public class CharTypeHandler extends BaseTypeHandler<String> {

    /**
     * Trims string from spaces or returns null
     *
     * @param str string
     * @return trimmed string or null
     */
    private static String trimmedStringOrNull(String str) {
        return str == null ? null : str.trim();
    }

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, String parameter,
                                    JdbcType jdbcType) throws SQLException {
        ps.setString(i, parameter.trim());
    }

    @Override
    public String getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return trimmedStringOrNull(rs.getString(columnName));
    }

    @Override
    public String getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return trimmedStringOrNull(rs.getString(columnIndex));
    }

    @Override
    public String getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return trimmedStringOrNull(cs.getString(columnIndex));
    }
}
