package cz.kb.bi.common.typehandler;

import cz.kb.bi.common.utils.PathUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Type handler for converting string to {@link Path}. Null is returned if string representation of
 * path is invalid.
 */
@Slf4j
public class PathTypeHandler extends BaseTypeHandler<Path> {

    /**
     * Converts string to path or returns null.
     *
     * @param pathStr string representation of path
     * @return {@link Path} of string or null
     */
    private static Path convertToPathOrNull(String pathStr) {
        if (pathStr != null) {
            return Paths.get(pathStr);
        }

        log.debug("Path string is null");

        return null;
    }

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Path parameter,
                                    JdbcType jdbcType) throws SQLException {

        String pathStr = PathUtils.pathToDisplay(parameter);

        ps.setString(i, pathStr);
    }

    @Override
    public Path getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return convertToPathOrNull(rs.getString(columnName));
    }

    @Override
    public Path getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return convertToPathOrNull(rs.getString(columnIndex));
    }

    @Override
    public Path getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return convertToPathOrNull(cs.getString(columnIndex));
    }
}
