package cz.kb.bi.common.config;

import com.fasterxml.classmate.TypeResolver;
import cz.kb.bi.ApplicationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRules;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Date;

/**
 * Configuration used for swagger API documentation
 */
public abstract class AbstractCommonSwaggerConfiguration {
    @Autowired
    protected ApplicationProperties applicationProperties;
    @Autowired
    private TypeResolver typeResolver;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(basePackage()))
                .build()
                .forCodeGeneration(true)
                .directModelSubstitute(LocalDate.class,
                        Date.class)
                .directModelSubstitute(LocalDateTime.class,
                        Date.class)
                .directModelSubstitute(ZonedDateTime.class,
                        Date.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(
                        AlternateTypeRules.newRule(typeResolver.resolve(DeferredResult.class,
                                typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                                typeResolver.resolve(WildcardType.class)))
                .consumes(Collections.singleton("application/json"))
                .produces(Collections.singleton("application/json"))
                .apiInfo(apiInfo());
    }

    /**
     * Base package, which should be scanned
     *
     * @return base package for swagger
     */
    protected abstract String basePackage();

    /**
     * Add module specific information about module description
     *
     * @param apiInfoBuilder builder used for api documentation
     */
    protected abstract void addModuleInformation(ApiInfoBuilder apiInfoBuilder);

    /**
     * API information about current application
     *
     * @return current {@link ApiInfo} for application
     */
    private ApiInfo apiInfo() {
        final ApiInfoBuilder builder = new ApiInfoBuilder()
                .version(String.format("%s-%s", applicationProperties.getVersion(), applicationProperties.getBuildNumber()))
                .contact(new Contact("Lukas Kosina", null, "lukas_kosina@kb.cz"))
                .license("All rights reserved to KB");
        addModuleInformation(builder);
        return builder.build();
    }
}