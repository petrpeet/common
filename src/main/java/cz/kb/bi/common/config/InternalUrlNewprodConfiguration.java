package cz.kb.bi.common.config;

import cz.kb.bi.common.domain.entity.InternalUrl;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;


/**
 * Plain implementation of {@link InternalUrl} that loads properties from main file for newprod profile.
 */
@Configuration
@PropertySource("classpath:internal-url.properties")
@ConfigurationProperties
@Profile("newprod")
class InternalUrlNewprodConfiguration extends InternalUrl {
}
