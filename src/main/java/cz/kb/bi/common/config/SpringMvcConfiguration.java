package cz.kb.bi.common.config;

import cz.kb.bi.common.converter.LocalDateConverter;
import cz.kb.bi.common.converter.LocalDateTimeConverter;
import cz.kb.bi.common.converter.ZonedDateTimeConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Extension to {@link WebMvcConfigurerAdapter} to add new instance of formatter:
 * <ul>
 * <li>{@link LocalDateConverter}</li>
 * <li>{@link LocalDateTimeConverter}</li>
 * </ul>
 */
@Configuration
public class SpringMvcConfiguration extends WebMvcConfigurerAdapter {

    /**
     * Add custom formatter into {@link FormatterRegistry}
     * <ul>
     * <li>{@link LocalDateConverter}</li>
     * <li>{@link LocalDateTimeConverter}</li>
     * </ul>
     *
     * @param registry registry of all Spring formatter
     */
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new LocalDateConverter());
        registry.addConverter(new LocalDateTimeConverter());
        registry.addConverter(new ZonedDateTimeConverter());
    }

    /**
     * Disable path extension for BI API services
     *
     * @param configurer content negotiation for mvc
     */
    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.favorPathExtension(false);
    }

    /**
     * Set {@link PathMatchConfigurer#setUseSuffixPatternMatch(Boolean)} to true value to use
     * suffixes in urls as default part of parameter in URL
     *
     * @param configurer configuration of matching path parameters
     */
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.setUseSuffixPatternMatch(false);
    }
}
