package cz.kb.bi.common.config;

import cz.kb.bi.common.domain.entity.InternalUrl;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


/**
 * Plain implementation of {@link InternalUrl} that loads properties from dev file.
 * For instances when spring profile is not set (IT tests).
 */
@Configuration
@PropertySource("classpath:internal-url.properties")
@PropertySource("classpath:internal-url-dev.properties")
@ConfigurationProperties
@Conditional(TestRunnerProfileCondition.class)
class InternalUrlTestRunnerConfiguration extends InternalUrl {
}
