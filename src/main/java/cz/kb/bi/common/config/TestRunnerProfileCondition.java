package cz.kb.bi.common.config;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Arrays;
import java.util.List;


/**
 * Specifies condition to be used in {@link org.springframework.context.annotation.Conditional} annotation.
 * Condition is true when no profile is specified (eg. when runnign tests).
 */
public class TestRunnerProfileCondition implements Condition {

    private static final List<String> APPLICATION_PROFILES  = Arrays.asList("test", "prod", "dev", "newprod");

    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata annotatedTypeMetadata) {
        boolean result = false;
        if (conditionContext.getEnvironment() != null) {
            String[] activeProfiles = conditionContext.getEnvironment().getActiveProfiles();
            if (activeProfiles.length == 0)
                result = true;
            else
                result = Arrays.stream(activeProfiles).noneMatch(this::predicate);
        }
        return result;
    }

    private boolean predicate(String activeProfile) {
        return APPLICATION_PROFILES.contains(activeProfile);
    }
}
