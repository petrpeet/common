package cz.kb.bi.common.config;

import cz.kb.bi.common.domain.entity.InternalUrl;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;


/**
 * Plain implementation of {@link InternalUrl} that loads properties from dev file.
 */
@Configuration
@PropertySource("classpath:internal-url.properties")
@PropertySource("classpath:internal-url-dev.properties")
@ConfigurationProperties
@Profile("dev")
class InternalUrlDevConfiguration extends InternalUrl {
}
