package cz.kb.bi.common.config;

import cz.kb.bi.ApplicationProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * Common application security configuration
 */
@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Slf4j
@Profile("!local")
public class CommonSecurityConfiguration extends ResourceServerConfigurerAdapter {

    private static final String ADMIN_ROLE = "ADMIN_ROLE";

    @Autowired
    private ApplicationProperties applicationProperties;

    /**
     * Common configuration for BI API microservices.
     * <ul>
     * <li>Public API and swagger related endpoint should be visible from everywhere.</li>
     * <li>All request should come from address defined by {@link ApplicationProperties#getAllowOrigin()} which is address of proxy.</li>
     * </ul>
     *
     * @param http http security from spring boot
     * @throws Exception if any problem occurs during {@link HttpSecurity#authorizeRequests()}
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        final String allowOrigin = applicationProperties.getAllowOrigin();
        log.info("API started with restriction to ipAddress:" + allowOrigin);
        http.authorizeRequests()
                // Everything in pub is publish so no authentication is needed
                .antMatchers("/pub/**")
                .hasIpAddress(allowOrigin)
                // Everything in pub is publish so no authentication is needed
                .antMatchers("/internal/**")
                .hasIpAddress(allowOrigin)
                // Spring cloud health monitoring
                .antMatchers("/health", "/trace", "/metrics", "/metrics/*", "/info")
                .permitAll()
                .antMatchers("/pause", "/restart", "/stop", "/start")
                .hasRole(ADMIN_ROLE)
                // Everything in swagger is publish so no authentication is needed
                .antMatchers("/swagger-ui.html", "/webjars/*", "/swagger-resources"
                        , "/swagger-resources/**"
                        , "/v2/*", "/configuration/*")
                .permitAll()
                // Error page is publish so no authentication is needed
                .antMatchers("/error")
                .permitAll()
                // Everything else should be at least authenticated. Security is add via Annotations in Controllers
                .anyRequest()
                .access(String.format("isAuthenticated() and hasIpAddress('%s')", allowOrigin))
        ;
    }

}
