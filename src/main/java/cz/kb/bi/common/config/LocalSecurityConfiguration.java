package cz.kb.bi.common.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * Common application security configuration
 */
@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Slf4j
@Profile("local")
public class LocalSecurityConfiguration extends ResourceServerConfigurerAdapter {

    /**
     * Default user used for local profile
     */
    public static final String DEFAULT_LOCAL_USER = "fenix";

    /**
     * Default user used for local profile
     */
    public static final String[] DEFAULT_LOCAL_ROLES = {"ROLE_AI_USERS_4800", "ROLE_AI_USERS_4800_DIC"};

    /**
     * Start API in local mode where every one can access everything.
     *
     * @param http http security from spring boot
     * @throws Exception if any problem occurs during {@link HttpSecurity#authorizeRequests()}
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        log.info("API started in local mode with no restriction");
        http.authorizeRequests()
                .anyRequest().permitAll()
                .and()
                .anonymous()
                .key(DEFAULT_LOCAL_USER)
                .principal(DEFAULT_LOCAL_USER)
                .authorities(DEFAULT_LOCAL_ROLES)
        ;
    }

}
