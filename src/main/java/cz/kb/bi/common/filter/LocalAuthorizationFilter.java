package cz.kb.bi.common.filter;

import cz.kb.bi.common.config.LocalSecurityConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.context.annotation.Profile;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.stream.Collectors;


/**
 * Filter used for removing Authorized header from request so the applications
 * does not have to remove them from requests
 */
@Profile("local")
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class LocalAuthorizationFilter implements Filter {

    /**
     * No settings needed.
     *
     * @param filterConfig configuration for filter
     */
    @Override
    public void init(FilterConfig filterConfig) {
        // Nothing to do
    }

    /**
     * Create new authentication {@link java.security.Principal} in security using {@link #createDefaultSecurityUser()}
     * and then remove the 'Authorization' header from request using {@link HeaderModificationRequestWrapper}.
     *
     * @param servletRequest  original request that is wrapper with {@link HeaderModificationRequestWrapper}
     * @param servletResponse response sent to server. It is not modified
     * @param filterChain     filter used for chaining for more data
     * @throws IOException      never thrown in current method. Only filter can throws the exception
     * @throws ServletException never thrown in current method. Only filter can throws the exception
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        createDefaultSecurityUser();
        filterChain.doFilter(createWrapperInRequest(servletRequest), servletResponse);
    }

    /**
     * Create new authorization {@link java.security.Principal} with following attributes:
     * <ul>
     * <li>username:fenix</li>
     * <li>credential:fenix</li>
     * <li>roles:ROLE_AI_USERS_4800,ROLE_AI_USERS_4800_DIC</li>
     * </ul>
     */
    private static void createDefaultSecurityUser() {
        SecurityContext context =
                SecurityContextHolder.getContext();
        context.setAuthentication(
                new TestingAuthenticationToken(
                        LocalSecurityConfiguration.DEFAULT_LOCAL_USER,
                        LocalSecurityConfiguration.DEFAULT_LOCAL_USER,
                        LocalSecurityConfiguration.DEFAULT_LOCAL_ROLES
                )
        );
    }

    /**
     * Wrap the origin request in parameter into {@link HeaderModificationRequestWrapper} with header
     * name to be removed set to 'Authorization'
     *
     * @param servletRequest original servlet request
     * @return new wrapped request
     */
    private HttpServletRequest createWrapperInRequest(ServletRequest servletRequest) {
        return new HeaderModificationRequestWrapper("Authorization", (HttpServletRequest) servletRequest);
    }

    /**
     * No settings needed.
     */
    @Override
    public void destroy() {
        // Nothing to do
    }

    /**
     * Wrapper used for removing header to the rest of filters
     */
    private class HeaderModificationRequestWrapper extends HttpServletRequestWrapper {
        /**
         * Name of the header to be removed from other filters
         */
        private final String headerName;

        /**
         * Create new instance of {@link HeaderModificationRequestWrapper} with set values
         *
         * @param headerName name of the header to be removed from other filters
         * @param request    original request to be wrapped
         */
        HeaderModificationRequestWrapper(String headerName, HttpServletRequest request) {
            super(request);
            this.headerName = Validate.notBlank(headerName, "Header name cannot be blank");
        }

        /**
         * If the name of the parameter 'inputHeaderName' is equal ignore case with
         * than return true. Otherwise return false.
         *
         * @param inputHeaderName header to be tested
         * @return true if inputHeaderName is equal to set original headerName
         */
        private boolean shouldBeRemoved(String inputHeaderName) {
            return StringUtils.equalsIgnoreCase(inputHeaderName, headerName);
        }

        /**
         * Opposite of {@link #shouldBeRemoved(String)}
         *
         * @param inputHeaderName header to be tested
         * @return true if inputHeaderName is not equal to set original headerName
         */
        private boolean shouldRemain(String inputHeaderName) {
            return !shouldBeRemoved(inputHeaderName);
        }

        /**
         * If the name of header returns true to {@link #shouldBeRemoved(String)} then
         * return 0 value. Otherwise return value of parent.
         *
         * @param name name of header
         * @return date representation in long
         */
        @Override
        public long getDateHeader(String name) {
            return shouldBeRemoved(name) ? 0 : super.getDateHeader(name);
        }

        /**
         * If the name of header returns true to {@link #shouldBeRemoved(String)} then
         * return null value. Otherwise return value of parent.
         *
         * @param name name of header
         * @return String value of header
         */
        @Override
        public String getHeader(String name) {
            return shouldBeRemoved(name) ? null : super.getHeader(name);
        }

        /**
         * If the name of header returns true to {@link #shouldBeRemoved(String)} then
         * return empty enumeration. Otherwise return value of parent.
         *
         * @param name name of header
         * @return enumeration of values with given name
         */
        @Override
        public Enumeration<String> getHeaders(String name) {
            return shouldBeRemoved(name) ? Collections.emptyEnumeration() : super.getHeaders(name);

        }

        /**
         * Get all headers in the request except those, who has true result in method {@link #shouldBeRemoved(String)}
         *
         * @return name of all headers
         */
        @Override
        public Enumeration<String> getHeaderNames() {
            return Collections.enumeration(
                    Collections.list(super.getHeaderNames())
                            .stream()
                            .filter(this::shouldRemain)
                            .collect(
                                    Collectors.toList()
                            )
            );
        }

        /**
         * If the name of header returns true to {@link #shouldBeRemoved(String)} then
         * return 0 value. Otherwise return value of parent.
         *
         * @param name name of header
         * @return integer representation in long
         */
        @Override
        public int getIntHeader(String name) {
            return shouldBeRemoved(name) ? 0 : super.getIntHeader(name);
        }
    }
}
