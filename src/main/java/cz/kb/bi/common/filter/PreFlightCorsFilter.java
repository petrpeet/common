package cz.kb.bi.common.filter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter used do add Control Origin Request Source headers to get from all url for OPTIONS method
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class PreFlightCorsFilter implements Filter {

    /**
     * Add following header to request to be able use it from difference sources and return {@link HttpServletResponse#SC_OK}.
     * <ul>
     * <li>Access-Control-Allow-Methods</li>
     * <li>Access-Control-Max-Age</li>
     * <li>Access-Control-Allow-Headers</li>
     * </ul>
     * Do not sent requests into another filters.
     * <p>
     * The OPTIONS method is called automatically by Web browser and it is really hard to debug.
     *
     * @param response response to set http headers to
     */
    private static void processOptionsRequest(HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST,GET,DELETE,PUT");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "authorization, content-type," +
                "access-control-request-headers,access-control-request-method,accept,origin,authorization,x-requested-with");
        response.setStatus(HttpServletResponse.SC_OK);
    }

    /**
     * Check if the request method from request is sent as a pre flight mode or not.
     * If the method is sent in pre flight mode (method OPTIONS)
     *
     * @param req   request sent to server
     * @param res   response that will be send back to server
     * @param chain chain use for additional {@link Filter}
     * @throws IOException      throws only in {@link FilterChain#doFilter(ServletRequest, ServletResponse)}
     * @throws ServletException throws only in {@link FilterChain#doFilter(ServletRequest, ServletResponse)}
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest request = (HttpServletRequest) req;

        if (StringUtils.equalsIgnoreCase("OPTIONS", request.getMethod())) {
            processOptionsRequest(response);
        } else {
            chain.doFilter(req, res);
        }
    }

    /**
     * Empty method. No init is needed
     *
     * @param filterConfig filter configuration
     */
    @Override
    public void init(FilterConfig filterConfig) {
        // This method does nothing
    }

    /**
     * No destroy is needed.
     */
    @Override
    public void destroy() {
        // This method does nothing
    }
}

