package cz.kb.bi.common.domain.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 * Class for keeping url paths of workflow monitor
 */
@Getter
@Setter
public class InternalUrlWorkflowMonitor {
    /**
     * Formatter used for creating URL for
     */
    private static final DateTimeFormatter WORKFLOW_MONITOR_FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd/HH/mm");

    @NonNull
    private Map<String, String> workflowStatusEndpoints;

    /**
     * Returns the url for failed workflows for given endpoint name since the date and time defined in parameter
     * checkTime
     *
     * @param checkTime    time of check
     * @param endpointName name of endpoint
     * @return formatted url for IFPC monitor
     */
    public String getFormattedWorkflowStatus(LocalDateTime checkTime, String endpointName) {
        if (workflowStatusEndpoints.containsKey(endpointName)) {
            return workflowStatusEndpoints.get(endpointName) + checkTime.format(WORKFLOW_MONITOR_FORMATTER);
        } else {
            throw new IllegalArgumentException(String.format("Endpoint name %s does not exists.", endpointName));
        }
    }

}
