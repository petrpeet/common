package cz.kb.bi.common.domain.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * Class for keeping url paths of teradata service.
 */
@Getter
@Setter
public class InternalUrlTeradata {

    /**
     * Teradata user detail string format with one %s for username.
     */
    @NonNull
    private String userDetail;

    /**
     * Teradata user right string format with one %s for username.
     */
    @NonNull
    private String userRight;

    /**
     * Teradata url for checking if db exists with one %s for username.
     */
    @NonNull
    private String databaseExists;

    /**
     * Teradata warehouse objects.
     */
    @NonNull
    private String warehouseObject;

    /**
     * Teradata warehouse object.
     */
    @NonNull
    private String oneWarehouseObject;

    /**
     * Teradata objects connected with workflow.
     */
    @NonNull
    private String ddlWorkflow;

    /**
     * Teradata objects connected with script.
     */
    @NonNull
    private String ddlScript;

    /**
     * Teradata objects connected with table.
     */
    @NonNull
    private String ddlTable;

    /**
     * Teradata write objects connected with workflow.
     */
    @NonNull
    private String ddlWorkflowWrite;

    /**
     * Teradata write objects connected with script.
     */
    @NonNull
    private String ddlScriptWrite;

    /**
     * Information from cmdb database
     */
    @NonNull
    private String cmdbInfo;

    /**
     * Information from r_fenix_lab
     */
    @NonNull
    private String labInfo;

    /**
     * Information about workflow config item
     */
    @NonNull
    private String cmdbWorkflow;

    /**
     * Returns formatted user detail.
     *
     * @param user username
     * @return formatted url
     */
    public String getFormattedUserDetail(String user) {
        return String.format(userDetail, user);
    }

    /**
     * Returns formatted user right.
     *
     * @param user username
     * @return formatted url
     */
    public String getFormattedUserRight(String user) {
        return String.format(userRight, user);
    }

    /**
     * Creates formatted string for database exists url.
     *
     * @param databaseName name of db
     * @return formatted url
     */
    public String getFormattedDatabaseExists(String databaseName) {
        return String.format(databaseExists, databaseName);
    }

    /**
     * Returns formatted url warehouseObject.
     *
     * @param name warehouse object name
     * @return formatted url for warehouseObject
     */
    public String getFormattedWarehouseObject(String name) {
        return String.format(warehouseObject, name);
    }

    /**
     * Returns formatted url oneWarehouseObject.
     *
     * @param name warehouse object name
     * @return formatted url for oneWarehouseObject
     */
    public String getFormattedOneWarehouseObject(String name) {
        return String.format(oneWarehouseObject, name);
    }

    /**
     * Returns formatted url ddl detail.
     *
     * @param folder folder
     * @param name workflow name
     * @return formatted url for ddlWorkflow
     */
    public String getFormattedDdlWorkflow(String folder, String name) {
        return String.format(ddlWorkflow, folder, name);
    }

    /**
     * Returns formatted url ddl detail.
     *
     * @param command command name
     * @param script script name
     * @return formatted url for ddlScript
     */
    public String getFormattedDdlScript(String command, String script) {
        return String.format(ddlScript, command, script);
    }

    /**
     * Returns formatted url ddl detail.
     *
     * @param database database name
     * @param table table name
     * @return formatted url for ddlTable
     */
    public String getFormattedDdlTable(String database, String table) {
        return String.format(ddlTable, database, table);
    }

    /**
     * Returns formatted url ddl detail.
     *
     * @param folder folder
     * @param name workflow name
     * @return formatted url for ddlWorkflowWrite
     */
    public String getFormattedDdlWorkflowWrite(String folder, String name) {
        return String.format(ddlWorkflowWrite, folder, name);
    }

    /**
     * Returns formatted url ddl detail.
     *
     * @param command command name
     * @param script script name
     * @return formatted url for ddlScriptWrite
     */
    public String getFormattedDdlScriptWrite(String command, String script) {
        return String.format(ddlScriptWrite, command, script);
    }

    /**
     *  Returns formatted url cmdb workflow
     * @param workflowName name of workflow
     * @return formatted url cmdb workflow
     */
    public String getCmdbWorkflow(String workflowName) {
        return String.format(cmdbWorkflow, workflowName);
    }
}
