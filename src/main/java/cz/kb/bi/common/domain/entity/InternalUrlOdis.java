package cz.kb.bi.common.domain.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;

/**
 * Class for keeping url paths of data integration repository
 */
@Getter
@Setter
@Validated
public class InternalUrlOdis {

    /**
     * Endpoint for job run status}
     */
    @NonNull
    private String jobRunStatus;

    /**
     * Get formatted url for job run status
     *
     * @param workflowName name of the workflow to be formatted
     * @return formatted url
     */
    public String getFormattedJobRunStatus(String workflowName) {
        return String.format(jobRunStatus, workflowName);
    }
}
