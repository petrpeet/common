package cz.kb.bi.common.domain.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * Class for keeping url paths of workflow documentation service.
 */
@Getter
@Setter
public class InternalUrlWorkflowDocumentation {

    /**
     * Url for getting workflow documentations by deployment event key
     */
    @NonNull
    public String workflowDocEvent;
    /**
     * Url for getting valid workflow documentation detail
     */
    @NonNull
    private String workflowDocValid;
    /**
     * Url for manipulating with workflow documentation detail
     */
    @NonNull
    private String workflowDoc;
    /**
     * Url for deleting by deployment key
     */
    @NonNull
    private String workflowDocDelete;
    /**
     * Actives wf docs by wf documentation name and key
     */
    @NonNull
    private String workflowDocActivate;

    /**
     * Returns formatted workflow documentation detail.
     *
     * @param workflowName name of script
     * @return formatted url
     */
    public String getFormattedWorkflowDocValid(String workflowName) {
        return String.format(workflowDocValid, workflowName);
    }

    /**
     * Gets formatted workflow delete by key
     *
     * @param key key
     * @return formatted url
     */
    public String getFormattedWorkflowDocDeleteByKey(String key) {
        return String.format(workflowDocDelete, key);
    }

    /**
     * Gets formatted url for finding all wf docs by key
     *
     * @param key key of task
     * @return formatted url
     */
    public String getFormattedWorkflowDocFindByKey(String key) {
        return String.format(workflowDocEvent, key);
    }
}
