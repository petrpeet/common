package cz.kb.bi.common.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Configuration
@PropertySource("classpath:roles-config.properties")
@ConfigurationProperties
public class RolesConfig {

    /**
     * List with roles for operation support members on ldap server for access to restricted parts of application.
     */
    private Set<String> operationSupportRoles;

    /**
     * List with roles for development members on ldap server for access to restricted parts of application.
     */
    private Set<String> developmentRoles;

}
