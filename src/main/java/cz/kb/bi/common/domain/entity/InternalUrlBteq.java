package cz.kb.bi.common.domain.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * Class for keeping url paths of bteq service.
 */
@Getter
@Setter
public class InternalUrlBteq {

    /**
     * Requests bteq init run
     */
    @NonNull
    private String runInit;

    /**
     * Requests bteq maint run.
     */
    @NonNull
    private String runMaint;

    /**
     * Gets run status based on id.
     */
    @NonNull
    private String runStatus;


    /**
     * Returns formatted bteq run status url.
     *
     * @param id identification of bteq run
     * @return formatted url
     */
    public String getFormattedRunStatus(String id) {
        return String.format(runStatus, id);
    }
}
