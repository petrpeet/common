package cz.kb.bi.common.domain.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * Class for keeping url paths of jira service.
 */
@Getter
@Setter
public class InternalUrlJira {

    /**
     * Sdt issue url format.
     */
    @NonNull
    private String sdtIssue;
    /**
     * Sdt attendants.
     */
    @NonNull
    private String attachments;

    /**
     * Sdt connection Check
     */
    @NonNull
    private String connectionCheck;

    /**
     * Issue insert comment
     */
    @NonNull
    private String insertComment;

    /**
     * Issue edit comment
     */
    @NonNull
    private String editComment;

    /**
     * Base url for incident (create and update)
     */
    @NonNull
    private String incident;

    /**
     * Base url for getting extenal id based search
     */
    @NonNull
    private String externalIdUrl;

    /**
     * Formats sdt issue url.
     *
     * @param jiraKey jira key
     * @return formatted url
     */
    public String getFormattedSdtIssue(String jiraKey) {
        return sdtIssue + "/" + jiraKey;
    }

    /**
     * Formats insert comment.
     *
     * @param jiraKey jira issue key
     * @return formatted url
     */
    public String getFormattedInsertComment(String jiraKey) {
        return String.format(insertComment, jiraKey);
    }

    /**
     * Format url for attachment for given issue
     *
     * @param jiraKey jira issue key
     * @return formatted url to attachment
     */
    public String getFormattedAttachment(String jiraKey) {
        return String.format(attachments, jiraKey);
    }

    /**
     * Get formatted url for search base on external id and workflow name
     *
     * @param externalId   external id identificator of workflow
     * @param workflowName name of the workflow
     * @return url for external id
     */
    public String getFormattedExternalId(Long externalId, String workflowName) {
        return String.format(externalIdUrl, workflowName, externalId);
    }

    /**
     * Gets formatted url for editing comment.
     *
     * @param jiraKey   key of jira task
     * @param commentId comment id
     * @return formatted url
     */
    public String getFormattedEditComment(String jiraKey, Long commentId) {
        return String.format(editComment, jiraKey, commentId);
    }
}
