package cz.kb.bi.common.domain.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * Abstract class for keeping url paths to internal micro-services.
 */
@Getter
@Setter
public abstract class InternalUrl {

    /**
     * Teradata mail plain url.
     */
    @NonNull
    private String mail;

    /**
     * Audit log url
     */
    @NonNull
    private String auditLog;

    /**
     * Bteq service urls
     */
    @NonNull
    private InternalUrlBteq bteq;

    /**
     * Jira service urls
     */
    @NonNull
    private InternalUrlJira jira;

    /**
     * Teradata service urls
     */
    @NotNull
    private InternalUrlTeradata teradata;

    /**
     * Script service urls
     */
    @NotNull
    private InternalUrlScript script;

    /**
     * Setting store
     */
    @NonNull
    private String settingStore;

    /**
     * Setting store find
     */
    @NonNull
    private String settingStoreFind;

    /**
     * Setting store delete
     */
    @NonNull
    private String settingStoreDelete;

    /**
     * Workflow documentation service urls
     */
    @NotNull
    private InternalUrlWorkflowDocumentation workflowDocumentation;

    /**
     * Workflow monitor service urls
     */
    @NotNull
    private InternalUrlWorkflowMonitor workflowMonitor;
    /**
     * Online Data Integration Serivces repository urls
     */
    @NotNull
    private InternalUrlOdis odis;

    /**
     * Permission service urls
     */
    @NotNull
    private InternalUrlPermission permission;
}
