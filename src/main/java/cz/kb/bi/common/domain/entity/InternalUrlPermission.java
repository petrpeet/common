package cz.kb.bi.common.domain.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * Url used check checking right for technical user that runs server
 */
@Getter
@Setter
public class InternalUrlPermission {
    /**
     * Url used for checking read and write data
     */
    @NonNull
    private String readWrite;
}
