package cz.kb.bi.common.domain.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.nio.file.Path;

/**
 * Class for keeping url paths of script service.
 */
@Getter
@Setter
public class InternalUrlScript {

    /**
     * Get script by full path
     */
    @NonNull
    private String scriptFullPath;

    /**
     * Get script lock format url
     */
    @NonNull
    private String scriptLock;

    /**
     * Get script url
     */
    @NonNull
    private String script;

    /**
     * Lock script urls
     */
    @NonNull
    private String lockScripts;

    /**
     * Unlock script url
     */
    @NonNull
    private String unlockScripts;

    /**
     * Returns formatted script detail.
     *
     * @param name name of script
     * @return formatted url
     */
    public String getFormattedScriptFullPath(String name) {
        return String.format(scriptFullPath, name);
    }

    /**
     * Gets formatted script lock.
     *
     * @param fullPath full path
     * @return formatted url
     */
    public String getFormattedScriptLock(Path fullPath) {
        return String.format(scriptLock, fullPath.toString());
    }

    /**
     * Gets formatted script.
     *
     * @param fullPath full path
     * @return formatted url
     */
    public String getFormattedScript(Path fullPath) {
        return String.format(script, fullPath.toString());
    }
}
