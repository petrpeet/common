package cz.kb.bi.common.dynamicsql;

import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

import java.sql.JDBCType;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Extends {@link SqlTable} with functionality enabling querying table object for its columns.
 */
public class DynamicSqlTable extends SqlTable {

    private Map<String, SqlColumn> columns = new HashMap<>();

    public DynamicSqlTable(String name){
        super(name);
    }

    /**
     * Finds column defined for this table by its name.
     * @param name Name of column.
     * @return {@link SqlColumn} - column object with given name defined for this table.
     */
    public SqlColumn getColumm(String name) {
        name = name.toUpperCase();
        return columns.get(name);
    }

    @Override
    public <T> SqlColumn<T> column(String name, JDBCType jdbcType) {
        name = name.toUpperCase();
        SqlColumn<T> column = super.column(name, jdbcType);
        columns.put(name, column);
        return column;
    }

    @Override
    public <T> SqlColumn<T> column(String name, JDBCType jdbcType, String typeHandler) {
        SqlColumn<T> column = super.column(name, jdbcType, typeHandler);
        columns.put(name, column);
        return column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DynamicSqlTable)) return false;
        if (!super.equals(o)) return false;
        DynamicSqlTable that = (DynamicSqlTable) o;
        return Objects.equals(columns, that.columns);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), columns);
    }
}
