package cz.kb.bi.common.rest;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;


/**
 * Error handler used for additional logging in case that rest template needs to handle error.
 */
@Slf4j
public class StatusErrorHandler extends DefaultResponseErrorHandler {

    /**
     * Rest decoder used for getting message from JSON
     */
    private static final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Get message from body of response and log it into {@link #log}. Than r
     *
     * @param response the response with the error
     * @throws IOException in case of I/O errors
     */
    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        log.error(readBody(response.getBody()));
        super.handleError(response);
    }

    /**
     * Read message part from json object and return it as a string body. If input stream is empty,
     * reading tree is not called and empty string is returned.
     *
     * @param inputStream body to be read
     * @return body of message
     * @throws IOException when there is problem with oper
     */
    private String readBody(InputStream inputStream) throws IOException {
        try {
            String readStream = IOUtils.toString(inputStream, StandardCharsets.UTF_8);

            if (StringUtils.isBlank(readStream)) {
                return "";
            }

            return objectMapper.readTree(readStream).get("message").getTextValue();
        } catch (JsonProcessingException e) {
            log.error("Unexpected read error", e);
            return "";
        }
    }
}

