package cz.kb.bi.common.rest;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * This configuration sets ssl verification always to true, so services are able to communicate on address
 * https://localhost ...
 */
@Configuration
public class RestTemplateConfiguration {

    /**
     * Set new instance of {@link SimpleClientHttpRequestFactory} into {@link RestTemplate#setRequestFactory(ClientHttpRequestFactory)}
     * that always returns true to ssl verification
     *
     * @param restTemplateBuilder rest template builder for rest communication
     * @return new instance of {@link RestTemplate} with ssl off
     */
    @Bean
    RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        RestTemplate restTemplate = restTemplateBuilder.build();
        restTemplate.setErrorHandler(
                new StatusErrorHandler()
        );
        restTemplate.setRequestFactory(
                new SimpleClientHttpRequestFactory() {
                    @Override
                    protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
                        if (connection instanceof HttpsURLConnection) {
                            ((HttpsURLConnection) connection).setHostnameVerifier((s, sslSession) -> true);
                        }
                        super.prepareConnection(connection, httpMethod);
                    }
                }
        );
        return restTemplate;
    }

}
