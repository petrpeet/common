package cz.kb.bi.common.helpers.api.settingstore;

import cz.kb.bi.domain.dto.settingstore.StoredSettingDTO;

import java.io.IOException;
import java.util.List;

/**
 * Provides functions for working with Setting Store Service.
 * These include sending rest requests and (de)serializing objects into/from json.
 */
public interface SettingStoreHelper {

    /**
     * Converts json to java bean.
     * @param json Text containing json.
     * @param clazz Type of target java bean,
     * @param <T> Type of java bean.
     * @return Java bean created from json.
     * @throws IOException When parsing problem occurs.
     */
    <T> T fromJSON(String json, Class<T> clazz) throws IOException;

    /**
     * Converts java bean to json string.
     * @param value Java bean.
     * @param clazz Type of jav bean.
     * @param <T> Type of java bean.
     * @return Json string with serialized java bean.
     * @throws IOException When parsing problem occurs.
     */
    <T> String toJSON(T value, Class<T> clazz) throws IOException;

    /**
     * Stores setting object.
     * @param setting Setting object to store.
     */
    void storeSetting(StoredSettingDTO setting);

    /**
     * Queries for setting objects by userName, domainName and domainItemId.
     * @param userName User name.
     * @param domainName Domain name.
     * @param domainItemId Domain item id.
     * @return List of found setting objects.
     */
    List<StoredSettingDTO> findSettings(String userName, String domainName, String domainItemId);

    /**
     * Deletes one setting record.
     * @param userName User name.
     * @param domainName Domain name.
     * @param domainItemId Domain item id.
     * @param order Order.
     */
    void deleteSetting(String userName, String domainName, String domainItemId, Integer order);
}
