package cz.kb.bi.common.helpers.impl;


import cz.kb.bi.common.helpers.api.BackendMessageJoiner;
import cz.kb.bi.common.mapping.MapperUtilities;
import cz.kb.bi.domain.entity.BackendMessage;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * BackendMessageJoinerImpl is implementation of {@link BackendMessageJoiner}.
 *
 * @author Jakub Tucek
 */
@Service
class BackendMessageJoinerImpl implements BackendMessageJoiner {

    /**
     * Order message by {@link BackendMessage#getOrder} then group them by {@link BackendMessage#getSummary()}.
     * All {@link BackendMessage#getDetail()} from more messages are stored into {@link BackendMessage#getAdditionalDetails()}
     *
     * @param backendMessages messages to be merged
     * @return {@link Stream} of merged messages
     */
    private static Stream<BackendMessage> groupMessages(List<BackendMessage> backendMessages) {
        final Map<String, BackendMessage> mergedMessages = backendMessages.stream()
                .collect(
                        Collectors.toMap(
                                BackendMessage::getSummary,
                                Function.identity(),
                                BackendMessageJoinerImpl::mergeMessages
                        )
                );
        return mergedMessages.keySet()
                .stream()
                .sorted()
                .map(mergedMessages::get);
    }

    /**
     * Merge two message into one and add {@link BackendMessage#getDetail()} into
     * {@link BackendMessage#getAdditionalDetails()} and return the first message.
     *
     * @param firstMessage  first message to be merged
     * @param secondMessage second message to be merged
     * @return merged message
     */
    private static BackendMessage mergeMessages(BackendMessage firstMessage, BackendMessage secondMessage) {
        BackendMessage mergedMessage = MapperUtilities.map(firstMessage, BackendMessage.class);
        if (mergedMessage.getAdditionalDetails() == null) {
            mergedMessage.setAdditionalDetails(new ArrayList<>());
        }
        mergedMessage.getAdditionalDetails().add(secondMessage.getDetail());
        return mergedMessage;
    }

    /**
     * Get order from {@link BackendMessage} if it is presented or {@link Integer#MAX_VALUE}
     *
     * @param backendMessage message from which the value should be returned
     * @return order of message or default value
     */
    private static Integer getOrderOrMaxIfNull(BackendMessage backendMessage) {
        return ObjectUtils.defaultIfNull(backendMessage.getOrder(), Integer.MAX_VALUE);
    }

    /**
     * Sort all {@link BackendMessage} by {@link BackendMessage#getOrder()} and then aggregate them by
     * {@link BackendMessage#getSummary()}. If the input is null, than empty {@link List} is returned.
     *  Null messages are discarded from processing.
     *
     * @param backendMessages messages to be sorted
     * @return sorted and aggregated {@link BackendMessage}
     */
    @Override
    public List<BackendMessage> groupBackendMessages(List<BackendMessage> backendMessages) {
        final Map<Integer, List<BackendMessage>> collect = ListUtils.emptyIfNull(backendMessages)
                .stream()
                .filter(Objects::nonNull)
                .collect(
                        Collectors.groupingBy(
                                BackendMessageJoinerImpl::getOrderOrMaxIfNull,
                                Collectors.toList()
                        )
                );

        return collect.keySet()
                .stream()
                .sorted()
                .map(collect::get)
                .flatMap(BackendMessageJoinerImpl::groupMessages)
                .collect(Collectors.toList());
    }
}


