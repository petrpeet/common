package cz.kb.bi.common.helpers.impl.settingstore;

import com.fasterxml.jackson.databind.ObjectWriter;
import cz.kb.bi.common.domain.entity.InternalUrl;
import cz.kb.bi.common.helpers.api.settingstore.SettingStoreHelper;
import cz.kb.bi.domain.dto.settingstore.StoredSettingDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Provides functions for working with Setting Store Service.
 * These include sending rest requests and (de)serializing objects into/from json.
 */
@Service
public class SettingStoreHelperImpl implements SettingStoreHelper {

    InternalUrl urls;
    RestTemplate restTemplate;

    @Autowired
    public SettingStoreHelperImpl(InternalUrl urls, RestTemplate restTemplate) {
        this.urls = urls;
        this.restTemplate = restTemplate;
    }

    /**
     * Converts json to java bean.
     * @param json Text containing json.
     * @param clazz Type of target java bean,
     * @param <T> Type of java bean.
     * @return Java bean created from json.
     * @throws IOException When parsing problem occurs.
     */
    public <T> T fromJSON(String json, Class<T> clazz) throws IOException {
        return Jackson2ObjectMapperBuilder.json().build().readValue(json, clazz);
    }

    /**
     * Converts jav bean to json string.
     * @param value Java bean.
     * @param clazz Type of jav bean.
     * @param <T> Type of java bean.
     * @return Json string with serialized java bean.
     * @throws IOException When parsing problem occurs.
     */
    public <T> String toJSON(T value, Class<T> clazz) throws IOException {
        ObjectWriter objectWriter = Jackson2ObjectMapperBuilder.json().build().writerFor(clazz);
        return objectWriter.writeValueAsString(value);
    }

    /**
     * Calls Setting Store Service via REST call. Stores setting object.
     * @param setting Setting object to store.
     */
    public void storeSetting(StoredSettingDTO setting) {
        restTemplate.put(urls.getSettingStore(), setting);
    }

    /**
     * Calls Setting Store Service via REST call. Queries for setting objects by userName, domainName and domainItemId.
     * @param userName User name.
     * @param domainName Domain name.
     * @param domainItemId Domain item id.
     * @return List of found setting objects.
     */
    public List<StoredSettingDTO> findSettings(String userName, String domainName, String domainItemId) {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>(3);
        map.add("userName", userName);
        map.add("domainName", domainName);
        map.add("domainItemId", domainItemId);

        StoredSettingDTO[] storedSettingDTOS
            = restTemplate.postForObject(urls.getSettingStoreFind(), map, StoredSettingDTO[].class);
        return storedSettingDTOS == null ? Collections.emptyList() : Arrays.asList(storedSettingDTOS);
    }

    /**
     * Calls Setting Store Service via REST call. Deletes one setting.
     * @param userName User name.
     * @param domainName Domain name.
     * @param domainItemId Domain item id.
     * @param order Order.
     */
    public void deleteSetting(String userName, String domainName, String domainItemId, Integer order) {
        restTemplate.put(urls.getSettingStoreDelete(),
                         StoredSettingDTO.builder()
                            .userName(userName)
                            .domainName(domainName)
                            .domainItemId(domainItemId)
                            .order(order)
                            .build());
    }
}
