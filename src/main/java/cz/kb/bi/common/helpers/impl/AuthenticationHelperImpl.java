package cz.kb.bi.common.helpers.impl;

import cz.kb.bi.common.domain.entity.RolesConfig;
import cz.kb.bi.common.helpers.api.AuthenticationHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Service
class AuthenticationHelperImpl implements AuthenticationHelper {

    private final RolesConfig rolesConfig;

    @Autowired
    AuthenticationHelperImpl(RolesConfig rolesConfig) {
        this.rolesConfig = rolesConfig;
    }

    /**
     * Return the current instance of {@link Authentication} from {@link SecurityContextHolder#getContext()}
     * If {@link SecurityContextHolder#getContext()} is resolved to null value, than return null value as
     * {@link Authentication}.
     *
     * @return current {@link Authentication} module
     */
    private static Authentication getAuthentication() {
        SecurityContext context = SecurityContextHolder.getContext();
        return context != null ? context.getAuthentication() : null;
    }

    /**
     * Check, if at least one of the user's right given by authorities is on desired right in parameter roles. Return true,
     * if at least one right is found.
     *
     * @param authorities list of user's right
     * @param roles       list of desired roles for some situation
     * @return true, is user has at least one role
     */
    private static boolean isInAuthorities(Collection<? extends GrantedAuthority> authorities, Set<String> roles) {
        return CollectionUtils.emptyIfNull(authorities)
                .stream()
                .map(GrantedAuthority::getAuthority)
                .map(roles::contains)
                .filter(Boolean::booleanValue)
                .findFirst()
                .orElse(false);
    }

    /**
     * Return the name of the user, is user is logged in, or empty string, is the user is not logged in.
     *
     * @return name of the user or empty string, if not logged in
     */
    @Override
    public String getCurrentUserName() {
        Authentication authentication = getAuthentication();

        return authentication != null ? authentication.getName() : "";
    }

    /**
     * Return the indication, if user is part of the operation support team or not.
     *
     * @return indicator, if user is operation support member
     */
    @Override
    public boolean isOperationSupportMember() {
        Authentication authentication = getAuthentication();
        return authentication != null && resolveOperationSupportMember(authentication);
    }

    /**
     * <p>
     * Resolve, if the users role is in the {@link RolesConfig#operationSupportRoles}.
     * </p>
     *
     * @param authentication authentication module from spring
     * @return true, if user is considered as operation support member
     */
    private boolean resolveOperationSupportMember(Authentication authentication) {
        final Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

        return isInAuthorities(authorities, rolesConfig.getOperationSupportRoles());
    }

}
