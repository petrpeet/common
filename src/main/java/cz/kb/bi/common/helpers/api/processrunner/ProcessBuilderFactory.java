package cz.kb.bi.common.helpers.api.processrunner;

import java.io.Serializable;

/**
 * Factory used for running process from command line. This utility does only running and returning of exit code
 */
public interface ProcessBuilderFactory extends Serializable {
    /**
     * Start command defined by parameters commands and return exit value
     *
     * @param commands command with parameters
     * @return exit code of application
     * @throws ProcessBuilderFactoryException if any problem occurs
     * @throws InterruptedException when thread is intrerupted
     */
    int startAndReturnExitCode(String... commands) throws InterruptedException;

    /**
     * Starts given commands in command line and return exit value.
     * Correct terminal is detected for system. Uses cmd for windows /bin/sh for other (unix) os.
     *
     * @param commands to run in terminal
     * @return status code
     * @throws ProcessBuilderFactoryException if any problem occurs
     * @throws InterruptedException when thread is intrerupted
     */
    int startInTerminalAndReturnExitCode(String... commands) throws InterruptedException;
}
