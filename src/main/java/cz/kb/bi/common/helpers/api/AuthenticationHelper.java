package cz.kb.bi.common.helpers.api;

/**
 * Interface that provides basic operation with security framework and return the basic information about user.
 */
public interface AuthenticationHelper {
    /**
     * Return the name of the user, is user is logged in, or empty string, is the user is not logged in.
     *
     * @return name of the user or empty string, if not logged in
     */
    String getCurrentUserName();

    /**
     * Return the indication, if user is part of the operation support team or not.
     *
     * @return indicator, if user is operation support member
     */
    boolean isOperationSupportMember();
}
