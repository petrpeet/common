package cz.kb.bi.common.helpers.api;


import cz.kb.bi.domain.entity.BackendMessage;

import java.util.List;

/**
 * BackendMessageJoiner is interface for grouping {@link BackendMessage} and prevent
 * excessive output for user.
 *
 * @author Jakub Tucek
 */
@FunctionalInterface
public interface BackendMessageJoiner {

    /**
     * Groups backend messages by
     *
     * @param backendMessages list of {@link BackendMessage} to group and join
     * @return {@link BackendMessage} joined
     */
    List<BackendMessage> groupBackendMessages(List<BackendMessage> backendMessages);
}
