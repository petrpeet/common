package cz.kb.bi.common.helpers.impl.processrunner;

import cz.kb.bi.common.helpers.api.processrunner.ProcessBuilderFactory;
import cz.kb.bi.common.helpers.api.processrunner.ProcessBuilderFactoryException;
import cz.kb.bi.common.utils.OSUtils;
import org.apache.commons.collections4.ListUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Factory used for running process from command line. This utility does only running and returning of exit code
 */
@Service
class ProcessBuilderFactoryImpl implements ProcessBuilderFactory {

    private static final long serialVersionUID = 8641798277556146395L;
    /**
     * Command line to be used (cmd on windows/shell on unix)
     */
    private final String commandLine = OSUtils.isWindows() ? "cmd" : "/bin/sh";

    /**
     * System-based flag
     */
    private final String commandLineFlag = OSUtils.isWindows() ? "/c" : "-c";

    /**
     * Actually starts process but argument is list of commands
     *
     * @param commands commands to run
     * @return status code
     * @throws ProcessBuilderFactoryException when process buiding fails
     */
    private static int doStart(List<String> commands) throws InterruptedException {
        try {
            return new ProcessBuilder(commands).start().waitFor();
        } catch (IOException e) {
            throw new ProcessBuilderFactoryException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int startAndReturnExitCode(String... commands) throws InterruptedException {
        return doStart(Arrays.asList(commands));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int startInTerminalAndReturnExitCode(String... commands) throws InterruptedException {
        List<String> cmdList = Arrays.asList(
                commandLine,
                commandLineFlag
        );
        return doStart(
                ListUtils.union(
                        cmdList,
                        Arrays.asList(commands)
                )
        );
    }
}
