package cz.kb.bi.common.factory;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.util.Map;

/**
 * This factory is used for creating {@link ObjectMapper}. Mapper cannot be used via Spring beans
 * annotation, because it ruins {@link java.time.LocalDateTime} and {@link java.time.LocalDate} converters in Spring.
 */
public class ObjectMapperFactory {
    /**
     * {@link JavaType} user for mapping {@link Map} with {@link String} as key and value
     */
    public static final JavaType STRING_MAP_TYPE;
    /**
     * {@link JavaType} user for mapping {@link Map} with {@link String} as key and {@link Object} as value
     */
    public static final JavaType OBJECT_MAP_TYPE;
    /**
     * {@link JavaType} user for mapping array of {@link String}
     */
    public static final JavaType STRING_ARRAY_TYPE;

    static {
        ObjectMapper tmpMapper = createObjectMapper();
        STRING_MAP_TYPE = tmpMapper.getTypeFactory()
                .constructMapLikeType(Map.class, String.class, String.class);
        OBJECT_MAP_TYPE = tmpMapper.getTypeFactory()
                .constructMapLikeType(Map.class, String.class, Object.class);
        STRING_ARRAY_TYPE = tmpMapper.getTypeFactory()
                .constructArrayType(String.class);
    }


    private ObjectMapperFactory() {
        //Should never be used
    }

    /**
     * Create new instance of {@link ObjectMapper} with settings not to fail on empty beans
     *
     * @return new instance of {@link ObjectMapper}
     */
    public static ObjectMapper createObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        return objectMapper;
    }

}
