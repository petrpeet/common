package cz.kb.bi.common.converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Converter used for Spring to convert input {@link LocalDateTime} according {@link DateTimeFormatter#ISO_DATE_TIME}
 * info {@link LocalDateTime} instance using {@link DateTimeFormatter}.
 */
public class LocalDateTimeConverter implements Converter<String, LocalDateTime> {

    /**
     * Formatter used for converting into {@link LocalDateTime}
     */
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME;

    /**
     * If input parameter is blank, than return null. In all other cases, return new instance of {@link LocalDateTime}
     * using {@link LocalDateTime#parse(CharSequence, DateTimeFormatter)}.
     *
     * @param inputParam input date in text representation
     * @return new instance of {@link LocalDateTime} corresponding to input parameter
     */
    @Override
    public LocalDateTime convert(String inputParam) {
        return StringUtils.isNotBlank(inputParam) ? LocalDateTime.parse(inputParam, dateTimeFormatter) : null;
    }
}
