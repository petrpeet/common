package cz.kb.bi.common.converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Converter used for Spring to convert input {@link ZonedDateTime} according {@link DateTimeFormatter#ISO_DATE_TIME}
 * info {@link ZonedDateTime} instance using {@link DateTimeFormatter}.
 */
public class ZonedDateTimeConverter implements Converter<String, ZonedDateTime> {

    /**
     * Formatter used for converting into {@link ZonedDateTime}
     */
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME;

    /**
     * If input parameter is blank, than return null. In all other cases, return new instance of {@link ZonedDateTime}
     * using {@link ZonedDateTime#parse(CharSequence, DateTimeFormatter)}.
     *
     * @param inputParam input date in text representation
     * @return new instance of {@link ZonedDateTime} corresponding to input parameter
     */
    @Override
    public ZonedDateTime convert(String inputParam) {
        return StringUtils.isNotBlank(inputParam) ? ZonedDateTime.parse(inputParam, dateTimeFormatter) : null;
    }
}
