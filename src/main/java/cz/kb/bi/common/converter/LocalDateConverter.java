package cz.kb.bi.common.converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Converter used for Spring to convert input {@link LocalDate} according {@link DateTimeFormatter#ISO_DATE_TIME}
 * info {@link LocalDate} instance using {@link DateTimeFormatter}.
 */
public class LocalDateConverter implements Converter<String, LocalDate> {

    /**
     * Formatter used for converting into {@link LocalDate}
     */
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE;

    /**
     * If input parameter is blank, than return null. In all other cases, return new instance of {@link LocalDate}
     * using {@link LocalDate#parse(CharSequence, DateTimeFormatter)}.
     *
     * @param inputParam input date in text representation
     * @return new instance of {@link LocalDate} corresponding to input parameter
     */
    @Override
    public LocalDate convert(String inputParam) {
        return StringUtils.isNotBlank(inputParam) ? LocalDate.parse(inputParam, dateTimeFormatter) : null;
    }
}
