package cz.kb.bi.common.operator;

import cz.kb.bi.common.utils.TextUtils;

import java.util.function.BinaryOperator;

/**
 * Library class binary operators used in Fenix
 */
public class BinaryOperatorUtils {
    private BinaryOperatorUtils() {
        // this class in not meant to be initialized
    }

    /**
     * Method acts like OR operator
     *
     * @return prevResult OR nextResult
     */
    public static BinaryOperator<Boolean> getOrBinaryOperator() {
        return (prevResult, nextResult) -> prevResult || nextResult;
    }

    /**
     * Method acts like AND operator
     *
     * @return prevResult AND nextResult
     */
    public static BinaryOperator<Boolean> getAndBinaryOperator() {
        return (prevResult, nextResult) -> prevResult && nextResult;
    }

    /**
     * Concatenate two string using new line character
     *
     * @return concatenated string
     */
    public static BinaryOperator<String> getLineConcatenation() {
        return (s, s2) -> s + "\n" + s2;
    }

    /**
     * Concatenate two string using new line character
     *
     * @return concatenated string
     */
    public static BinaryOperator<String> getWindowsLineConcatenation() {
        return (s, s2) -> s + TextUtils.WINDOWS_NEW_LINE + s2;
    }
}
