package cz.kb.bi.common.updownload;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.concurrent.ConcurrentMap;

/**
 * Service for managing delete queue
 */
public interface UpdownLoadQueueService {

    /**
     * Add file to custom queue with custom deletion time.
     * @param path path with file
     * @param localDateTime time to delete
     * @param queue custom queue with files
     */
    void addFileToDelQueue(Path path, LocalDateTime localDateTime, ConcurrentMap<Path, LocalDateTime> queue);

    /**
     *  Add file to default queue with custom deletion time.
     * @param path path with file
     * @param localDateTime time to delete
     */
    void addFileToDelQueue(Path path, LocalDateTime localDateTime);

    /**
     * Add file to custom queue with default deletion time.
     * @param path path with file
     * @param queue custom queue with files
     */
    void addFileToDelQueue(Path path, ConcurrentMap<Path, LocalDateTime> queue);

    /**
     * Add file to default queue with default deletion time.
     * @param path path with file
     */
    void addFileToDelQueue(Path path);

    /**
     * Clean files in custom queue
     * @param queue queue to clean files
     */
    void cleanFiles(ConcurrentMap<Path, LocalDateTime> queue);

    /**
     * Force clean of default map. Should be use only in test.
     */
    void forceClean();
}
