package cz.kb.bi.common.updownload;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

/**
 * Service for uploading and downloading files. Uploading is not implemented yet.
 */
public interface UpdownLoadService {

    /**
     * Create zip from input list of string with paths. Sent zip to download and to delete.
     * @param paths paths to files
     * @param deleteSource do i want delete sources from path?
     * @param queue custom queue for deleting files
     * @return ResponseEntity with zip in stream
     * @throws IOException when io exception occurs
     */
    ResponseEntity<Resource> downloadResponse(List<String> paths, boolean deleteSource, ConcurrentMap<Path, LocalDateTime> queue) throws IOException;

    /**
     * Create zip from input list of string with paths. Sent zip to download and to delete.
     * @param paths paths to files
     * @param deleteSource do i want delete sources from path?
     * @return ResponseEntity with zip in stream
     * @throws IOException when io exception occurs
     */
    ResponseEntity<Resource> downloadResponse(List<String> paths, boolean deleteSource) throws IOException;

    /**
     * Sent file to response.
     * @param path path to files
     * @param deleteSource do i want delete sources from path?
     * @param queue custom queue for deleting files
     * @return ResponseEntity with zip in stream
     * @throws IOException when io exception occurs
     */
    ResponseEntity<Resource> downloadResponse(String path, boolean deleteSource, ConcurrentMap<Path, LocalDateTime> queue) throws IOException;

    /**
     * Sent file to response.
     * @param path path to files
     * @param deleteSource do i want delete sources from path?
     * @return ResponseEntity with zip in stream
     * @throws IOException when io exception occurs
     */
    ResponseEntity<Resource> downloadResponse(String path, boolean deleteSource) throws IOException;

    /**
     * Upload file and get {@link Path} with file.
     * @param multipartFile uploaded file
     * @param deleteSource Should be file deleted?
     * @param queue queue, which care about deleting
     * @return Path with uploaded file
     */
    Path uploadFile(MultipartFile multipartFile, boolean deleteSource, ConcurrentMap<Path, LocalDateTime> queue) throws IOException;

    /**
     * Upload file and get {@link Path} with file.
     * @param multipartFile uploaded file
     * @param deleteSource Should be file deleted?
     * @return Path with uploaded file
     */
    Path uploadFile(MultipartFile multipartFile, boolean deleteSource) throws IOException;

    /**
     * Unzip file, unzip it and get paths from this zip
     * @param path uploaded file
     * @param deleteSource Should be file deleted?
     * @param queue queue, which care about deleting
     * @return uploaded files
     */
    List<Path> uploadZipFile(Path path, boolean deleteSource, ConcurrentMap<Path, LocalDateTime> queue) throws IOException;

    /**
     * Unzip file and get paths from this zip
     * @param path uploaded file
     * @param deleteSource Should be file deleted?
     * @return uploaded files
     */
    List<Path> uploadZipFile(Path path, boolean deleteSource) throws IOException;
}
