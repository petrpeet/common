package cz.kb.bi.common.updownload;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Implementation of {@link UpdownLoadQueueService}
 */
@Service
@Slf4j
public class UpdownLoadQueueImpl implements UpdownLoadQueueService {

    private ConcurrentHashMap<Path, LocalDateTime> hashMap = new ConcurrentHashMap<>();

    /**
     * {@inheritDoc}
     * @param path path with file
     * @param localDateTime time to delete
     * @param queue custom queue with files
     */
    @Override
    public synchronized void addFileToDelQueue(Path path, LocalDateTime localDateTime, ConcurrentMap<Path, LocalDateTime> queue) {
        queue.put(path, localDateTime);
    }

    /**
     * {@inheritDoc}
     * @param path path with file
     * @param localDateTime time to delete
     */
    @Override
    public synchronized void addFileToDelQueue(Path path, LocalDateTime localDateTime) {
        addFileToDelQueue(path, localDateTime, hashMap);
    }

    /**
     * {@inheritDoc}
     * @param path path with file
     * @param queue custom queue with files
     */
    @Override
    public synchronized void addFileToDelQueue(Path path, ConcurrentMap<Path, LocalDateTime> queue) {
        queue.put(path, LocalDateTime.now().plusMinutes(1));
    }

    /**
     * {@inheritDoc}
     * @param path path with file
     */
    @Override
    public synchronized void addFileToDelQueue(Path path) {
        addFileToDelQueue(path, hashMap);
    }

    /**
     * {@inheritDoc}
     * @param queue queue to clean files
     */
    @Override
    public void cleanFiles(ConcurrentMap<Path, LocalDateTime> queue) {
        executionCleanFiles(queue);
    }

    /**
     * Clean files in default queue
     */
    @Scheduled(cron = "0 0 19 * * *")
    public void cleanFiles() {
        executionCleanFiles(hashMap);
    }

    /**
     * Force clean of default map. Should be use only in test.
     */
    @Override
    public void forceClean() {
        hashMap.keySet().stream().forEach(i->doDeleteAction(LocalDateTime.now(), LocalDateTime.now().plusMinutes(1), i));
    }

    /**
     * Clean queue
     * @param queue queue to clean
     */
    private void executionCleanFiles(ConcurrentMap<Path, LocalDateTime> queue) {
        log.info("ScriptZipCleanerServiceImpl started");
        Set<Map.Entry<Path, LocalDateTime>> entries = queue.entrySet();
        LocalDateTime now = LocalDateTime.now();

        for (Map.Entry<Path, LocalDateTime> entry : entries) {
            Path path = entry.getKey();
            LocalDateTime localDateTime = entry.getValue();

            doDeleteAction(localDateTime, now, path);
        }
        log.info("ScriptZipCleanerServiceImpl ended");
    }

    /**
     * Do Delete for some path. If path is a directory, directory and all his content are deleted
     * @param localDateTime time to deletion of entry
     * @param now time of now
     * @param path path to delete
     */
    private void doDeleteAction(LocalDateTime localDateTime, LocalDateTime now, Path path){
        if (localDateTime.isBefore(now)) {
            try {
                if(path.toFile().isDirectory() && path.toFile().exists()){
                    FileUtils.deleteDirectory(path.toFile());
                } else {
                    Files.deleteIfExists(path);
                }
            } catch (IOException e) {
                log.error("Failed to delete script in queue", e);
            }
        }
    }


}
