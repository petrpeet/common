package cz.kb.bi.common.updownload;

import cz.kb.bi.common.utils.ZipUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

/**
 * implementation of {@link UpdownLoadService}
 */
@Service
public class UpdownLoadServiceImpl implements UpdownLoadService {

    @Autowired
    private UpdownLoadQueueService updownLoadQueueService;

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<Resource> downloadResponse(List<String> paths, boolean deleteSource, ConcurrentMap<Path, LocalDateTime> queue) throws IOException {
        Path zipFile = ZipUtils.createZipFile(paths, "scripts-");
        updownLoadQueueService.addFileToDelQueue(zipFile, queue);
        if (deleteSource) {
            paths.stream().forEach(i -> updownLoadQueueService.addFileToDelQueue(Paths.get(i), queue));
        }
        return createEntity(zipFile);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<Resource> downloadResponse(List<String> paths, boolean deleteSource) throws IOException {
        Path zipFile = ZipUtils.createZipFile(paths, "scripts-");
        updownLoadQueueService.addFileToDelQueue(zipFile);
        if (deleteSource) {
            paths.stream().forEach(i -> updownLoadQueueService.addFileToDelQueue(Paths.get(i)));
        }
        return createEntity(zipFile);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<Resource> downloadResponse(String path, boolean deleteSource, ConcurrentMap<Path, LocalDateTime> queue) throws IOException {
        Path pathFile = Paths.get(path);
        if (deleteSource) {
            updownLoadQueueService.addFileToDelQueue(pathFile, queue);
        }
        return createEntity(pathFile);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<Resource> downloadResponse(String path, boolean deleteSource) throws IOException {
        Path pathFile = Paths.get(path);
        if (deleteSource) {
            updownLoadQueueService.addFileToDelQueue(pathFile);
        }
        return createEntity(pathFile);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Path uploadFile(MultipartFile multipartFile, boolean deleteSource, ConcurrentMap<Path, LocalDateTime> queue) throws IOException {
        Path path = createPath(multipartFile);
        if (deleteSource){
            updownLoadQueueService.addFileToDelQueue(path, queue);
        }
        return path;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Path uploadFile(MultipartFile multipartFile, boolean deleteSource) throws IOException {
        Path path = createPath(multipartFile);
        if (deleteSource){
            updownLoadQueueService.addFileToDelQueue(path);
        }
        return path;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Path> uploadZipFile(Path path, boolean deleteSource) throws IOException {
        List<Path> result = ZipUtils.unzipFile(path);
        if (deleteSource){
            updownLoadQueueService.addFileToDelQueue(path);
            updownLoadQueueService.addFileToDelQueue(result.get(0).getParent());
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Path> uploadZipFile(Path path, boolean deleteSource, ConcurrentMap<Path, LocalDateTime> queue) throws IOException {
        List<Path> result = ZipUtils.unzipFile(path);
        if (deleteSource){
            updownLoadQueueService.addFileToDelQueue(path, queue);
            updownLoadQueueService.addFileToDelQueue(result.get(0).getParent(), queue);
        }
        return result;
    }

    /**
     * Create path and copy content of {@link MultipartFile} to path
     * @param multipartFile file with needed content
     * @return created {@link Path}
     * @throws IOException
     */
    private Path createPath(MultipartFile multipartFile) throws IOException {
        byte[] bytes = multipartFile.getBytes();
        Path path = Files.createTempFile("loadedFile", "Temp");
        Files.write(path, bytes);
        return path;
    }

    /**
     * Create response entity with zip file
     * @param path path with zip to download
     * @return created ResponseEntity
     * @throws FileNotFoundException
     */
    private ResponseEntity<Resource> createEntity(Path path) throws FileNotFoundException {
        InputStreamResource resource = new InputStreamResource(new FileInputStream(path.toFile()));
        return ResponseEntity.ok()
                .contentLength(path.toFile().length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }
}
