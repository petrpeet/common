package cz.kb.bi.common.utils;

import java.nio.file.Path;

/**
 * Utility class for manipulation to or from {@link Path}.
 */
public class PathUtils {

    private PathUtils() {
        //Should not be used
    }

    public static String subpathToDisplay(Path path) {
        return path.toString().replace("\\", "/");
    }

    /**
     * Converts path to display (network) representation starting with //.
     *
     * @param path path to convert
     * @return converted path in string or null if given path was null
     */
    public static String pathToDisplay(Path path) {
        if (path != null) {
            final String tmpPath = subpathToDisplay(path);
            // If the path is Windows style, than do nothing
            if (tmpPath.matches("^\\S:.*")) {
                return tmpPath;
            } else {
                // If the path is server UNC, make sure, that it starts with //
                return tmpPath.startsWith("//") ? tmpPath : "/" + tmpPath;
            }
        } else {
            return null;
        }
    }

    /**
     * Checks if path is network path or not.
     *
     * @param path path to check
     * @return true if path is network path
     */
    public static boolean isNetworkPath(Path path) {
        return path.isAbsolute() && pathToDisplay(path).startsWith("//");
    }
}
