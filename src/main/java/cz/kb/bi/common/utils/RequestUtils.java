package cz.kb.bi.common.utils;

import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * RequestsUtils provides utility methods for manipulation with {@link HttpServletRequest}.
 */
public class RequestUtils {

    private RequestUtils() {
        //empty
    }


    /**
     * <p>
     * Gets parameter from {@link HttpServletRequest}. With help of
     * {@link HandlerMapping#PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE} gets base path from {@link HttpServletRequest}.
     * Result is then cleared from prefix mapping path.
     * </p>
     * Example:
     * <ul>
     * <li>End point mapping is: /internal/script/**</li>
     * <li>Called url was: /internal/script/my/super/path</li>
     * <li>Method is called with params: ("/internal/script/", request) </li>
     * <li>Result is: "/my/super/path"</li>
     * </ul>
     *
     * @param prefix  prefix that is replaced with empty string in url
     * @param request request containing mapped path
     * @return parameter in url
     */
    public static String getParameter(String prefix, HttpServletRequest request) {
        String queryTmp = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        return queryTmp.replaceFirst(prefix, "");
    }
}
