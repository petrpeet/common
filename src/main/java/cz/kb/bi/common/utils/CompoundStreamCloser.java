package cz.kb.bi.common.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.Closeable;
import java.io.IOException;

@Slf4j
public class CompoundStreamCloser implements Runnable {

    private final Closeable closeable;

    public CompoundStreamCloser(Closeable scanner) {
        this.closeable = scanner;
    }

    @Override
    public void run() {
        try {
            closeable.close();
        } catch (IOException e) {
            log.error("Unable to close closeable.", e);
        }
    }
}
