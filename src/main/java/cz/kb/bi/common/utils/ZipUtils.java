package cz.kb.bi.common.utils;

import cz.kb.bi.domain.entity.common.TextFile;
import lombok.SneakyThrows;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Create zip file from given paths.
 */
public final class ZipUtils {

    private static final int BATCH_SIZE = 512;

    private ZipUtils() {
        //empty
    }

    /**
     * Create zip from list of text files.
     * @param textFiles lisst with script names and content
     * @param fileName name of result zip
     * @return created zip
     * @throws IOException when manipulating with file fails
     */
    public static Path createZipFileFromTexts(List<TextFile> textFiles, String fileName) throws IOException {
        Path directory = Files.createTempDirectory("zipDir");
        List<Path>  pathsToZip = textFiles.stream().map(i-> createPathFromText(i, directory)).collect(Collectors.toList());
        Path zip = createZipFileFromPaths(pathsToZip, fileName);
        FileUtils.deleteDirectory(directory.toFile());
        return zip;
    }

    /**
     * Create Path from textFile
     * @param textFile object with file name and content
     * @return created Path
     */
    @SneakyThrows(IOException.class)
    private static Path createPathFromText(TextFile textFile, Path dir)  {
        Path result = Paths.get(dir + "/" + textFile.getScriptName());
        Files.write(result, textFile.getScriptContent().getBytes());
        return result;
    }

    /**
     * Create zip from list of full paths.
     *
     * @param paths    list of paths, which i want to zip
     * @param fileName name of zip
     * @return zip made from input list
     * @throws IOException when manipulating with file fails
     */
    public static Path createZipFile(List<String> paths, String fileName) throws IOException {
        Path tmpFilePath = Files.createTempFile(fileName, ".zip");
        File tmpFile = tmpFilePath.toFile();

        try (OutputStream out = new FileOutputStream(tmpFile);
             ArchiveOutputStream os
                     = new ArchiveStreamFactory().createArchiveOutputStream(ArchiveStreamFactory.ZIP, out)) {
            for (String p : paths) {
                addEntryIntoArchive(os, p);
            }
        } catch (ArchiveException e) {
            Files.delete(tmpFilePath);
            throw new IOException(e);
        }

        return tmpFilePath;
    }

    /**
     * Create zip from list of full paths in {@link Path}.
     *
     * @param paths    list of paths, which i want to zip
     * @param fileName name of zip
     * @return zip made from input list
     */
    @SneakyThrows(IOException.class)
    public static Path createZipFileFromPaths(List<Path> paths, String fileName) {
        List<String> convertToPaths = paths.stream()
                .map(Path::toAbsolutePath)
                .map(Path::toString)
                .collect(Collectors.toList());

        return createZipFile(convertToPaths, fileName);
    }

    /**
     * Add entry defined in result parameter into archive
     *
     * @param archive target zip archive
     * @param pathStr result to be added
     * @throws IOException if any IO problem occurs
     */
    public static void addEntryIntoArchive(ArchiveOutputStream archive, String pathStr) throws IOException {
        Path path = Paths.get(pathStr);
        archive.putArchiveEntry(new ZipArchiveEntry(path.getFileName().toString()));

        try (FileInputStream fileInputStream = new FileInputStream(path.toFile())) {
            IOUtils.copy(fileInputStream, archive);
            archive.closeArchiveEntry();
        }
    }

    /**
     * Unzip File and return list of Path with files form zip
     * @param zip zip File
     * @return Path with unzipped files
     * @throws IOException when problem with I/O or zip occurs.
     */
    public static List<Path> unzipFile(Path zip) throws IOException {
        Path unzipped = Files.createTempDirectory("unzipped");
        try {
            ZipFile zipFile = new ZipFile(zip.toString());
            zipFile.extractAll(unzipped.toString());
        } catch (ZipException e) {
            throw new IOException("Problems with Zip. Do you use correct file?", e);
        }
        try(Stream<Path> files = Files.walk(unzipped)){
            return files
                    .skip(1)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Merge List of files into one file.
     *
     * @param filesToMerge List of files to merge
     * @param target     File, where filesToMerge will be merged
     * @throws IOException
     */
    public static void mergeFiles(List<Path> filesToMerge, Path target) throws IOException {
        try (OutputStream os = Files.newOutputStream(target)) {
            filesToMerge.stream()
                    .forEach(i -> {
                        try {
                            writeToFile(os, i);
                        } catch (IOException e) {
                            throw new IllegalStateException("IOException", e);
                        }
                    });
        }
    }

    /**
     * Write file from {@link Path} to file.
     *
     * @param os outputstream for writing. Method don't close this stream.
     * @param source file to write
     * @throws IOException
     */
    private static void writeToFile(OutputStream os, Path source) throws IOException {
        byte[] inputArr = new byte[BATCH_SIZE];
        int read;
        try (InputStream is = Files.newInputStream(source)) {
            while ((read = is.read(inputArr)) == BATCH_SIZE) {
                os.write(inputArr);
            }
            if (read > -1) {
                os.write(inputArr, 0, read);
            }
        }
    }

}
