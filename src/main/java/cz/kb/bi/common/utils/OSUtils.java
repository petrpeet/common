package cz.kb.bi.common.utils;

/**
 * Utility class containing helpers for determining OS specific info / settings.
 */
public final class OSUtils {

    private OSUtils() {
        //empty
    }

    /**
     * Returns os name
     *
     * @return os.name property as string
     */
    public static String getOsName() {
        return System.getProperty("os.name");
    }

    /**
     * Checks if os name starts with string "Windows"
     *
     * @return true if is windows
     */
    public static boolean isWindows() {
        return getOsName().startsWith("Windows");
    }

}