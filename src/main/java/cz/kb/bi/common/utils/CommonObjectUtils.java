package cz.kb.bi.common.utils;

import org.apache.commons.collections4.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Common utilities used for {@link Object}. The name is different from others in package
 * because ObjectUtils is used in ApacheCommons too.
 */
public class CommonObjectUtils {

    private CommonObjectUtils() {
        // should not be used
    }

    /**
     * Return first non null parameter from input parameters or null, if all parameters are null
     *
     * @param inputValues input values to be scanned
     * @param <T>         type on input parameters
     * @return first non null parameters
     */
    @SafeVarargs
    public static <T> T getFirstIfNotNull(T... inputValues) {
        return Stream.of(
                inputValues
        ).filter(Objects::nonNull)
                .findFirst()
                .orElse(null);
    }

    /**
     * Return first non empty list from input parameters or null, if all parameters are null
     *
     * @param inputValues input values to be scanned
     * @param <T>         type on input parameters
     * @return first non null parameters
     */
    @SafeVarargs
    public static <T extends List> T getFirstIfNotEmpty(T... inputValues) {
        return Stream.of(
                inputValues
        ).filter(CollectionUtils::isNotEmpty)
                .findFirst()
                .orElse(null);
    }

}
