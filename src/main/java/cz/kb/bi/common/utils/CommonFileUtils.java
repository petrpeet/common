package cz.kb.bi.common.utils;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Stream;

import static java.util.Spliterator.IMMUTABLE;
import static java.util.Spliterator.NONNULL;
import static java.util.Spliterators.spliteratorUnknownSize;
import static java.util.stream.StreamSupport.stream;

@Slf4j
public final class CommonFileUtils {

    public static final Charset UTF_CHARSET = StandardCharsets.UTF_8;
    public static final Charset CP_1250_CHARSET = Charset.forName("cp1250");
    public static final Charset DEFAULT_CHARSET = Charset.defaultCharset();

    private static final String RETURN_CARRIAGE = TextUtils.RETURN_CARRIAGE;

    private CommonFileUtils() {
        throw new UnsupportedOperationException("This operation is not supported");
    }

    public static Charset suggestBTEQScriptCharset(Path scriptPath) {
        try {
            Files.readAllLines(scriptPath, UTF_CHARSET);
            return UTF_CHARSET;
        } catch (IOException e) {
            log.debug("Charset reading problem", e);
            return CP_1250_CHARSET;
        }
    }

    public static Stream<String> openAsStreamBTEQSafely(Path path) {
        try {
            return openAsStreamBTEQ(path);
        } catch (IOException e) {
            log.error("Unable to open BTEQ", e);
            return Stream.empty();
        }
    }

    public static Stream<String> openAsStreamBTEQ(Path path) throws IOException {
        try {
            return Files.lines(path, UTF_CHARSET);
        } catch (IOException e) {
            log.debug("Unable to open file in UTF", e);
            return Files.lines(path, CP_1250_CHARSET);
        }
    }

    private static Stream<String> cleanStream(Stream<String> stringStream) {
        return stringStream.filter(Objects::nonNull).map(i -> i.replaceAll(RETURN_CARRIAGE, ""));
    }

    @SneakyThrows(IOException.class)
    public static Stream<String> openAsStream(Path path, String delimiterRegex, Charset charset) {
        Scanner scanner = new Scanner(Files.newInputStream(path), charset.name()); //NOSONAR sonar doesn't allow to keep the scanner open
        return cleanStream(
            stream(
                spliteratorUnknownSize(
                    scanner.useDelimiter(delimiterRegex),
                    IMMUTABLE | NONNULL
                ),
                false
            )
        ).onClose(new CompoundStreamCloser(scanner));
    }

    /**
     * Gets base file name - without extension. Uses {@link FilenameUtils#getBaseName(String)}
     *
     * @param path path of file
     * @return base file name
     */
    public static String getBaseFileName(Path path) {
        return FilenameUtils.getBaseName(path.getFileName().toString());
    }

    /**
     * Gets file extension on given path. Uses {@link FilenameUtils#getExtension(String)}.
     * Dot before extension is present only if extension actually exist, then empty string is returned.
     *
     * @param path path of file
     * @return file extension; empty if not found
     */
    public static String getFileExtensionWithDot(Path path) {
        String extension = FilenameUtils.getExtension(path.getFileName().toString());
        return extension.isEmpty() ? "" : "." + extension;
    }

    /**
     * Tests if file exists.
     *
     * @param file file path to check
     * @return true if file exists and is not dir
     */
    public static boolean isFileExists(Path file) {
        return file.toFile().exists() && !file.toFile().isDirectory();
    }
}
