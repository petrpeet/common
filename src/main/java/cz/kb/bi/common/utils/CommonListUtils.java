package cz.kb.bi.common.utils;

import org.apache.commons.collections4.ListUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Common utilities used for {@link List}. The name is different from others in package
 * because ListUtils is used in ApacheCommons too.
 */
public class CommonListUtils {


    private CommonListUtils() {
        // should not be used
    }


    /**
     * Join entries from all lists in input parameters into one. Null in input parameters are skipped and no exception
     * is raised.
     *
     * @param inputLists input lists to be converted
     * @param <T>        input type of list
     * @return entries from all input lists joined into one
     */
    @SafeVarargs
    public static <T> List<T> joinLists(List<T>... inputLists) {
        return Stream.of(
                inputLists
        )
                .map(ListUtils::emptyIfNull)
                .flatMap(Collection::stream)
                .collect(
                        Collectors.toList()
                );
    }
}
