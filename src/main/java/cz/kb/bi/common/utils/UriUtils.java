package cz.kb.bi.common.utils;

import java.net.URI;

/**
 * Utilities used for manipulation with {@link java.net.URI}, which is not provided by URI itself.
 */
public class UriUtils {
    private UriUtils() {
        // Should not be used
    }

    /**
     * Append path in second parameter to URI in first parameter. If URI contains trailing
     *
     * @param uri          base uri
     * @param pathToAppend path to be append to uri
     * @return new uri that concatenates uri with path to append
     */
    public static URI append(URI uri, String pathToAppend) {
        final String uriString = uri.toString();
        final String fixedUri = uriString.endsWith("/") ? uriString.substring(0, uriString.length() - 1) : uriString;
        final String fixedPathToAppend = pathToAppend.startsWith("/") ? pathToAppend.substring(1, pathToAppend.length()) : pathToAppend;

        return URI.create(String.format("%s/%s", fixedUri, fixedPathToAppend));
    }
}
