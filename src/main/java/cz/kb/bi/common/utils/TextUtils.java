package cz.kb.bi.common.utils;


import org.apache.commons.lang3.StringUtils;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utils class keeping global constants used for text processing.
 */
public final class TextUtils {

    public static final String NEW_LINE = "\n";
    public static final String RETURN_CARRIAGE = "\r";
    public static final String WINDOWS_NEW_LINE = RETURN_CARRIAGE + NEW_LINE;

    private TextUtils() {
        throw new UnsupportedOperationException("This operation is not supported");
    }

    /**
     * Merge input string into one by concatenation of all nonBlank strings into one
     *
     * @param inputString input string to be concatenated
     * @return non blank input string concatenated into one
     */
    public static String mergeStrings(String... inputString) {
        return Stream.of(inputString)
                .filter(StringUtils::isNotEmpty)
                .collect(
                        Collectors.joining(WINDOWS_NEW_LINE)
                );
    }
}
