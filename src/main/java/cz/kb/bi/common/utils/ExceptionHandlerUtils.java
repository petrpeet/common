package cz.kb.bi.common.utils;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.Optional;

/**
 * This utility class is designed to simplify handling exception errors.
 */
@Slf4j
public class ExceptionHandlerUtils {


    private ExceptionHandlerUtils() {
        //empty
    }

    /**
     * Handles {@link HttpStatusCodeException}. If status code is 404, then empty is returned, otherwise
     * exception is logged and rethrown.
     *
     * @param e            thrown exception
     * @param errorMessage error message in logger format
     * @param args         arguments to be inserted into message
     * @param <T>          type of optional to be returned
     * @return empty optional
     * @throws HttpStatusCodeException if status code is NOT 404
     */
    public static <T> Optional<T> handleApiErrorOptional(HttpStatusCodeException e,
                                                         String errorMessage,
                                                         String... args) {
        if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
            return Optional.empty();
        }

        log.error(errorMessage, args, e);
        throw e;
    }

    /**
     * Handles {@link HttpStatusCodeException}.
     * Same as {@link #handleApiErrorOptional(HttpStatusCodeException, String, String...)}, but return value is boolean.
     *
     * @param e            thrown exception
     * @param errorMessage error message in logger format
     * @param args         arguments to be inserted into message
     * @return false
     * @throws HttpStatusCodeException if status code is NOT 404
     */
    public static boolean handleApiErrorBoolean(HttpStatusCodeException e, String errorMessage, String... args) {
        if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
            return false;
        }

        log.error(errorMessage, args, e);
        throw e;
    }

}
