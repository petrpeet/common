package cz.kb.bi.common.security;

import org.springframework.security.access.annotation.Secured;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is shortcut how to write that any authenticated used is allowed to
 * use data
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Secured("ROLE_AI_USERS_4800_FENIX_ADMIN")
public @interface AdministratorsOnly {
}