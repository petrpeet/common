package cz.kb.bi.common.mapping.mapper;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerConverter;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Converter used for Dozer to convert String into Path and via versa
 */
public class StringPathConverter extends DozerConverter<String, Path> {

    /**
     * Patterns that should be removed from path converted to string or ignored during computation.
     * Only second group is passed to final result
     */
    private static final Pattern[] FILE_PREFIX_REMOVE_PATTERN = {Pattern.compile("(file:)(//.*)")};

    /**
     * Default empty constructor for call super with {@link String} and {@link Path} argumetnsø
     */
    public StringPathConverter() {
        super(String.class, Path.class);
    }

    /**
     * Replace all {@code \} in input string for {@code /} and them check all {@link Pattern} in
     * {@link #FILE_PREFIX_REMOVE_PATTERN} and if any any pattern matches, than replace the input string for
     * second group only. If not, return string with replaced slash
     *
     * @param inputPath input pattern to be fixed
     * @return normalized string that can be converted into {@link Path}
     * @throws NullPointerException if the input string is null
     */
    private static String normalizePath(String inputPath) {
        final String replacedSlash = inputPath.replaceAll("\\\\", "/");
        return Stream.of(FILE_PREFIX_REMOVE_PATTERN)
                .map(i -> i.matcher(replacedSlash))
                .filter(Matcher::matches)
                .findFirst()
                .map(i -> i.replaceAll("$2"))
                .orElse(replacedSlash);
    }

    /**
     * Invert input string in first parameter into path
     *
     * @param string string to be converted into path
     * @param path   not used
     * @return Path corresponding to string
     */
    @Override
    public Path convertTo(String string, Path path) {
        return StringUtils.isNotBlank(string) ? Paths.get(normalizePath(string)) : null;
    }

    /**
     * Convert input path in first parameters into string
     * @param path path to be converted
     * @param string not used
     * @return String corresponding to path
     */
    @Override
    public String convertFrom(Path path, String string) {
        return path != null ? normalizePath(path.toString()) : null;
    }
}
