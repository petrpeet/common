package cz.kb.bi.common.mapping.mapper;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.dozer.DozerConverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;

/**
 * Converter used for Dozer to convert String into Path and via versa
 */
@Getter(value = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
public class LocalDateZoneDateTimeConverter extends DozerConverter<ZonedDateTime, LocalDate> {

    private LocalDateTimeZoneDateTimeConverter converter;

    public LocalDateZoneDateTimeConverter() {
        super(ZonedDateTime.class, LocalDate.class);
        converter = new LocalDateTimeZoneDateTimeConverter();
    }

    /**
     * Converts date to date time.
     *
     * @param localDate local date to convert
     * @return null if local date is null or {@link LocalDateTime} with 0:0:0 time
     */
    private static LocalDateTime dateToDateTime(LocalDate localDate) {
        return localDate != null ?
                LocalDateTime.of(
                        localDate,
                        LocalTime.MIDNIGHT
                ) : null;
    }

    /**
     * Converts the source field to the destination field and return the resulting destination
     * value.
     *
     * @param source      the value of the source field
     * @param destination the current value of the desitinatino field (or null)
     * @return the resulting value for the destinatino field
     */
    @Override
    public LocalDate convertTo(ZonedDateTime source, LocalDate destination) {
        LocalDateTime localDateTime = converter.convertTo(source, dateToDateTime(destination));

        return localDateTime != null ? localDateTime.toLocalDate() : null;
    }

    /**
     * Converts the source field to the destination field and return the resulting destination
     * value
     *
     * @param source      the value of the source field
     * @param destination the current value of the destination field (or null)
     * @return the resulting value for the destination field
     */
    @Override
    public ZonedDateTime convertFrom(LocalDate source, ZonedDateTime destination) {
        return converter.convertFrom(
                dateToDateTime(source),
                destination
        );
    }
}
