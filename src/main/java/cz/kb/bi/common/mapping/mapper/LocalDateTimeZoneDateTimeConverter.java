package cz.kb.bi.common.mapping.mapper;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.dozer.DozerConverter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Converter used for Dozer to convert String into Path and via versa
 */
@Getter(value = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
public class LocalDateTimeZoneDateTimeConverter extends DozerConverter<ZonedDateTime, LocalDateTime> {

    /**
     * Zone in Czech Republic that corresponds to summer time
     */
    public static final ZoneId SUMMER_ZONE = ZoneId.of("+02:00");

    /**
     * Zone in Czech Republic that corresponds to winter time
     */
    public static final ZoneId WINTER_ZONE = ZoneId.of("+01:00");

    private Map<Integer, Map.Entry<LocalDateTime, LocalDateTime>> timeShifts = new HashMap<>();

    /**
     * Default empty constructor for call super with {@link ZonedDateTime} and {@link LocalDateTime} argumetnsø
     */
    public LocalDateTimeZoneDateTimeConverter() {
        super(ZonedDateTime.class, LocalDateTime.class);
    }

    /**
     * Converts the source field to the destination field and return the resulting destination
     * value.
     *
     * @param source      the value of the source field
     * @param destination the current value of the desitinatino field (or null)
     * @return the resulting value for the destinatino field
     */
    @Override
    public LocalDateTime convertTo(ZonedDateTime source, LocalDateTime destination) {
        return source != null ? source.withZoneSameInstant(ZoneId.of("Europe/Prague")).toLocalDateTime() : null;
    }

    /**
     * Convert {@link LocalDateTime} into {@link ZonedDateTime} with suggestion of time zone according to
     * calendar of Czech Republic.
     *
     * @param source      the value of the source field
     * @param destination the current value of the destination field (or null)
     * @return the resulting value for the destination field
     */
    @Override
    public ZonedDateTime convertFrom(LocalDateTime source, ZonedDateTime destination) {
        return source != null ? convertToZoneDateTime(source) : null;
    }

    /**
     * Convert {@link LocalDateTime} into {@link ZonedDateTime} with guest time zone. Time zone is cached for every year,
     * that is used for conversion.
     *
     * @param source source time to be converted
     * @return converted time with guest time zone
     */
    private ZonedDateTime convertToZoneDateTime(LocalDateTime source) {
        final Integer year = source.getYear();
        if (!timeShifts.containsKey(year)) {
            calculateShiftForYear(year);
        }
        final Map.Entry<LocalDateTime, LocalDateTime> localDateTimeLocalDateTimeEntry = timeShifts.get(year);
        final LocalDateTime shiftStart = localDateTimeLocalDateTimeEntry.getKey();
        final LocalDateTime shiftEnd = localDateTimeLocalDateTimeEntry.getValue();
        final ZoneId currentZone =
                source.isAfter(shiftStart) && source.isBefore(shiftEnd) ? SUMMER_ZONE : WINTER_ZONE;
        return ZonedDateTime.of(source, currentZone);
    }

    /**
     * Calculate the summer/winter time shift in Czech Republic for current year and store it into {@link #timeShifts}.
     * The summer shift starts in last sunday of March.
     * The winter shift starts in last sunday of October.
     *
     * @param year year to calculate time shift
     */
    private void calculateShiftForYear(Integer year) {
        LocalDateTime start = LocalDateTime.of(year, 4, 1, 3, 0);
        LocalDateTime end = LocalDateTime.of(year, 11, 1, 3, 0);
        timeShifts.put(
                year,
                new AbstractMap.SimpleImmutableEntry<>(
                        start.minusDays(start.getDayOfWeek().getValue()),
                        end.minusDays(end.getDayOfWeek().getValue())
                )
        );
    }
}
