package cz.kb.bi.common.mapping;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.SetUtils;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utilities used for mapping internal entity object as DTO
 */
public class MapperUtilities {
    /**
     * Mapper used for mapping
     */
    private static final Mapper DTO_MAPPER;

    static {
        DozerBeanMapper tmpMapper = new DozerBeanMapper();
        tmpMapper.setMappingFiles(Collections.singletonList("biApiMappers.xml"));
        DTO_MAPPER = tmpMapper;
    }

    private MapperUtilities() {
        // Never use the constructor
    }

    /**
     * Map {@link List} of input object into {@link List} of output objects, which class is provided in second parameter.
     * Dozer common mapping is used, so the properties are mapped according name convention.
     *
     * @param inputObjects input objects
     * @param outputClass  class of output objects
     * @param <T>          type of output objects
     * @param <I>          type of input objects
     * @return new instance of {@link List} with objects converted. The class is defined by second parameter.
     */
    public static <T, I> List<T> map(List<I> inputObjects, Class<T> outputClass) {
        return ListUtils.emptyIfNull(inputObjects)
                .stream()
                .map(i -> map(i, outputClass))
                .collect(Collectors.toList());
    }

    /**
     * Map {@link Set} of input object into {@link Set} of output objects, which class is provided in second parameter.
     * Dozer common mapping is used, so the properties are mapped according name convention.
     *
     * @param inputObjects input objects
     * @param outputClass  class of output objects
     * @param <T>          type of output objects
     * @param <I>          type of input objects
     * @return new instance of {@link Set} with objects converted. The class is defined by second parameter.
     */
    public static <T, I> Set<T> map(Set<I> inputObjects, Class<T> outputClass) {
        return SetUtils.emptyIfNull(inputObjects)
                .stream()
                .map(i -> map(i, outputClass))
                .collect(Collectors.toSet());
    }

    /**
     * Map input object into output object, which class is provided in second parameter.
     * Dozer common mapping is used, so the properties are mapped according name convention.
     *
     * @param inputObject input object
     * @param outputClass class of output object
     * @param <T>         type of output objects
     * @param <I>         type of input objects
     * @return new instance of object converted. The class is defined by second parameter.
     */
    public static <T, I> T map(I inputObject, Class<T> outputClass) {
        return DTO_MAPPER.map(inputObject, outputClass);
    }

    /**
     * Map input object into output object, which class is provided in second parameter.
     * Dozer common mapping is used, so the properties are mapped according name convention.
     * If input object is null, returns value provided as third parameter.
     * @param inputObject input object
     * @param outputClass class of output object
     * @param nullValue default value if input is null
     * @return new instance of object converted. The class is defined by second parameter.
     *          If input object is null, returns value provided as third parameter.
     */
    public static <T, I> T map(I inputObject, Class<T> outputClass, T nullValue) {
        return inputObject == null ? nullValue : DTO_MAPPER.map(inputObject, outputClass);
    }

    /**
     * Map input object as {@link Optional} into output object, which class is provided in second parameter. When input
     * {@link Optional} is empty, then null returned.
     * Dozer common mapping is used, so the properties are mapped according name convention.
     *
     * @param inputObject input object
     * @param outputClass class of output object
     * @param <T>         type of output objects
     * @param <I>         type of input objects
     * @return new instance of object converted. The class is defined by second parameter.
     */
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, I> T map(Optional<I> inputObject, Class<T> outputClass) {
        return inputObject.map(i -> DTO_MAPPER.map(i, outputClass)).orElse(null);
    }

    /**
     * Converts optional into {@link ResponseEntity} using following logic:
     * <ul>
     * <li>If inputObject is {@link Optional#isPresent()} than {@link ResponseEntity#getStatusCode()} is set to
     * {@link HttpStatus#OK} and {@link ResponseEntity#getBody()}</li>
     * <li>If inputObject is not {@link Optional#isPresent()} than {@link ResponseEntity#getStatusCode()} is set to
     * {@link HttpStatus#NOT_FOUND} and {@link ResponseEntity#getBody()}</li>
     * <li>Body of {@link ResponseEntity} is set with {@link #map(Optional, Class)}</li>
     * </ul>
     *
     * @param inputObject input object that should be checked
     * @param outputClass output class that should be as a result
     * @param <T>         type of output object
     * @param <I>         type of input object
     * @return {@link ResponseEntity} with filled body and status according input entities
     */
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <T, I> ResponseEntity<T> mapToResponseEntity(Optional<I> inputObject, Class<T> outputClass) {
        HttpStatus status = inputObject.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(map(inputObject, outputClass), status);
    }

    /**
     * Converts optional into {@link ResponseEntity} using following logic:
     * <ul>
     * <li>If inputObject is {@link Optional#isPresent()} than {@link ResponseEntity#getStatusCode()} is set to
     * {@link HttpStatus#OK} and {@link ResponseEntity#getBody()}</li>
     * <li>If inputObject is not {@link Optional#isPresent()} than {@link ResponseEntity#getStatusCode()} is set to
     * {@link HttpStatus#NOT_FOUND} and {@link ResponseEntity#getBody()}</li>
     * <li>Body of {@link ResponseEntity} is set with the actual value of input object</li>
     * </ul>
     *
     * @param inputObject input object that should be checked
     * @param <I>         type of input object
     * @return {@link ResponseEntity} with filled body and status according input entities
     */
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static <I> ResponseEntity<I> mapToResponseEntity(Optional<I> inputObject) {
        HttpStatus status = inputObject.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(inputObject.orElse(null), status);
    }

    /**
     * Converts input Object into {@link ResponseEntity} using following logic:
     *
     * @param inputObject input object that should be checked
     * @param outputClass output class that should be as a result
     * @param <T>         type of output object
     * @param <I>         type of input object
     * @return {@link ResponseEntity} with filled body and status according input entities
     */
    public static <T, I> ResponseEntity<T> mapToResponseEntity(I inputObject, Class<T> outputClass) {
        return new ResponseEntity<>(map(inputObject, outputClass), HttpStatus.OK);
    }

    /**
     * Map all input object stored in array into array of output class object. The conversion is done by
     * {@link #map(Object, Class)}
     *
     * @param inputArrayObject input array of objects
     * @param outputClass      output class of object
     * @param <T>              type of output object
     * @param <I>              type of input object
     * @return new array of object of output class
     */
    @SuppressWarnings("unchecked")
    public static <T, I> List<T> map(I[] inputArrayObject, Class<T> outputClass) {
        return Stream.of(inputArrayObject)
                .map(i -> map(i, outputClass))
                .collect(
                        Collectors.toList()
                );
    }


    /**
     * Maps object to optional of given outputClass. If input object is null, then {@link Optional#empty()}
     * is returned. If object is not null, then result of {@link #map(Object, Class)} is only wrapped by optional.
     *
     * @param inputObject input object
     * @param outputClass output class of object
     * @param <T>         type of output object
     * @param <I>         type of input object
     * @return new optional of object of output class
     */
    public static <T, I> Optional<T> mapToOptional(I inputObject, Class<T> outputClass) {
        if (inputObject == null) {
            return Optional.empty();
        }
        return Optional.of(
                map(inputObject, outputClass)
        );
    }


}
