package cz.kb.bi.common.mapping.mapper;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerConverter;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Converter used for Dozer to convert String into Duration and via versa
 */
public class StringDurationConverter extends DozerConverter<String, Duration> {

    private static final String SPLIT_FIELD_CHAR = ":";

    /**
     * Default empty constructor for call super with {@link String} and {@link Duration} argumetnsø
     */
    public StringDurationConverter() {
        super(String.class, Duration.class);
    }

    /**
     * The expected format of string is one of:
     * <ul>
     * <li>hh:mm:ss</li>
     * <li>mm:ss</li>
     * <li>ss</li>
     * </ul>
     * Split the input string by : and then add the values and return it as
     * {@link Duration#ofSeconds(long)}.
     *
     * @param string input string to be parsed
     * @return duration according string
     */
    private static Duration convertIntoDuration(String string) {
        String[] split = string.split(SPLIT_FIELD_CHAR);
        long sum = 0;
        long multiplier = 0;
        for (int i = split.length - 1; i >= 0; i--) {
            final Integer value = Integer.valueOf(split[i]);
            sum += value * (int) Math.pow(60, multiplier++);
        }
        return Duration.ofSeconds(sum);
    }

    /**
     * Invert input string in first parameter into duration
     *
     * @param string   string to be converted into path
     * @param duration not used
     * @return Duration corresponding to string
     */
    @Override
    public Duration convertTo(String string, Duration duration) {
        return StringUtils.isNotBlank(string) ? convertIntoDuration(string) : null;
    }

    /**
     * Convert input duration in first parameters into string
     *
     * @param duration duration to be converted
     * @param string   not used
     * @return String corresponding to path
     */
    @Override
    public String convertFrom(Duration duration, String string) {
        return duration != null ? convertIntoString(duration) : null;
    }

    /**
     * Return the input duration in one of following format:
     * <ul>
     * <li>hh:mm:ss</li>
     * <li>mm:ss</li>
     * <li>ss</li>
     * </ul>
     * number of hours can be greater than 24.
     *
     * @param duration to be parsed
     * @return stirng representation of duration
     */
    private static String convertIntoString(Duration duration) {
        long seconds = duration.getSeconds();

        List<String> results = new ArrayList<>();
        for (int i = 2; i >= 0; i--) {
            final int multiplier = (int) Math.pow(60, i);
            int result = (int) (seconds / multiplier);
            if (result != 0 || !results.isEmpty()) {
                final String stringValue = String.valueOf(result);
                results.add(stringValue.length() < 2 ? "0" + stringValue : stringValue);
                seconds -= result * multiplier;
            }
        }

        return results.stream().collect(Collectors.joining(SPLIT_FIELD_CHAR));
    }
}
