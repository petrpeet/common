package cz.kb.bi.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This exception is used for spring MVC to change the response code of server into error code 404 {@link HttpStatus#NOT_FOUND}
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends FormattedRuntimeException {
    /**
     * Create new instance of exception with return code {@link HttpStatus#NOT_FOUND}
     *
     * @param message message of return
     * @param params  params for formatter
     */
    public NotFoundException(String message, Object... params) {
        super(message, params);
    }
}
