package cz.kb.bi.common.exception;

/**
 * Exception used for formatting input text using {@link String#format(String, Object...)}
 */
abstract class FormattedRuntimeException extends RuntimeException {
    /**
     * Constructs a new runtime exception with the specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    FormattedRuntimeException(String message, Object... params) {
        super(String.format(message, params));
    }
}
