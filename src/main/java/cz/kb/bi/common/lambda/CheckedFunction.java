package cz.kb.bi.common.lambda;

import java.util.function.Function;

/**
 * This interface can be used when function in lambda throws some checked exception. This interface wraps the exception
 * and then throw unchecked {@link IllegalStateException}.
 *
 * @param <T> input parameter of function
 * @param <R> returned parameter of function
 */
@FunctionalInterface
public interface CheckedFunction<T, R, E extends Exception> extends Function<T, R> {

    @Override
    default R apply(T t) {
        try {
            return applyThrows(t);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    R applyThrows(T t) throws E;
}
