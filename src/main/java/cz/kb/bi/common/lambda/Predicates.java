package cz.kb.bi.common.lambda;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * Predicates used in lambda for most common operatiions
 */
public class Predicates {
    private Predicates() {
        throw new IllegalStateException("Do not use default constructor");
    }

    /**
     * Predicate for not null object
     *
     * @param <T> type of object
     * @return true, if object is not null
     */
    public static <T> Predicate<T> notNull() {
        return Objects::nonNull;
    }

    /**
     * Predicate for null object
     *
     * @param <T> type of object
     * @return true, if object is null
     */
    public static <T> Predicate<T> isNull() {
        return Objects::isNull;
    }
}
