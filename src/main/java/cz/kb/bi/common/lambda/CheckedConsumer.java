package cz.kb.bi.common.lambda;

import java.util.function.Consumer;

/**
 * This interface can be used when consumer in lambda throws some checked exception. This interface wraps the exception
 * and then throw unchecked {@link IllegalStateException}.
 *
 * @param <T> input parameter of consumer
 */
@FunctionalInterface
public interface CheckedConsumer<T, E extends Exception> extends Consumer<T> {

    @Override
    default void accept(T t) {
        try {
            applyThrows(t);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    void applyThrows(T t) throws E;
}
