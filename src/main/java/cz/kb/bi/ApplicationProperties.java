package cz.kb.bi;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties("application")
public class ApplicationProperties {
    /**
     * Version of currently build application
     */
    private String version;
    /**
     * Original which is allowed to access application
     */
    private String allowOrigin;
    /**
     * The build number of current application
     */
    private String buildNumber;
}
