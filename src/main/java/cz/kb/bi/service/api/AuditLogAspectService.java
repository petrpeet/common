package cz.kb.bi.service.api;

import org.aspectj.lang.JoinPoint;

/**
 * Service used as a logger service for other services
 */
@FunctionalInterface
public interface AuditLogAspectService {

    /**
     * Log audit log entry defined by {@link JoinPoint}
     *
     * @param joinPoint joinPoint of aspect audit
     */
    void logAuditStep(JoinPoint joinPoint);

}

