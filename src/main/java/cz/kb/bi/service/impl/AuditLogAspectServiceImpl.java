package cz.kb.bi.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.kb.bi.common.domain.entity.InternalUrl;
import cz.kb.bi.common.factory.ObjectMapperFactory;
import cz.kb.bi.domain.dto.auditlog.AuditLogEntryDTO;
import cz.kb.bi.service.api.AuditLogAspectService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Service used for logging user information into database using internal REST service
 */
@Aspect
@Component
@Slf4j
class AuditLogAspectServiceImpl implements AuditLogAspectService {

    /**
     * Name of the controller in audit project, which should not be included into audit log,
     * because it would lead into infinite loop.
     */
    private static final String AUDIT_LOG_STORE_CONTROLLER = "AuditLogStoreController";
    /**
     * Rest template used for communication with audit project
     */
    private final RestTemplate restTemplate;
    private final InternalUrl internalUrl;
    /**
     * Mapper used for converting parameters into JSON object
     */
    private ObjectMapper objectMapper;
    /**
     * Name of the application, which should be logged
     */
    @Value("${spring.application.name}")
    private String applicationName;

    @Autowired
    public AuditLogAspectServiceImpl(RestTemplate restTemplate, InternalUrl internalUrl) {
        this.restTemplate = restTemplate;
        this.internalUrl = internalUrl;
    }

    /**
     * Get currently logged user from {@link SecurityContextHolder}. If user is unknown, or there is no security context
     * available, then return 'unidentified'.
     *
     * @return name of the current user
     */
    private static String getCurrentUser() {
        final String defaultUser = "unidentified";
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            return StringUtils.defaultIfBlank(auth.getName(), defaultUser);
        } catch (Exception e) {
            log.debug("Unable to get security context", e);
            return defaultUser;
        }
    }

    /**
     * Create new instnace of {@link ObjectMapper} and check, that {@link #applicationName} is not blank
     *
     * @throws NullPointerException     if {@link #applicationName} is null
     * @throws IllegalArgumentException {@link #applicationName} is empty
     */
    @PostConstruct
    public void init() {
        Validate.notBlank(applicationName, "Application Name cannot be blank");
        objectMapper = ObjectMapperFactory.createObjectMapper();

        log.info("Sending audit logs to: {}", internalUrl.getAuditLog());
    }

    /**
     * Log communication with server using AspectJ {@link JoinPoint}
     *
     * @param joinPoint joinPoint of aspect audit
     */
    @Override
    @Before("execution(* cz.kb.bi.*.controller.impl..*Controller.*(..))")
    public void logAuditStep(JoinPoint joinPoint) {
        final String auditLogUrl = internalUrl.getAuditLog();
        try {
            final Object targetBean = joinPoint.getTarget();
            final String className = targetBean.getClass().getSimpleName();
            if (!AUDIT_LOG_STORE_CONTROLLER.equalsIgnoreCase(className)) {
                final AuditLogEntryDTO logEntry = AuditLogEntryDTO.builder()
                        .userName(getCurrentUser())
                        .applicationName(applicationName)
                        .className(className)
                        .methodName(joinPoint.getSignature().getName())
                        .parametersValue(convertParameters(joinPoint.getArgs()))
                        .build();

                restTemplate.postForObject(auditLogUrl, logEntry, LocalDateTime.class);
            }
        } catch (Exception e) {
            log.error("Unable to store audit trace:" + auditLogUrl, e);
        }
    }

    /**
     * Convert method parameters into JSON file. In any problem occurs during conversion, try
     * to use {@link Arrays#toString(Object[])} instead.
     *
     * @param args input method parameters
     * @return JSON with converted values
     */
    private String convertParameters(Object[] args) {
        return Stream.of(args)
                .map(this::convertArgument)
                .collect(
                        Collectors.joining(",", "[", "]")
                );

    }

    /**
     * Write argument is JSON object
     *
     * @param argument argument to be serialized
     * @return serialized argument
     */
    private String convertArgument(Object argument) {
        try {
            if (argument != null) {
                if (argument instanceof HttpServletRequest) {
                    return ((HttpServletRequest) argument).getRequestURI();
                }
                return objectMapper.writeValueAsString(argument);
            } else {
                return "null";
            }
        } catch (JsonProcessingException e) {
            log.error("Unable to serialize arguments", e);
            return argument.toString();
        }
    }

    /**
     * Set the application name. This setter is used only by tests. Spring inject values directly
     *
     * @param applicationName application name to be set
     */
    void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

}
