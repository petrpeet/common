package cz.kb.bi.common.updownload;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.concurrent.ConcurrentHashMap;

@RunWith(MockitoJUnitRunner.class)
public class UpdownLoadQueueImplTest {

    @InjectMocks
    private UpdownLoadQueueImpl updownLoad;

    @Test
    public void checkDelete() throws IOException {
        Path tempFile = Files.createTempFile("pre1", "po");
        Path tempFile2 = Files.createTempFile("pre2", "po");
        Path tempFile3 = Files.createTempFile("pre3", "po");
        Path tempDirectory = Files.createTempDirectory("tempDirXX");
        Path tempFile4 = Files.createTempFile(tempDirectory, "pre4", "po");
        Path notExisting = Paths.get("not_existing/path/so/it/cant/be/deleted");


        updownLoad.addFileToDelQueue(tempFile, LocalDateTime.now().minusMinutes(1));
        updownLoad.addFileToDelQueue(tempFile2, LocalDateTime.now().minusMinutes(1));
        updownLoad.addFileToDelQueue(notExisting, LocalDateTime.now().minusMinutes(1));
        updownLoad.addFileToDelQueue(tempFile3);
        updownLoad.addFileToDelQueue(tempDirectory, LocalDateTime.now().minusMinutes(1));

        updownLoad.cleanFiles();

        Assert.assertFalse(tempFile.toFile().exists());
        Assert.assertFalse(tempFile2.toFile().exists());
        Assert.assertFalse(tempDirectory.toFile().exists());
        Assert.assertTrue(tempFile3.toFile().exists());

        Files.deleteIfExists(tempFile3);
    }

    @Test
    public void checkDeleteMyQueue() throws IOException {
        ConcurrentHashMap<Path, LocalDateTime> hashMap = new ConcurrentHashMap<>();
        Path tempFile = Files.createTempFile("pre1", "po");
        Path tempFile2 = Files.createTempFile("pre2", "po");
        Path tempFile3 = Files.createTempFile("pre3", "po");
        Path tempFile4 = Files.createTempFile("pre4", "po");
        Path tempDirectory = Files.createTempDirectory("tempDirXX");
        Path tempFile5 = Files.createTempFile(tempDirectory, "pre4", "po");
        Path notExisting = Paths.get("not_existing/path/so/it/cant/be/deleted");

        updownLoad.addFileToDelQueue(tempFile, LocalDateTime.now().minusMinutes(1), hashMap);
        updownLoad.addFileToDelQueue(tempFile2, LocalDateTime.now().minusMinutes(1), hashMap);
        updownLoad.addFileToDelQueue(tempFile4, LocalDateTime.now().minusMinutes(1));
        updownLoad.addFileToDelQueue(notExisting, LocalDateTime.now().minusMinutes(1), hashMap);
        updownLoad.addFileToDelQueue(tempDirectory, LocalDateTime.now().minusMinutes(1), hashMap);
        updownLoad.addFileToDelQueue(tempFile3, hashMap);

        updownLoad.cleanFiles(hashMap);

        Assert.assertFalse(tempFile.toFile().exists());
        Assert.assertFalse(tempFile2.toFile().exists());
        Assert.assertFalse(tempDirectory.toFile().exists());
        Assert.assertTrue(tempFile3.toFile().exists());
        Assert.assertTrue(tempFile4.toFile().exists());

        Files.deleteIfExists(tempFile3);
        Files.deleteIfExists(tempFile4);
    }

    @Test
    public void testForceClean() throws IOException {
        Path tempFile = Files.createTempFile("pre1", "po");
        Path tempFile2 = Files.createTempFile("pre2", "po");
        Path tempFile3 = Files.createTempFile("pre3", "po");
        updownLoad.addFileToDelQueue(tempFile, LocalDateTime.now().minusMinutes(1));
        updownLoad.addFileToDelQueue(tempFile2, LocalDateTime.now().minusMinutes(1));
        updownLoad.addFileToDelQueue(tempFile3);
        updownLoad.forceClean();
        Assert.assertFalse(tempFile.toFile().exists());
        Assert.assertFalse(tempFile2.toFile().exists());
        Assert.assertFalse(tempFile3.toFile().exists());
    }
}
