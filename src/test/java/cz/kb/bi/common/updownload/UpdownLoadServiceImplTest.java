package cz.kb.bi.common.updownload;

import cz.kb.bi.common.utils.ZipUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ZipUtils.class})
public class UpdownLoadServiceImplTest {

    @InjectMocks
    private UpdownLoadServiceImpl updownLoadService;

    @Mock
    private UpdownLoadQueueService updownLoadQueueService;

    @Mock
    private ConcurrentHashMap<Path, LocalDateTime> hashMap;

    @Mock
    private MultipartFile multipartFile;

    private Path path;

    @Mock
    private List<Path> mockedPathList;

    @Mock
    private Path mockPath;

    @Mock
    private Path mockParent;

    public void prepare() throws IOException {
        path = Files.createTempFile("ScriptZipControllerTest", "checkZipDownload");
        PowerMockito.mockStatic(ZipUtils.class);
        PowerMockito.when(
                ZipUtils.createZipFile(Mockito.eq(Collections.singletonList("path1")), Mockito.anyString())
        ).thenReturn(path);
    }

    private void assertResponse(ResponseEntity<Resource> resourceResponseEntity) throws IOException {
        Assert.assertEquals(HttpStatus.OK, resourceResponseEntity.getStatusCode());
        Assert.assertEquals(MediaType.parseMediaType("application/octet-stream"),
                resourceResponseEntity.getHeaders().getContentType());
        Assert.assertNotNull(resourceResponseEntity.getBody());
        resourceResponseEntity.getBody().getInputStream().close();
    }

    @Test
    public void testZipAndDeleteAll() throws IOException {
        prepare();
        ResponseEntity<Resource> resourceResponseEntity = updownLoadService.downloadResponse(Collections.singletonList("path1"), true);
        Mockito.verify(updownLoadQueueService).addFileToDelQueue(path);
        Mockito.verify(updownLoadQueueService).addFileToDelQueue(Mockito.eq(Paths.get("path1")));

        assertResponse(resourceResponseEntity);

        Files.delete(path);
    }

    @Test
    public void testZipAndDeleteZip() throws IOException {
        prepare();
        ResponseEntity<Resource> resourceResponseEntity = updownLoadService.downloadResponse(Collections.singletonList("path1"), false);
        Mockito.verify(updownLoadQueueService).addFileToDelQueue(path);
        Mockito.verify(updownLoadQueueService, Mockito.times(0)).addFileToDelQueue(Mockito.eq(Paths.get("path1")));

        assertResponse(resourceResponseEntity);

        Files.delete(path);
    }

    @Test
    public void testZipAndDeleteAllQueue() throws IOException {
        prepare();
        ResponseEntity<Resource> resourceResponseEntity = updownLoadService.downloadResponse(Collections.singletonList("path1"), true, hashMap);
        Mockito.verify(updownLoadQueueService).addFileToDelQueue(path, hashMap);
        Mockito.verify(updownLoadQueueService).addFileToDelQueue(Mockito.eq(Paths.get("path1")), Mockito.eq(hashMap));

        assertResponse(resourceResponseEntity);

        Files.delete(path);
    }

    @Test
    public void testZipAndDeleteZipQueue() throws IOException {
        prepare();
        ResponseEntity<Resource> resourceResponseEntity = updownLoadService.downloadResponse(Collections.singletonList("path1"), false, hashMap);
        Mockito.verify(updownLoadQueueService).addFileToDelQueue(path, hashMap);
        Mockito.verify(updownLoadQueueService, Mockito.times(0)).addFileToDelQueue(Mockito.eq(Paths.get("path1")), Mockito.eq(hashMap));

        assertResponse(resourceResponseEntity);

        Files.delete(path);
    }

    @Test
    public void testDownloadWithoutDelete() throws IOException {
        ResponseEntity<Resource> resourceResponseEntity = updownLoadService.downloadResponse("src/test/resources/cz/kb/bi/common/utils/script1.txt", false);
        Mockito.verify(updownLoadQueueService, Mockito.times(0)).addFileToDelQueue(Mockito.eq(Paths.get("src/test/resources/cz/kb/bi/common/utils/script1.txt")));
        assertResponse(resourceResponseEntity);
    }

    @Test
    public void testDownloadWithDelete() throws IOException {
        ResponseEntity<Resource> resourceResponseEntity = updownLoadService.downloadResponse("src/test/resources/cz/kb/bi/common/utils/script1.txt", true);
        Mockito.verify(updownLoadQueueService, Mockito.times(1)).addFileToDelQueue(Mockito.eq(Paths.get("src/test/resources/cz/kb/bi/common/utils/script1.txt")));
        assertResponse(resourceResponseEntity);
    }

    @Test
    public void testDownloadWithoutDeleteQueue() throws IOException {
        ResponseEntity<Resource> resourceResponseEntity = updownLoadService.downloadResponse("src/test/resources/cz/kb/bi/common/utils/script1.txt", false, hashMap);
        Mockito.verify(updownLoadQueueService, Mockito.times(0))
                .addFileToDelQueue(Mockito.eq(Paths.get("src/test/resources/cz/kb/bi/common/utils/script1.txt")), Mockito.eq(hashMap));
        assertResponse(resourceResponseEntity);
    }

    @Test
    public void testDownloadWithDeleteQueue() throws IOException {
        ResponseEntity<Resource> resourceResponseEntity = updownLoadService.downloadResponse("src/test/resources/cz/kb/bi/common/utils/script1.txt", true, hashMap);
        Mockito.verify(updownLoadQueueService, Mockito.times(1))
                .addFileToDelQueue(Mockito.eq(Paths.get("src/test/resources/cz/kb/bi/common/utils/script1.txt")), Mockito.eq(hashMap));
        assertResponse(resourceResponseEntity);
    }

    private void assertUpload(Path result) throws IOException {
        byte[] readedBytes = Files.readAllBytes(result);
        Assert.assertThat(new byte[]{1,5,15,20}, Matchers.equalTo(Files.readAllBytes(result)));
        Files.delete(result);
    }

    @Test
    public void testUpload() throws IOException {
        Mockito.when(multipartFile.getBytes()).thenReturn(new byte[]{1,5,15,20});
        Path result = updownLoadService.uploadFile(multipartFile, true);
        Mockito.verify(updownLoadQueueService, Mockito.times(1)).addFileToDelQueue(Mockito.eq(result));
        assertUpload(result);
    }

    @Test
    public void testUploadWithoutDelete() throws IOException {
        Mockito.when(multipartFile.getBytes()).thenReturn(new byte[]{1,5,15,20});
        Path result = updownLoadService.uploadFile(multipartFile, false);
        Mockito.verify(updownLoadQueueService, Mockito.times(0)).addFileToDelQueue(Mockito.eq(result));
        assertUpload(result);
    }

    @Test
    public void testUploadQueue() throws IOException {
        Mockito.when(multipartFile.getBytes()).thenReturn(new byte[]{1,5,15,20});
        Path result = updownLoadService.uploadFile(multipartFile, true, hashMap);
        Mockito.verify(updownLoadQueueService, Mockito.times(1)).addFileToDelQueue(Mockito.eq(result), Mockito.eq(hashMap));
        assertUpload(result);
    }

    @Test
    public void testUploadQueueWithoutDelete() throws IOException {
        Mockito.when(multipartFile.getBytes()).thenReturn(new byte[]{1,5,15,20});
        Path result = updownLoadService.uploadFile(multipartFile, false, hashMap);
        Mockito.verify(updownLoadQueueService, Mockito.times(0)).addFileToDelQueue(Mockito.eq(result), Mockito.eq(hashMap));
        assertUpload(result);
    }

    @Test
    public void testUnzipWithoutDelete() throws IOException {
        PowerMockito.mockStatic(ZipUtils.class);
        PowerMockito.when(
                ZipUtils.unzipFile(path)
        ).thenReturn(mockedPathList);
        List<Path> result = updownLoadService.uploadZipFile(path, false);
        Mockito.verify(updownLoadQueueService, Mockito.times(0)).addFileToDelQueue(path);
        Assert.assertThat(result, Matchers.equalTo(mockedPathList));

    }

    @Test
    public void testUnzipWithDelete() throws IOException {
        PowerMockito.mockStatic(ZipUtils.class);
        PowerMockito.when(
                ZipUtils.unzipFile(path)
        ).thenReturn(Collections.singletonList(Paths.get("src/test/resources/cz/kb")));
        List<Path> result = updownLoadService.uploadZipFile(path, true);
        Mockito.verify(updownLoadQueueService).addFileToDelQueue(path);
        Mockito.verify(updownLoadQueueService).addFileToDelQueue(Mockito.eq(Paths.get("src/test/resources/cz")));
        Assert.assertThat(result, Matchers.contains(
                Matchers.equalTo(Paths.get("src/test/resources/cz/kb")
        )));
    }

    @Test
    public void testUnzipWithoutDeleteWithQueue() throws IOException {
        PowerMockito.mockStatic(ZipUtils.class);
        PowerMockito.when(
                ZipUtils.unzipFile(path)
        ).thenReturn(mockedPathList);
        List<Path> result = updownLoadService.uploadZipFile(path, false, hashMap);
        Mockito.verify(updownLoadQueueService, Mockito.times(0)).addFileToDelQueue(path, hashMap);
        Assert.assertThat(result, Matchers.equalTo(mockedPathList));
    }

    @Test
    public void testUnzipWithDeleteWithQueue() throws IOException {
        PowerMockito.mockStatic(ZipUtils.class);
        PowerMockito.when(
                ZipUtils.unzipFile(path)
        ).thenReturn(Collections.singletonList(Paths.get("src/test/resources/cz/kb")));
        List<Path> result = updownLoadService.uploadZipFile(path, true, hashMap);
        Mockito.verify(updownLoadQueueService).addFileToDelQueue(path, hashMap);
        Mockito.verify(updownLoadQueueService).addFileToDelQueue(Mockito.eq(Paths.get("src/test/resources/cz")), Mockito.eq(hashMap));
        Assert.assertThat(result, Matchers.contains(
                Matchers.equalTo(Paths.get("src/test/resources/cz/kb")
                )));
    }

}
