package cz.kb.bi.common.lambda;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import static cz.kb.bi.common.lambda.Predicates.isNull;
import static cz.kb.bi.common.lambda.Predicates.notNull;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class PredicatesTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void checkThatNotNullPredicateIsCorrect() throws Exception {
        List<String> retStrings = asList(null, "a", "b", null, "c", null, null)
                .stream()
                .filter(notNull())
                .collect(toList());
        assertThat(
                retStrings,
                contains(
                        "a", "b", "c"
                )
        );
    }

    @SuppressWarnings("unchecked")
    @Test
    public void checkThatNullPredicateIsCorrect() throws Exception {
        List<String> retStrings = asList(null, "a", "b", null, "c", null, null)
                .stream()
                .filter(isNull())
                .collect(toList());
        assertThat(
                retStrings,
                allOf(
                        contains(
                                nullValue(),
                                nullValue(),
                                nullValue(),
                                nullValue()
                        )
                )
        );
    }

    @Test
    public void checkThatConstructorWillFail()
            throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        expectedException.expect(InvocationTargetException.class);

        Constructor<Predicates> c = Predicates.class.getDeclaredConstructor();
        c.setAccessible(true);
        c.newInstance();
    }
}