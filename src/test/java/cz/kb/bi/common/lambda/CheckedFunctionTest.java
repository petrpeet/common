package cz.kb.bi.common.lambda;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class CheckedFunctionTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static void verifyThatConversionIsCorrect(CheckedFunction<String, Integer, Exception> checkedFunction) {
        List<Integer> results =
                Stream.of("1", "2").map(checkedFunction).collect(toList());
        assertThat(
                results,
                contains(1, 2)
        );
    }

    protected static Integer throwingFunction(String string) throws Exception {
        return Integer.valueOf(string);
    }

    @Test
    public void checkThatFunctionsWorksWell() throws Exception {
        verifyThatConversionIsCorrect(Integer::valueOf);
    }

    @Test
    public void checkThatThrowingFunctionWorksWell() throws Exception {
        verifyThatConversionIsCorrect(CheckedFunctionTest::throwingFunction);
    }

    @Test
    public void checkThatFunctionWillThrowIllegalStateException() throws Exception {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage(containsString("Test"));
        verifyThatConversionIsCorrect(
                i -> {
                    throw new Exception("Test");
                }
        );
    }
}