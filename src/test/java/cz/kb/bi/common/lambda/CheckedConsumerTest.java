package cz.kb.bi.common.lambda;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class CheckedConsumerTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private List<String> copyList;

    private static void verifyThatConversionIsCorrect(CheckedConsumer<String, Exception> checkedConsumer) {
        asList("1", "2").forEach(checkedConsumer);
    }

    @Before
    public void setUp() throws Exception {
        copyList = new ArrayList<>();
    }

    protected void throwingConsumer(String string) throws Exception {
        copyList.add(string);
    }

    @Test
    public void checkThatThrowingConsumerWorksWell() throws Exception {
        verifyThatConversionIsCorrect(this::throwingConsumer);
        assertThat(
                copyList,
                contains("1", "2")
        );
    }

    @Test
    public void checkThatFunctionWillThrowIllegalStateException() throws Exception {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage(containsString("Test"));
        verifyThatConversionIsCorrect(
                i -> {
                    throw new Exception("Test");
                }
        );
    }

}