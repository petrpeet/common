package cz.kb.bi.common.typehandler;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@RunWith(MockitoJUnitRunner.class)
public class PathTypeHandlerTest {


    @InjectMocks
    private PathTypeHandler pathTypeHandler;

    @Mock
    private ResultSet resultSet;
    @Mock
    private CallableStatement callableStatement;
    @Mock
    private PreparedStatement preparedStatement;

    private Path path = Paths.get("/");


    @Before
    public void init() throws Exception {
        Mockito.when(resultSet.getString(1)).thenReturn("/");
        Mockito.when(resultSet.getString("asd")).thenReturn("/");
        Mockito.when(callableStatement.getString(1)).thenReturn("/");
    }

    @Test
    public void setNonNullParameter() throws Exception {
        pathTypeHandler.setNonNullParameter(preparedStatement, 2, path, null);

        Mockito.verify(preparedStatement).setString(2, "//");
    }

    @Test
    public void setNonNullParameterInvalidSlash() throws Exception {
        Path customPath = Paths.get("\\");
        pathTypeHandler.setNonNullParameter(preparedStatement, 2, customPath, null);

        Mockito.verify(preparedStatement).setString(2, "//");
    }


    @Test
    public void setNonNullParameterBeginsWithTwoSlashes() throws Exception {
        Path customPath = Paths.get("//vsfs10.ds.kb.cz/ifpcshare/InformaticaServer/Cmd/CPStage/CP_prepare.cmd");
        pathTypeHandler.setNonNullParameter(preparedStatement, 2, customPath, null);

        Mockito.verify(preparedStatement)
                .setString(
                        Mockito.eq(2),
                        Mockito.endsWith("//vsfs10.ds.kb.cz/ifpcshare/InformaticaServer/Cmd/CPStage/CP_prepare.cmd")
                );
    }

    @Test
    public void getNullableResult() throws Exception {
        Path nullableResult = pathTypeHandler.getNullableResult(resultSet, "asd");

        Assert.assertEquals(
                path,
                nullableResult
        );

    }

    @Test
    public void getNullableResultIndex() throws Exception {
        Path nullableResult = pathTypeHandler.getNullableResult(resultSet, 1);

        Assert.assertEquals(
                path,
                nullableResult
        );
    }

    @Test
    public void getNullableResultCallable() throws Exception {
        Path nullableResult = pathTypeHandler.getNullableResult(callableStatement, 1);

        Assert.assertEquals(
                path,
                nullableResult
        );
    }

    @Test
    public void checkReturnsNull() throws Exception {
        Mockito.when(resultSet.getString("asd")).thenReturn(null);

        Path nullableResult = pathTypeHandler.getNullableResult(resultSet, "asd");

        Assert.assertEquals(
                null,
                nullableResult
        );
    }

    @Test
    public void checkReturnsEmpty() throws Exception {
        Mockito.when(resultSet.getString("asd")).thenReturn("");

        Path nullableResult = pathTypeHandler.getNullableResult(resultSet, "asd");

        Assert.assertEquals(
                Paths.get(""),
                nullableResult
        );
    }

}