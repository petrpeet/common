package cz.kb.bi.common.typehandler;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.CallableStatement;
import java.sql.ResultSet;

@RunWith(MockitoJUnitRunner.class)
public class TrimSpaceEnumTypeHandlerTest {


    private TrimSpaceEnumTypeHandler<SwagLevel> trimSpaceEnumTypeHandler = new TrimSpaceEnumTypeHandler<>(SwagLevel.class);
    @Mock
    private ResultSet resultSet;
    @Mock
    private CallableStatement callableStatement;

    @Test
    public void checkConversionWithoutSpace() throws Exception {
        Mockito.when(resultSet.getString("COOL")).thenReturn("COOL");

        SwagLevel cool = trimSpaceEnumTypeHandler.getNullableResult(resultSet, "COOL");

        Assert.assertEquals(SwagLevel.COOL, cool);
    }

    @Test
    public void checkConversionWithSpace() throws Exception {
        Mockito.when(resultSet.getString("COOL")).thenReturn("COOL ");

        SwagLevel cool = trimSpaceEnumTypeHandler.getNullableResult(resultSet, "COOL");

        Assert.assertEquals(SwagLevel.COOL, cool);
    }

    @Test
    public void checkConversionNull() throws Exception {
        Mockito.when(resultSet.getString("COOL")).thenReturn(null);

        SwagLevel cool = trimSpaceEnumTypeHandler.getNullableResult(resultSet, "COOL");

        Assert.assertEquals(null, cool);
    }

    @Test
    public void checkConversionFromIndex() throws Exception {
        Mockito.when(resultSet.getString(1)).thenReturn("NOT_COOL");

        SwagLevel cool = trimSpaceEnumTypeHandler.getNullableResult(resultSet, 1);

        Assert.assertEquals(SwagLevel.NOT_COOL, cool);
    }

    @Test
    public void checkConversionFromIndexFromCallable() throws Exception {
        Mockito.when(callableStatement.getString(1)).thenReturn("NOT_COOL");

        SwagLevel cool = trimSpaceEnumTypeHandler.getNullableResult(callableStatement, 1);

        Assert.assertEquals(SwagLevel.NOT_COOL, cool);
    }

    enum SwagLevel {
        COOL,
        NOT_COOL
    }

}