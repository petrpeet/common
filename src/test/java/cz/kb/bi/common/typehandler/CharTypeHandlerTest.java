package cz.kb.bi.common.typehandler;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


@RunWith(MockitoJUnitRunner.class)
public class CharTypeHandlerTest {

    @InjectMocks
    private CharTypeHandler charTypeHandler;

    @Mock
    private ResultSet resultSet;
    @Mock
    private CallableStatement callableStatement;
    @Mock
    private PreparedStatement preparedStatement;

    @Before
    public void init() throws Exception {
        Mockito.when(resultSet.getString(1)).thenReturn(" /");
        Mockito.when(resultSet.getString("asd")).thenReturn("/ ");
        Mockito.when(callableStatement.getString(1)).thenReturn("/");
    }

    @Test
    public void setNonNullParameter() throws Exception {
        charTypeHandler.setNonNullParameter(preparedStatement, 2, " x", null);

        Mockito.verify(preparedStatement).setString(2, "x");
    }

    @Test
    public void getNullableResult() throws Exception {
        String nullableResult = charTypeHandler.getNullableResult(resultSet, "asd");

        Assert.assertEquals(
                "/",
                nullableResult
        );

    }

    @Test
    public void getNullableResultIndex() throws Exception {
        String nullableResult = charTypeHandler.getNullableResult(resultSet, 1);

        Assert.assertEquals(
                "/",
                nullableResult
        );
    }

    @Test
    public void getNullableResultCallable() throws Exception {
        String nullableResult = charTypeHandler.getNullableResult(callableStatement, 1);

        Assert.assertEquals(
                "/",
                nullableResult
        );
    }

    @Test
    public void checkReturnsNull() throws Exception {
        Mockito.when(resultSet.getString("asd")).thenReturn(null);

        String nullableResult = charTypeHandler.getNullableResult(resultSet, "asd");

        Assert.assertEquals(
                null,
                nullableResult
        );
    }
}