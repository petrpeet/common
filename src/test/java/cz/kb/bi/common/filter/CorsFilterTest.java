package cz.kb.bi.common.filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RunWith(MockitoJUnitRunner.class)
public class CorsFilterTest {

    @InjectMocks
    private CorsFilter corsFilter;

    @Mock
    private HttpServletRequest req;

    @Mock
    private HttpServletResponse res;

    @Mock
    private FilterChain chain;

    @Mock
    private FilterConfig filterConfig;

    @Before
    public void setUp() throws Exception {
        Mockito.when(req.getRequestURI()).thenReturn("/teradata");
    }

    @Test
    public void checkInit() throws Exception {
        corsFilter.init(filterConfig);

        Mockito.verifyZeroInteractions(filterConfig);
    }

    @Test
    public void checkDestroy() throws Exception {
        corsFilter.destroy();

        Mockito.verifyZeroInteractions(req, res, chain, filterConfig);
    }

    @Test
    public void checkThatChainIsCalledOnNotOptions() throws Exception {
        Mockito.when(req.getMethod()).thenReturn("GET");

        corsFilter.doFilter(req, res, chain);

        Mockito.verify(chain).doFilter(req, res);
    }

    @Test
    public void checkHeadersOnChain() throws Exception {
        Mockito.when(req.getMethod()).thenReturn("GET");

        corsFilter.doFilter(req, res, chain);

        Mockito.verify(res).setHeader("Access-Control-Allow-Origin", "*");
        Mockito.verify(res).setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        Mockito.verify(res).setHeader("Access-Control-Allow-Headers", "x-requested-with");
        Mockito.verify(res).setHeader("Access-Control-Max-Age", "3600");
    }

    @Test
    public void checkHeadersOnOptions() throws Exception {
        Mockito.when(req.getMethod()).thenReturn("Options");

        corsFilter.doFilter(req, res, chain);

        Mockito.verify(chain).doFilter(Mockito.eq(req), Mockito.eq(res));
    }

}