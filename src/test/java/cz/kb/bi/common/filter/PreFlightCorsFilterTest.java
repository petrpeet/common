package cz.kb.bi.common.filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RunWith(MockitoJUnitRunner.class)
public class PreFlightCorsFilterTest {


    @InjectMocks
    private PreFlightCorsFilter corsFilter;

    @Mock
    private HttpServletRequest req;

    @Mock
    private HttpServletResponse res;

    @Mock
    private FilterChain chain;

    @Mock
    private FilterConfig filterConfig;

    @Before
    public void setUp() throws Exception {
        Mockito.when(req.getMethod()).thenReturn("OPTIONS");
    }

    @Test
    public void checkInit() throws Exception {
        corsFilter.init(filterConfig);

        Mockito.verifyZeroInteractions(filterConfig);
    }

    @Test
    public void checkDestroy() throws Exception {
        corsFilter.destroy();

        Mockito.verifyZeroInteractions(req, res, chain, filterConfig);
    }

    @Test
    public void checkOptionsRequest() throws Exception {
        corsFilter.doFilter(req, res, chain);

        Mockito.verify(res).setHeader("Access-Control-Allow-Methods", "POST,GET,DELETE,PUT");
        Mockito.verify(res).setHeader("Access-Control-Max-Age", "3600");
        Mockito.verify(res).setHeader("Access-Control-Allow-Headers", "authorization, content-type," +
                "access-control-request-headers,access-control-request-method,accept,origin,authorization,x-requested-with");
        Mockito.verify(res).setStatus(HttpServletResponse.SC_OK);
    }

    @Test
    public void checkFilterOnOptions() throws Exception {
        corsFilter.doFilter(req, res, chain);

        Mockito.verifyZeroInteractions(chain);
    }

    @Test
    public void checkNonOptionsRequest() throws Exception {
        Mockito.when(req.getMethod()).thenReturn("GET");

        corsFilter.doFilter(req, res, chain);

        Mockito.verifyZeroInteractions(res);
        Mockito.verify(chain).doFilter(req, res);
    }

}