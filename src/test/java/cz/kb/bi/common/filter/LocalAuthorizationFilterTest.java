package cz.kb.bi.common.filter;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class LocalAuthorizationFilterTest {

    private LocalAuthorizationFilter localAuthorizationFilter = new LocalAuthorizationFilter();

    @Mock
    private SecurityContext securityContext;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private FilterChain filterChain;

    @Captor
    private ArgumentCaptor<ServletRequest> servletRequestArgumentCaptor;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(SecurityContextHolder.class);
        PowerMockito.when(SecurityContextHolder.getContext()).thenReturn(securityContext);
    }

    @Test
    public void checkSettingOfNewUser() throws Exception {
        localAuthorizationFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(securityContext).setAuthentication(
                Mockito.eq(
                        new TestingAuthenticationToken(
                                "fenix",
                                "fenix",
                                "ROLE_AI_USERS_4800", "ROLE_AI_USERS_4800_DIC"
                        )
                )
        );
    }

    @Test
    public void checkCallingOfChain() throws Exception {
        localAuthorizationFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);

        Mockito.verify(filterChain).doFilter(Mockito.any(), Mockito.eq(httpServletResponse));
    }

    @Test
    public void checkBehaviorOfRequest() throws Exception {
        Mockito.when(httpServletRequest.getHeader("Authorization")).thenReturn("Test value");
        HttpServletRequest chainedRequest = runFiltering();
        Assert.assertNull(chainedRequest.getHeader("Authorization"));
    }

    @Test
    public void checkBehaviorOfRequest2() throws Exception {
        Mockito.when(httpServletRequest.getHeader("NewHeader")).thenReturn("Test value");
        HttpServletRequest chainedRequest = runFiltering();
        Assert.assertEquals(
                "Test value",
                chainedRequest.getHeader("NewHeader")
        );
    }


    @Test
    public void checkDate() throws Exception {
        final Date testDate = new Date();
        Mockito.when(httpServletRequest.getDateHeader("Authorization")).thenReturn(testDate.getTime());
        HttpServletRequest chainedRequest = runFiltering();
        Assert.assertEquals(
                0L,
                chainedRequest.getDateHeader("Authorization"));
    }

    @Test
    public void checkDate2() throws Exception {
        final long testDate = new Date().getTime();
        Mockito.when(httpServletRequest.getDateHeader("TestHeader")).thenReturn(testDate);
        HttpServletRequest chainedRequest = runFiltering();
        Assert.assertEquals(
                testDate,
                chainedRequest.getDateHeader("TestHeader"));
    }

    @Test
    public void checkHeaders() throws Exception {
        Enumeration<String> testValue = Collections.enumeration(Collections.singletonList("testValue"));
        Mockito.when(httpServletRequest.getHeaders("Authorization")).thenReturn(testValue);
        HttpServletRequest chainedRequest = runFiltering();
        Assert.assertEquals(
                Collections.emptyEnumeration(),
                chainedRequest.getHeaders("Authorization")
        );
    }

    @Test
    public void checkHeaders2() throws Exception {
        Enumeration<String> testValue = Collections.enumeration(Collections.singletonList("testValue"));
        Mockito.when(httpServletRequest.getHeaders("TestHeader")).thenReturn(testValue);
        HttpServletRequest chainedRequest = runFiltering();
        Assert.assertEquals(
                testValue,
                chainedRequest.getHeaders("TestHeader"));
    }


    @Test
    public void checkInt() throws Exception {
        final int testValues = 42;
        Mockito.when(httpServletRequest.getIntHeader("Authorization")).thenReturn(testValues);
        HttpServletRequest chainedRequest = runFiltering();
        Assert.assertEquals(
                0,
                chainedRequest.getIntHeader("Authorization"));
    }

    @Test
    public void checkInt2() throws Exception {
        final int testValues = 42;
        Mockito.when(httpServletRequest.getIntHeader("TestHeader")).thenReturn(testValues);
        HttpServletRequest chainedRequest = runFiltering();
        Assert.assertEquals(
                testValues,
                chainedRequest.getIntHeader("TestHeader"));
    }

    @Test
    public void checkHeadersNames() throws Exception {
        Mockito.when(httpServletRequest.getHeaderNames()).thenReturn(
                Collections.enumeration(Arrays.asList("Authorization", "TestHeader", "NewHeader"))
        );
        HttpServletRequest httpServletRequest = runFiltering();
        Assert.assertThat(
                Collections.list(httpServletRequest.getHeaderNames()),
                Matchers.contains("TestHeader", "NewHeader")
        );
    }

    private HttpServletRequest runFiltering() throws Exception {
        localAuthorizationFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
        Mockito.verify(filterChain).doFilter(servletRequestArgumentCaptor.capture(), Mockito.any());
        return (HttpServletRequest) servletRequestArgumentCaptor.getValue();
    }
}