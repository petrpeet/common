package cz.kb.bi.common.rest;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.HttpClientErrorException;

import java.io.ByteArrayInputStream;

@RunWith(MockitoJUnitRunner.class)
public class StatusErrorHandlerTest {

    @Mock
    private ClientHttpResponse clientHttpResponse;

    private StatusErrorHandler statusErrorHandler = new StatusErrorHandler();

    @Mock
    private Appender<ILoggingEvent> mockAppender;

    @Captor
    private ArgumentCaptor<ILoggingEvent> captor;

    @Before
    public void setUp() throws Exception {
        Mockito.when(clientHttpResponse.getStatusCode()).thenReturn(HttpStatus.BAD_REQUEST);
        Mockito.when(clientHttpResponse.getRawStatusCode()).thenReturn(400);
        Mockito.when(clientHttpResponse.getHeaders()).thenReturn(new HttpHeaders());
        Mockito.when(clientHttpResponse.getBody()).thenAnswer(i -> new ByteArrayInputStream("{\"message\":\"Wrong message type\"}".getBytes()));

        Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);

        Mockito.when(mockAppender.getName()).thenReturn("MOCK");
        root.addAppender(mockAppender);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void checkBadMessageFormat() throws Exception {
        Mockito.when(clientHttpResponse.getBody()).thenAnswer(i -> new ByteArrayInputStream("Hello".getBytes()));
        try {
            statusErrorHandler.handleError(clientHttpResponse);
        } catch (HttpClientErrorException ex) {
            // Do nothing
        }
        Mockito.verify(mockAppender, Mockito.times(2)).doAppend(captor.capture());
        Assert.assertThat(
                captor.getAllValues(),
                Matchers.containsInAnyOrder(
                        Matchers.allOf(
                                Matchers.hasProperty("level", Matchers.equalTo(Level.ERROR)),
                                Matchers.hasProperty("message", Matchers.equalTo("Unexpected read error"))
                        ), Matchers.allOf(
                                Matchers.hasProperty("level", Matchers.equalTo(Level.ERROR)),
                                Matchers.hasProperty("message", Matchers.equalTo(""))
                        )
                )
        );
    }

    @Test
    public void checkThatEmptyStreamDoesNotFail() throws Exception {
        Mockito.when(clientHttpResponse.getBody()).thenAnswer(i -> new ByteArrayInputStream("".getBytes()));
        try {
            statusErrorHandler.handleError(clientHttpResponse);
        } catch (HttpClientErrorException ex) {
            // Do nothing
        }
        Mockito.verify(mockAppender, Mockito.times(1)).doAppend(captor.capture());
        Assert.assertThat(
                captor.getAllValues(),
                Matchers.contains(
                        Matchers.allOf(
                                Matchers.hasProperty("level", Matchers.equalTo(Level.ERROR)),
                                Matchers.hasProperty("message", Matchers.equalTo(""))
                        )
                )
        );
    }

    @Test
    public void checkLogParse() throws Exception {
        boolean exRaised = false;
        try {
            statusErrorHandler.handleError(clientHttpResponse);
        } catch (HttpClientErrorException ex) {
            exRaised = true;
        }
        Assert.assertTrue(exRaised);
        Mockito.verify(mockAppender).doAppend(captor.capture());
        Assert.assertThat(
                captor.getValue(),
                Matchers.allOf(
                        Matchers.hasProperty("level", Matchers.equalTo(Level.ERROR)),
                        Matchers.hasProperty("message", Matchers.equalTo("Wrong message type"))
                )
        );
    }

}