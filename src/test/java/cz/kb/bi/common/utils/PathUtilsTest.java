package cz.kb.bi.common.utils;

import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Paths;

public class PathUtilsTest {

    @Test
    public void nullPath() throws Exception {
        Assert.assertNull(
                PathUtils.pathToDisplay(null)
        );
    }

    @Test
    public void networkPath() throws Exception {
        Assert.assertEquals(
                "//vsfs10.ds.kb.cz/CtlFiles/Test",
                PathUtils.pathToDisplay(Paths.get("\\\\vsfs10.ds.kb.cz\\CtlFiles\\Test"))
        );

    }

    @Test
    public void checkIsNetworkPath() throws Exception {
        Assert.assertTrue(PathUtils.isNetworkPath(Paths.get("//vsfs10.ds.kb.cz/CtlFiles/Test")));
    }

    @Test
    public void checkIsNotNetworkPath() throws Exception {
        Assert.assertFalse(PathUtils.isNetworkPath(Paths.get("n:/vsfs10.ds.kb.cz/CtlFiles/Test")));
    }

    @Test
    public void checkFormatOfWindowsPath() throws Exception {
        Assert.assertEquals(
                "c:/Java/BI-API-PROD/bteq",
                PathUtils.pathToDisplay(Paths.get("c:\\Java\\BI-API-PROD\\bteq"))
        );
    }

    @Test
    public void checkFormatOfWindowsPath2() throws Exception {
        Assert.assertEquals(
                "c:/Java/BI-API-PROD/bteq",
                PathUtils.pathToDisplay(Paths.get("c:/Java/BI-API-PROD/bteq"))
        );
    }
}