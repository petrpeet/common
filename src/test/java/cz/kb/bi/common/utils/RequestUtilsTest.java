package cz.kb.bi.common.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Constructor;


@RunWith(MockitoJUnitRunner.class)
public class RequestUtilsTest {

    @Mock
    private HttpServletRequest request;


    @Test
    public void getParameter() {
        Mockito.when(request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE)).thenReturn(
                "/prefix/path"
        );

        String parameter = RequestUtils.getParameter("/prefix/", request);
        Assert.assertEquals("path", parameter);
    }


    @Test
    public void getParameterDoublePrefix() {
        Mockito.when(request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE)).thenReturn(
                "/prefix/path/prefix"
        );

        String parameter = RequestUtils.getParameter("/prefix/", request);
        Assert.assertEquals("path/prefix", parameter);
    }

    @Test
    public void checkPrivateConstructorCall() throws Exception {
        Constructor<RequestUtils> constructor = RequestUtils.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        constructor.newInstance();
    }

}