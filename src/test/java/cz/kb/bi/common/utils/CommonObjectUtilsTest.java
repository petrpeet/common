package cz.kb.bi.common.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;

@RunWith(MockitoJUnitRunner.class)
public class CommonObjectUtilsTest {

    @Mock
    private Object first, second;

    @Test
    public void allNullReturnNull() throws Exception {
        Assert.assertNull(
                CommonObjectUtils.getFirstIfNotNull(null, null, null)
        );
    }

    @Test
    public void someNullReturnFirst() throws Exception {
        Assert.assertEquals(
                first,
                CommonObjectUtils.getFirstIfNotNull(null, first)
        );
    }

    @Test
    public void allNonNullReturnFirst() throws Exception {
        Assert.assertEquals(
                first,
                CommonObjectUtils.getFirstIfNotNull(first, second)
        );
    }

    @Test
    public void allNullListReturnNull() {
        Assert.assertNull(
                CommonObjectUtils.getFirstIfNotEmpty(null, null, null)
        );
    }

    @Test
    public void allEmptyListReturnNull() {
        Assert.assertNull(
                CommonObjectUtils.getFirstIfNotEmpty(Collections.emptyList(), new ArrayList<Object>())
        );
    }

    @Test
    public void someNullListReturnFirst() throws Exception {
        Assert.assertEquals(
                Collections.singletonList(first),
                CommonObjectUtils.getFirstIfNotEmpty(null, Collections.singletonList(first))
        );
    }

    @Test
    public void allNonNullListReturnFirst() throws Exception {
        Assert.assertEquals(
                Collections.singletonList(first),
                CommonObjectUtils.getFirstIfNotEmpty(Collections.singletonList(first), Collections.singletonList(second))
        );
    }
}