package cz.kb.bi.common.utils;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class CommonListUtilsTest {

    @Mock
    private Object first, second, third, fourth;

    @Test
    public void allNullValues() throws Exception {
        Assert.assertThat(
                CommonListUtils.joinLists(null, null, null),
                Matchers.emptyIterable()
        );
    }

    @Test
    public void someNullValues() throws Exception {
        Assert.assertThat(
                CommonListUtils.joinLists(Arrays.asList(first, second), null, null),
                Matchers.contains(first, second)
        );
    }

    @Test
    public void allNonNullValues() throws Exception {
        Assert.assertThat(
                CommonListUtils.joinLists(Arrays.asList(first, second), Arrays.asList(third, fourth)),
                Matchers.contains(first, second, third, fourth)
        );
    }
}