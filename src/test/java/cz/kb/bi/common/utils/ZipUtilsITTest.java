package cz.kb.bi.common.utils;

import cz.kb.bi.domain.entity.common.TextFile;
import org.apache.commons.io.FileUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipInputStream;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class ZipUtilsITTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void checkThatEmptyResultWontFailZip() throws Exception {
        Path result = ZipUtils.createZipFile(Collections.emptyList(), "result");

        Assert.assertNotNull(result);
        Assert.assertNotNull(result.toFile());

        FileUtils.forceDelete(result.toFile());
    }

    @Test
    public void checkThatZipHasProperFiles() throws Exception {
        Path zipFile = ZipUtils.createZipFile(
                Arrays.asList(
                        "src/test/resources/cz/kb/bi/common/utils/script1.txt",
                        "src/test/resources/cz/kb/bi/common/utils/script2.txt"
                ),
                "me file"
        );


        checkCommonZipFile(zipFile);
    }

    private void checkCommonZipFile(Path zipFile) throws Exception {
        try (InputStream inputStream = new FileInputStream(zipFile.toFile());
             ZipInputStream zip = new ZipInputStream(inputStream)) {

            Assert.assertEquals("script1.txt", zip.getNextEntry().getName());
            Assert.assertEquals("script2.txt", zip.getNextEntry().getName());
            Assert.assertNull(zip.getNextEntry());
            Assert.assertEquals(0, zip.available());
        } finally {
            FileUtils.forceDelete(zipFile.toFile());
        }

    }

    @Test
    public void createZipFileFromPaths() throws Exception {
        Path zipFile = ZipUtils.createZipFileFromPaths(
                Arrays.asList(
                        Paths.get("src/test/resources/cz/kb/bi/common/utils/script1.txt"),
                        Paths.get("src/test/resources/cz/kb/bi/common/utils/script2.txt")
                ),
                "me file"
        );

        checkCommonZipFile(zipFile);
    }

    @Test
    public void throwsException() throws Exception {
        exception.expect(IOException.class);

        ZipUtils.createZipFileFromPaths(Collections.singletonList(Paths.get("/not-existing")), "name");
    }

    @Test
    public void testUnzipp() throws IOException {
        Path input = Paths.get("src/test/resources/cz/kb/bi/common/utils/skript.zip");
        List<Path> result = ZipUtils.unzipFile(input);
        System.out.println(result.get(0).getFileName().toString());
        System.out.println(result.get(1).getFileName().toString());
        Assert.assertThat("scriptA.txt", Matchers.equalTo(result.get(0).getFileName().toString()));
        Assert.assertThat("scriptB.txt", Matchers.equalTo(result.get(1).getFileName().toString()));
        Assert.assertThat(result.get(0).toFile(), FenixMatchers.equalsFileContent("ABCD\nEFGH\n"));
        Assert.assertThat(result.get(1).toFile(), FenixMatchers.equalsFileContent("1122\n4433\n"));
        System.out.println(result.get(0).getParent());
        FileUtils.deleteDirectory(result.get(0).getParent().toFile());
    }

    @Test
    public void testCreateZipFileFromTexts() throws Exception {
        List<TextFile> fileList = Arrays.asList(new TextFile("script1.txt", "aa\r\nbb"), new TextFile("script2.txt", "cc\nbb"));
        Path result = ZipUtils.createZipFileFromTexts(fileList, "output");
        checkCommonZipFile(result);
    }

    @Test
    public void checkThatMergingFilesIsOk() throws IOException {
        File file = File.createTempFile("merged", ".txt");
        ZipUtils.mergeFiles(createFileList(), file.toPath());
        String expected = FileUtils.readFileToString(new File("src/test/resources/script/CorrectOutput.txt"), "UTF-8");
        String actual = FileUtils.readFileToString(file, "UTF-8");
        assertThat(expected, equalTo(actual));
        file.delete();
    }

    private List<Path> createFileList() {
        return Arrays.asList(
                Paths.get("src/test/resources/script/input1.txt"),
                Paths.get("src/test/resources/script/input2.txt"),
                Paths.get("src/test/resources/script/input3.txt")
        );
    }
}