package cz.kb.bi.common.utils;

import org.junit.Assert;
import org.junit.Test;

public class TextUtilsTest {

    @Test
    public void allNullString() throws Exception {
        Assert.assertEquals(
                "",
                TextUtils.mergeStrings(null, null, null)
        );
    }

    @Test
    public void someNullString() throws Exception {
        Assert.assertEquals(
                "hello" + TextUtils.WINDOWS_NEW_LINE + " " + TextUtils.WINDOWS_NEW_LINE + "world",
                TextUtils.mergeStrings("hello", null, " ", "world")
        );
    }

    @Test
    public void allNonNullString() throws Exception {
        Assert.assertEquals(
                "hello" + TextUtils.WINDOWS_NEW_LINE + " " + TextUtils.WINDOWS_NEW_LINE + "world",
                TextUtils.mergeStrings("hello", " ", "world")
        );
    }
}