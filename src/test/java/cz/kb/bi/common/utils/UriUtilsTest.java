package cz.kb.bi.common.utils;

import org.junit.Assert;
import org.junit.Test;

import java.net.URI;

public class UriUtilsTest {

    @Test
    public void append1() throws Exception {
        Assert.assertEquals(
                URI.create("http://www.google.com/testUri"),
                UriUtils.append(URI.create("http://www.google.com/"), "/testUri")
        );
    }

    @Test
    public void append4() throws Exception {
        Assert.assertEquals(
                URI.create("http://www.google.com/testUri"),
                UriUtils.append(URI.create("http://www.google.com"), "/testUri")
        );
    }

    @Test
    public void append2() throws Exception {
        Assert.assertEquals(
                URI.create("http://www.google.com/testUri"),
                UriUtils.append(URI.create("http://www.google.com/"), "testUri")
        );
    }

    @Test
    public void append3() throws Exception {
        Assert.assertEquals(
                URI.create("http://www.google.com/testUri"),
                UriUtils.append(URI.create("http://www.google.com"), "testUri")
        );
    }
}