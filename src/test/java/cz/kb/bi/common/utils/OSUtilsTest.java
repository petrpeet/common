package cz.kb.bi.common.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


@RunWith(PowerMockRunner.class)
@PrepareForTest(OSUtils.class)
public class OSUtilsTest {

    @Test
    public void checkOsName() {
        Assert.assertEquals(
                System.getProperty("os.name"),
                OSUtils.getOsName()
        );
    }

    @Test
    public void checkIsWin() {
        Assert.assertEquals(
                System.getProperty("os.name").startsWith("Windows"),
                OSUtils.isWindows()
        );
    }

}