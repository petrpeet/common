package cz.kb.bi.common.utils;

import cz.kb.bi.common.domain.entity.InternalUrl;
import cz.kb.bi.common.helpers.impl.settingstore.SettingStoreHelperImpl;
import cz.kb.bi.domain.dto.settingstore.StoredSettingDTO;
import lombok.Builder;
import lombok.Data;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class SettingStoreHelperImplTest {

    @InjectMocks
    SettingStoreHelperImpl settingStoreHelper;

    @Mock
    InternalUrl urls;

    @Mock
    RestTemplate restTemplate;

    StoredSettingDTO setting = new StoredSettingDTO();

    StoredSettingDTO[] settingDTOS = new StoredSettingDTO[] { setting };

    @Test
    public void writeAndReadTest() throws IOException {

        LocalDateTime now = LocalDateTime.now();
        ReferenceObject source = ReferenceObject.builder()
            .a("Hello")
            .b(11L)
            .list(
                Arrays.asList(
                    ReferenceObject.builder()
                    .a("Hello2")
                    .b(22L)
                    .build()
                )
            )
            .time(now)
            .build();

        String json = settingStoreHelper.toJSON(source, ReferenceObject.class);

        ReferenceObject object = settingStoreHelper.fromJSON(json, ReferenceObject.class);

        Assert.assertThat(
            object,
            Matchers.allOf(
                Matchers.hasProperty("a", Matchers.is("Hello")),
                Matchers.hasProperty("b", Matchers.is(11L)),
                Matchers.hasProperty("time", Matchers.is(now)),
                Matchers.hasProperty("list",
                                     Matchers.contains(
                                         Matchers.allOf(
                                            Matchers.hasProperty("a", Matchers.is("Hello2")),
                                            Matchers.hasProperty("b", Matchers.is(22L)))
                                    ))));
    }

    @Test
    public void testStoreSetting() {
        Mockito.doReturn("URL1").when(urls).getSettingStore();
        settingStoreHelper.storeSetting(setting);
        Mockito.verify(restTemplate).put(Mockito.eq("URL1"), Mockito.eq(setting));
    }

    @Test
    public void testFindSettings()  {
        Mockito.doReturn("URL1").when(urls).getSettingStoreFind();
        Mockito.doReturn(settingDTOS).when(restTemplate).postForObject(
            Mockito.eq("URL1"),
            Mockito.argThat(
                Matchers.allOf(
                    Matchers.hasEntry(Matchers.is("userName"),
                                      Matchers.contains("user")),
                    Matchers.hasEntry(Matchers.is("domainName"),
                                      Matchers.contains("d1")),
                    Matchers.hasEntry(Matchers.is("domainItemId"),
                                      Matchers.contains("1"))
                )
            ),
            Mockito.eq(StoredSettingDTO[].class)
        );
        List<StoredSettingDTO> settings = settingStoreHelper.findSettings("user",
                                                                          "d1", "1");
        Assert.assertThat(settings,  Matchers.contains(setting));
    }

    @Data
    @Builder
    public static class ReferenceObject {
        private String a;
        private Long b;
        private List<ReferenceObject> list;
        private LocalDateTime time;
    }

    @Test
    public void testDeleteSetting() {
        Mockito.doReturn("URL1").when(urls).getSettingStoreDelete();
        settingStoreHelper.deleteSetting("u1", "d1", "id1", 12);
        Mockito.verify(restTemplate).put(Mockito.eq("URL1"),
                                         Mockito.argThat(
                                             Matchers.allOf(
                                                 Matchers.hasProperty("userName", Matchers.is("u1")),
                                                 Matchers.hasProperty("domainName",  Matchers.is("d1")),
                                                 Matchers.hasProperty("domainItemId",  Matchers.is("id1")),
                                                 Matchers.hasProperty("order",  Matchers.is(12))
                                             )
                                         ));
    }
}
