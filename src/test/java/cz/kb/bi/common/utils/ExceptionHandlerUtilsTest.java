package cz.kb.bi.common.utils;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.lang.reflect.Constructor;


@RunWith(MockitoJUnitRunner.class)
public class ExceptionHandlerUtilsTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void checkEmptyConstructor() throws Exception {
        Constructor<ExceptionHandlerUtils> constructor = ExceptionHandlerUtils.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        constructor.newInstance();
    }
    @Test
    public void handleApiErrorOptional() {
        HttpClientErrorException ex = new HttpClientErrorException(HttpStatus.NOT_FOUND);

        Assert.assertFalse(ExceptionHandlerUtils.handleApiErrorOptional(ex, "").isPresent());
    }

    @Test
    public void handleApiErrorOptionaReThrowsl() {
        HttpServerErrorException ex = new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);

        exception.expect(HttpServerErrorException.class);
        ExceptionHandlerUtils.handleApiErrorOptional(ex, "");
    }

    @Test
    public void handleApiErrorBoolean() {
        HttpClientErrorException ex = new HttpClientErrorException(HttpStatus.NOT_FOUND);

        Assert.assertFalse(ExceptionHandlerUtils.handleApiErrorBoolean(ex, ""));
    }

    @Test
    public void handleApiErrorBooleanReThrows() {
        HttpServerErrorException ex = new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);

        exception.expect(HttpServerErrorException.class);
        ExceptionHandlerUtils.handleApiErrorBoolean(ex, "");
    }

}