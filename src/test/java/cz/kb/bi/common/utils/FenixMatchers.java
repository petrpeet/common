package cz.kb.bi.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@Slf4j
public class FenixMatchers {

    private FenixMatchers() {
        // Not meant to be create
    }

    public static Matcher<? super String> pathEqualsInStringRepresentation(final Path expectedPath) {
        return new TypeSafeMatcher<String>() {
            @Override
            protected boolean matchesSafely(String s) {
                return StringUtils.equalsIgnoreCase(PathUtils.pathToDisplay(expectedPath), PathUtils.pathToDisplay(Paths.get(s)));
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Text representation of path is not equal to ");
                description.appendValue(PathUtils.pathToDisplay(expectedPath));
            }
        };
    }


    public static Matcher<? super String> pathToStringEquals(final Path inputPath) {
        return new TypeSafeMatcher<String>() {
            @Override
            protected boolean matchesSafely(String s) {
                return Paths.get(s).equals(inputPath);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Path not equal to " + inputPath);
            }
        };
    }

    public static Matcher<? super Path> pathEquals(final Path inputPath) {
        return new TypeSafeMatcher<Path>() {
            @Override
            protected boolean matchesSafely(Path path) {
                return StringUtils.equalsIgnoreCase(
                        fixPath(inputPath),
                        fixPath(path)
                );
            }

            private String fixPath(Path path) {
                if (path != null) {
                    final String tmpPth = path.toString().replaceAll("\\\\", "/");
                    return tmpPth.startsWith("//") ? tmpPth.substring(1) : tmpPth;
                } else {
                    return null;
                }
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Path was not equal to ");
                description.appendText(fixPath(inputPath));
                description.appendText("[after os fix].");
            }
        };
    }

    public static Matcher<? super File> equalsFileContent(final String content) {
        return new TypeSafeMatcher<File>() {

            private Matcher<? super InputStream> inputStreamMatcher
                    = equalsInputStream(content);

            @Override
            protected boolean matchesSafely(File file) {
                try {
                    return inputStreamMatcher.matches(new FileInputStream(file));
                } catch (FileNotFoundException e) {
                    throw new IllegalArgumentException(e);
                }
            }

            @Override
            public void describeTo(Description description) {
                inputStreamMatcher.describeTo(description);
            }
        };
    }

    public static Matcher<? super InputStream> equalsInputStream(final String content) {
        return new TypeSafeMatcher<InputStream>() {

            private String actualValue;

            private boolean read = false;

            private void readValue(InputStream inputStream) throws IOException {
                if (!read) {
                    actualValue = readInputStream(inputStream);
                    read = true;
                }
            }

            @Override
            protected boolean matchesSafely(InputStream inputStream) {
                try {
                    readValue(inputStream);
                    return equalTo(content).matches(actualValue);
                } catch (IOException e) {
                    log.error("Matcher error", e);
                    return false;
                }
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Input stream contains(");
                equalTo(content).describeTo(description);
                description.appendText(")");
            }

            @Override
            protected void describeMismatchSafely(InputStream inputStream, Description mismatchDescription) {
                try {
                    readValue(inputStream);
                    assertThat(
                            actualValue,
                            equalTo(content)
                    );
                    //equalTo(content).describeMismatch(actualValue, mismatchDescription);
                } catch (IOException e) {
                    log.error("Matcher error", e);
                }
            }
        };
    }

    private static String readInputStream(InputStream inputStream) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            while (bufferedReader.ready()) {
                sb.append(bufferedReader.readLine());
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    public static BaseMatcher<? super String> containsIgnoreCase(final String input) {
        return new BaseMatcher<String>() {
            @Override
            public boolean matches(Object o) {
                return defaultIfNull(o, "").toString().toLowerCase().contains(input.toLowerCase());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("containsIgnoringCase(")
                        .appendValue(input)
                        .appendText(")");
            }
        };
    }

    public static BaseMatcher<? super String> containsStringIgnoreCaseTrim(final String input) {
        return new BaseMatcher<String>() {
            @Override
            public boolean matches(Object o) {
                return defaultIfNull(o, "").toString().toLowerCase().trim().contains(input.toLowerCase().trim());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("containsStringIgnoreCaseTrim(")
                        .appendValue(input)
                        .appendText(")");
            }
        };
    }

    public static Matcher<? super Map<?, ?>> emptyMap() {
        return new BaseMatcher<Map<?, ?>>() {
            @Override
            public boolean matches(Object o) {
                return o != null && o instanceof Map && ((Map) o).isEmpty();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("emptyMap");
            }
        };
    }

    private static String replaceEndLine(String input) {
        final String END_LINE_PATTERN = "[\\n\\r]+";
        return StringUtils.defaultString(input).replaceAll(END_LINE_PATTERN, "\\t");
    }

    public static Matcher<? super String> containsIgnoreEndLine(final String input) {
        return new TypeSafeMatcher<String>() {

            @Override
            protected boolean matchesSafely(String inputString) {
                return replaceEndLine(inputString).contains(replaceEndLine(input));
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("contains(")
                        .appendValue(input)
                        .appendText(")");
            }
        };
    }

    public static Matcher<? super String> equalToIgnoreEndLine(final String input) {
        return new TypeSafeMatcher<String>() {
            @Override
            protected boolean matchesSafely(String inputString) {
                return replaceEndLine(input).equals(replaceEndLine(inputString));
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("equalToIgnoreEndLine(")
                        .appendValue(input)
                        .appendText(")");
            }
        };
    }

    public static Matcher<? super String> isNotBlank() {
        return new TypeSafeMatcher<String>() {
            @Override
            protected boolean matchesSafely(String s) {
                return StringUtils.isNotBlank(s);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Input value was blank");
            }
        };
    }

    public static Matcher<? super String> isBlank() {
        return new TypeSafeMatcher<String>() {
            @Override
            protected boolean matchesSafely(String s) {
                return StringUtils.isBlank(s);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Input value was not blank");
            }
        };
    }

    public static Matcher<? super String> stringWithLength(final Matcher<? super Integer> sizeMatcher) {
        return new TypeSafeMatcher<String>() {
            @Override
            protected boolean matchesSafely(String s) {
                return sizeMatcher.matches(s.length());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Expected text with:");
                sizeMatcher.describeTo(description);
            }
        };
    }

    public static Matcher<? super Map<?, ?>> mapWithSize(int size) {
        return new TypeSafeMatcher<Map<?, ?>>() {
            @Override
            protected boolean matchesSafely(Map<?, ?> map) {
                return map != null && map.size() == size;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Expected Map size of:" + size);
            }
        };
    }

    public static Matcher<? super Path> pathToStringContains(final String containsStr) {
        return new TypeSafeMatcher<Path>() {
            @Override
            protected boolean matchesSafely(Path s) {
                return s.toString().contains(containsStr);
            }

            @Override
            public void describeTo(Description description) {

            }
        };
    }
}
