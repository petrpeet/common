package cz.kb.bi.common.utils;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static cz.kb.bi.common.utils.CommonFileUtils.CP_1250_CHARSET;
import static cz.kb.bi.common.utils.CommonFileUtils.UTF_CHARSET;
import static cz.kb.bi.common.utils.CommonFileUtils.openAsStreamBTEQ;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class CommonFileUtilsITTest {

    private static final String rootPath = "src/test/resources/cz/kb/bi/common/script";
    private static final String filePath = rootPath + "/testFile.txt";
    private static final String notFilePath = filePath + "notFound";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void checkThatUTF8FileIsEncoded() throws Exception {
        final Path path = Paths.get(rootPath, "utf8.txt");
        final Charset charset = CommonFileUtils.suggestBTEQScriptCharset(path);

        Assert.assertEquals(UTF_CHARSET, charset);
    }

    @Test
    public void checkThatCP1250FileIsEncoded() throws Exception {
        final Path path = Paths.get(rootPath, "cp1250.txt");
        final Charset charset = CommonFileUtils.suggestBTEQScriptCharset(path);

        Assert.assertEquals(CP_1250_CHARSET, charset);
    }

    @Test
    public void checkThatFileIsReadWithDelimiter() throws IOException {
        try (Stream<String> fileStream = CommonFileUtils.openAsStream(Paths.get(filePath), "(ab)+", CommonFileUtils.UTF_CHARSET)) {
            String context
                    = fileStream
                    .reduce(
                            "", (s, s2) -> s.trim() + "\n" + s2.trim()
                    ).trim();
            assertThat(
                    context,
                    equalTo("aaa\n" +
                            "ddd\n" +
                            "ccc")
            );
        }
    }

    @Test
    public void checkThatConstructorWillFail()
            throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        expectedException.expect(InvocationTargetException.class);

        Constructor<CommonFileUtils> c = CommonFileUtils.class.getDeclaredConstructor();
        c.setAccessible(true);
        c.newInstance();
    }

    @Test
    public void checkOpenAsStreamBTEQ() throws IOException {
        expectedException.expect(NoSuchFileException.class);
        expectedException.expectMessage(containsString("testFile.txt"));
        openAsStreamBTEQ(Paths.get(notFilePath));
    }

    @Test
    public void checkFileNotExists() {
        assertTrue(CommonFileUtils.isFileExists(Paths.get(filePath)));
    }

    @Test
    public void checkFileExists() {
        assertFalse(CommonFileUtils.isFileExists(Paths.get(notFilePath)));
    }

    @Test
    public void checkFileNotExistsFolder() {
        assertFalse(CommonFileUtils.isFileExists(Paths.get(rootPath)));
    }


    @Test
    public void getBaseFileName() {
        Assert.assertEquals(
                "testFile",
                CommonFileUtils.getBaseFileName(Paths.get(filePath))
        );
    }

    @Test
    public void getFileExtension() throws Exception {
        Assert.assertEquals(
                ".txt",
                CommonFileUtils.getFileExtensionWithDot(Paths.get(filePath))
        );
    }

    @Test
    public void getFileExtensionNone() throws Exception {
        Assert.assertEquals(
                "",
                CommonFileUtils.getFileExtensionWithDot(Paths.get(rootPath + "/abcsasd"))
        );
    }
}
