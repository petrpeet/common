package cz.kb.bi.common.security;

import org.hamcrest.Matchers;
import org.junit.rules.ExpectedException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

public class ExpectedExceptionSettings {

    private ExpectedExceptionSettings() {
        // Should not be used
    }

    public static void setAccessDenied(ExpectedException expectedException) {
        expectedException.expect(AccessDeniedException.class);
        expectedException.expectMessage(
                Matchers.anyOf(
                        Matchers.equalTo("Access is denied"),
                        Matchers.equalTo("Přístup odepřen")
                ));
    }

    public static void setNotLoggedIn(ExpectedException expectedException) {
        expectedException.expect(AuthenticationCredentialsNotFoundException.class);
        expectedException.expectMessage("An Authentication object was not found in the SecurityContext");
    }
}
