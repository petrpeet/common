package cz.kb.bi.common.mapping.mapper;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.AbstractMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class LocalDateTimeZoneDateTimeConverterTest {

    private final LocalDateTime summerTime = LocalDateTime.of(2017, 7, 1, 11, 1);
    private final LocalDateTime springTime = LocalDateTime.of(2017, 2, 1, 11, 1);
    private final LocalDateTime winterTime = LocalDateTime.of(2017, 12, 1, 11, 1);
    private LocalDateTimeZoneDateTimeConverter converter;

    @Mock
    private Map<Integer, Map.Entry<LocalDateTime, LocalDateTime>> mockShifts;

    @Before
    public void setUp() throws Exception {
        converter = new LocalDateTimeZoneDateTimeConverter();
    }

    @Test
    public void nullZone() throws Exception {
        Assert.assertNull(converter.convertFrom(null, null));
    }

    @Test
    public void nullLocal() throws Exception {
        Assert.assertNull(converter.convertTo(null, null));
    }

    @Test
    public void sameYearNotCalculatedTwice() throws Exception {
        Mockito.when(mockShifts.containsKey(Mockito.eq(2017))).thenReturn(false, true);
        Mockito.when(mockShifts.get(Mockito.eq(2017))).thenReturn(new AbstractMap.SimpleImmutableEntry<>(LocalDateTime.now(), LocalDateTime.now()));
        converter.setTimeShifts(mockShifts);

        converter.convertFrom(summerTime, null);
        converter.convertFrom(summerTime, null);

        Mockito.verify(mockShifts).put(Mockito.eq(2017), Mockito.any());
    }

    @Test
    public void springTime() throws Exception {
        Assert.assertEquals(
                springTime.atZone(LocalDateTimeZoneDateTimeConverter.WINTER_ZONE),
                converter.convertFrom(springTime, null)
        );
    }

    @Test
    public void summerTime() throws Exception {
        Assert.assertEquals(
                summerTime.atZone(LocalDateTimeZoneDateTimeConverter.SUMMER_ZONE),
                converter.convertFrom(summerTime, null)
        );
    }

    @Test
    public void winterTime() throws Exception {
        Assert.assertEquals(
                winterTime.atZone(LocalDateTimeZoneDateTimeConverter.WINTER_ZONE),
                converter.convertFrom(winterTime, null)
        );
    }

    @Test
    public void testDateBound2017() throws Exception {
        converter.convertFrom(winterTime, null);
        Assert.assertThat(
                converter.getTimeShifts(),
                Matchers.hasEntry(
                        Matchers.equalTo(2017),
                        Matchers.equalTo(
                                new AbstractMap.SimpleImmutableEntry<>(
                                        LocalDateTime.of(2017, 3, 26, 3, 0),
                                        LocalDateTime.of(2017, 10, 29, 3, 0))
                        )
                )
        );
    }

    @Test
    public void testDateBound2018() throws Exception {
        converter.convertFrom(LocalDateTime.of(2018, 1, 1, 1, 1), null);
        Assert.assertThat(
                converter.getTimeShifts(),
                Matchers.hasEntry(
                        Matchers.equalTo(2018),
                        Matchers.equalTo(
                                new AbstractMap.SimpleImmutableEntry<>(
                                        LocalDateTime.of(2018, 3, 25, 3, 0),
                                        LocalDateTime.of(2018, 10, 28, 3, 0))
                        )
                )
        );
    }

    @Test
    public void notNullZoned() throws Exception {
        final ZonedDateTime testTime = ZonedDateTime.now();
        Assert.assertEquals(
                testTime.toLocalDateTime(),
                converter.convertTo(testTime, null)
        );
    }

    @Test
    public void notNullZonedBadZone() throws Exception {
        final ZonedDateTime testTime = ZonedDateTime.of(LocalDateTime.of(2018, 10, 28, 3, 0), ZoneId.of("Europe/Lisbon"));
        Assert.assertEquals(
                LocalDateTime.of(2018, 10, 28, 4, 0),
                converter.convertTo(testTime, null)
        );
    }


}