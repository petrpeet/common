package cz.kb.bi.common.mapping.mapper;

import org.junit.Assert;
import org.junit.Test;

import java.time.Duration;

public class StringDurationConverterTest {

    private StringDurationConverter stringDurationConverter
            = new StringDurationConverter();

    @Test
    public void nullDurationIsCorrect() throws Exception {
        Assert.assertNull(stringDurationConverter.convertFrom(null, null));
    }

    @Test
    public void nullStringIsCorrect() throws Exception {
        Assert.assertNull(stringDurationConverter.convertTo(null, null));
    }

    @Test
    public void convertIntoStringIsCorrect() throws Exception {
        Assert.assertEquals("30", stringDurationConverter.convertFrom(Duration.ofSeconds(30)));
    }

    @Test
    public void convertIntoStringIsCorrect2() throws Exception {
        Assert.assertEquals("30:00", stringDurationConverter.convertFrom(Duration.ofMinutes(30)));
    }

    @Test
    public void convertIntoStringIsCorrect3() throws Exception {
        Assert.assertEquals("29:30", stringDurationConverter.convertFrom(Duration.ofMinutes(30).minusSeconds(30)));
    }

    @Test
    public void convertIntoStringIsCorrect4() throws Exception {
        Assert.assertEquals("02:58:30", stringDurationConverter.convertFrom(Duration.ofHours(3)
                .minusMinutes(1)
                .minusSeconds(30))
        );
    }

    @Test
    public void convertIntoDurationIsCorrect() throws Exception {
        Assert.assertEquals(Duration.ofSeconds(30), stringDurationConverter.convertTo("30"));
    }

    @Test
    public void convertIntoDurationIsCorrect2() throws Exception {
        Assert.assertEquals(Duration.ofMinutes(30), stringDurationConverter.convertTo("30:00"));
    }

    @Test
    public void convertIntoDurationIsCorrect3() throws Exception {
        Assert.assertEquals(Duration.ofMinutes(30).minusSeconds(30), stringDurationConverter.convertTo("29:30"));
    }

    @Test
    public void convertIntoDurationIsCorrect4() throws Exception {
        Assert.assertEquals(
                Duration.ofHours(3)
                        .minusMinutes(1)
                        .minusSeconds(30),
                stringDurationConverter.convertTo("02:58:30")
        );
    }

}