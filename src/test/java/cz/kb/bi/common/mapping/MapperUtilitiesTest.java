package cz.kb.bi.common.mapping;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class MapperUtilitiesTest {

    @Test
    public void checkThatConvertToObjectIsCorrect() {
        SourceObject sourceObject = new SourceObject("id", "name", "surname");

        final TargetObject mappedObject =
                MapperUtilities.map(sourceObject, TargetObject.class);

        Assert.assertThat(
                mappedObject,
                Matchers.allOf(
                        Matchers.hasProperty("name", Matchers.equalTo("name")),
                        Matchers.hasProperty("surname", Matchers.equalTo("surname"))
                )
        );
    }

    @Test
    public void checkConversionOfEmptyInputWithDefault() {
        SourceObject sourceObject = null;
        TargetObject defaultObject = new TargetObject();
        defaultObject.setName("E");
        TargetObject result = MapperUtilities.map(sourceObject, TargetObject.class, defaultObject);
        Assert.assertThat(result, Matchers.hasProperty("name", Matchers.is("E")));
    }

    @Test
    public void checkConversionOfNonEmptyInputWithDefault() {
        SourceObject sourceObject =  new SourceObject("A", "A", "A");
        TargetObject defaultObject = new TargetObject();
        defaultObject.setName("E");
        TargetObject result = MapperUtilities.map(sourceObject, TargetObject.class, defaultObject);
        Assert.assertThat(result, Matchers.hasProperty("name", Matchers.is("A")));
    }

    @Test
    public void checkThatMapOfEmptyOptionalIsCorrect() {
        Assert.assertNull(MapperUtilities.map(Optional.empty(), TargetObject.class));
    }

    @Test
    public void checkThatMapOfOptionalIsCorrect() {

        Optional<SourceObject> sourceObject = Optional.of(
                new SourceObject("id", "name", "surname")
        );

        final TargetObject mappedObject =
                MapperUtilities.map(sourceObject, TargetObject.class);

        Assert.assertThat(
                mappedObject,
                Matchers.allOf(
                        Matchers.hasProperty("name", Matchers.equalTo("name")),
                        Matchers.hasProperty("surname", Matchers.equalTo("surname"))
                )
        );
    }

    @SuppressWarnings("unchecked")
    @Test
    public void checkThatMapOfListIsCorrect() {
        List<SourceObject> sourceObject = Arrays.asList(
                new SourceObject("id", "name", "surname"),
                new SourceObject("id1", "name1", "surname1")
        );


        final List<TargetObject> mappedObject =
                MapperUtilities.map(sourceObject, TargetObject.class);

        Assert.assertThat(
                mappedObject,
                Matchers.contains(
                        Matchers.allOf(
                                Matchers.hasProperty("name", Matchers.equalTo("name")),
                                Matchers.hasProperty("surname", Matchers.equalTo("surname"))
                        ),
                        Matchers.allOf(
                                Matchers.hasProperty("name", Matchers.equalTo("name1")),
                                Matchers.hasProperty("surname", Matchers.equalTo("surname1"))
                        )
                )
        );
    }

    @SuppressWarnings("unchecked")
    @Test
    public void checkThatMapOfSetIsCorrect() {

        Set<SourceObject> sourceObject = new HashSet<>(Arrays.asList(
                new SourceObject("id", "name", "surname"),
                new SourceObject("id1", "name1", "surname1")
        ));


        final Set<TargetObject> mappedObject =
                MapperUtilities.map(sourceObject, TargetObject.class);

        Assert.assertThat(
                mappedObject,
                Matchers.containsInAnyOrder(
                        Matchers.allOf(
                                Matchers.hasProperty("name", Matchers.equalTo("name")),
                                Matchers.hasProperty("surname", Matchers.equalTo("surname"))
                        ),
                        Matchers.allOf(
                                Matchers.hasProperty("name", Matchers.equalTo("name1")),
                                Matchers.hasProperty("surname", Matchers.equalTo("surname1"))
                        )
                )
        );
    }

    @Test
    public void checkThatEmptyOptionalIsMappedCorrectly() throws Exception {
        ResponseEntity<TargetObject> targetEntity = MapperUtilities.mapToResponseEntity(Optional.empty(),
                TargetObject.class);

        Assert.assertThat(
                targetEntity,
                Matchers.allOf(
                        Matchers.hasProperty("statusCode", Matchers.equalTo(HttpStatus.NOT_FOUND)),
                        Matchers.hasProperty("body", Matchers.nullValue())
                )
        );
    }

    @Test
    public void checkThatFullOptionalIsMappedCorrectly() throws Exception {
        ResponseEntity<TargetObject> targetEntity = MapperUtilities.mapToResponseEntity(
                Optional.of(
                        new SourceObject("id", "name", "surname")
                ),
                TargetObject.class
        );

        Assert.assertThat(
                targetEntity,
                Matchers.allOf(
                        Matchers.hasProperty("statusCode", Matchers.equalTo(HttpStatus.OK)),
                        Matchers.hasProperty("body",
                                targetObjectMatcher()
                        )
                )
        );
    }

    @Test
    public void testMapToResponseEntity() {
        ResponseEntity<TargetObject> result = MapperUtilities.mapToResponseEntity(new SourceObject("id", "name", "surname"), TargetObject.class);
        Assert.assertThat(
                result,
                Matchers.allOf(
                        Matchers.hasProperty("statusCode", Matchers.equalTo(HttpStatus.OK)),
                        Matchers.hasProperty("body",
                                targetObjectMatcher()
                        )
                )
        );
    }

    private Matcher<Object> targetObjectMatcher() {
        return Matchers.allOf(
                Matchers.hasProperty("name", Matchers.equalTo("name")),
                Matchers.hasProperty("surname", Matchers.equalTo("surname"))
        );
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void checkThatMappingOptionalIsDoneCorrectly() {
        Optional<TargetObject> targetObject = MapperUtilities.mapToOptional(
                new SourceObject("id", "name", "surname"),
                TargetObject.class
        );

        Assert.assertThat(
                targetObject.get(),
                targetObjectMatcher()
        );
    }

    @Test
    public void checkThatMappingEmptyOptionalIsDoneCorrectly() {
        Optional<TargetObject> targetObject = MapperUtilities.mapToOptional(
                null,
                TargetObject.class
        );

        Assert.assertFalse(targetObject.isPresent());
    }

    @Test
    public void checkMappingOptional() throws Exception {
        ResponseEntity<Object> objectResponseEntity =
                MapperUtilities.mapToResponseEntity(Optional.empty());

        Assert.assertThat(
                objectResponseEntity,
                Matchers.hasProperty("statusCode", Matchers.equalTo(HttpStatus.NOT_FOUND))
        );
    }

    @Test
    public void checkMappingValue() throws Exception {
        ResponseEntity<Object> objectResponseEntity =
                MapperUtilities.mapToResponseEntity(Optional.of("String"));

        Assert.assertThat(
                objectResponseEntity,
                Matchers.allOf(
                        Matchers.hasProperty("statusCode", Matchers.equalTo(HttpStatus.OK)),
                        Matchers.hasProperty("body", Matchers.equalTo("String"))
                )
        );
    }
}