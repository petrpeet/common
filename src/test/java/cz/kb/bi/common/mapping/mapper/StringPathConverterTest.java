package cz.kb.bi.common.mapping.mapper;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StringPathConverterTest {

    private StringPathConverter stringPathConverter;

    private String testString;

    private Path testPath;


    @Before
    public void setUp() throws Exception {
        testPath = Files.createTempFile("temp-test-file", ".tmp");
        testString = testPath.toString().replaceAll("\\\\", "/");
        stringPathConverter = new StringPathConverter();
    }

    @Test
    public void checkThatStringConversionIsOk() throws Exception {
        Assert.assertEquals(
                testString,
                stringPathConverter.convertFrom(testPath, null)
        );
    }

    @Test
    public void checkThatPathConversionIsOk() throws Exception {
        Assert.assertEquals(
                testPath,
                stringPathConverter.convertTo(testString, null)
        );
    }

    @Test
    public void checkThatNullIsCorrect() throws Exception {
        Assert.assertNull(stringPathConverter.convertFrom(null, null));
    }

    @Test
    public void checkThatNullIsCorrect1() throws Exception {
        Assert.assertNull(stringPathConverter.convertTo(null, null));
    }

    @Test
    public void checkConversionToPath() throws Exception {
        Assert.assertEquals(
                Paths.get("//VSFS03.DS.KB.CZ/Data/4800/Shared/BIUsers/LKosina/pubWrite/shadowfolder/TEST-2671/DDL/Tables/ddl.txt"),
                stringPathConverter.convertTo("file://VSFS03.DS.KB.CZ/Data/4800/Shared/BIUsers/LKosina/pubWrite/shadowfolder/TEST-2671/DDL/Tables/ddl.txt", null)

        );
    }

    @After
    public void tearDown() throws Exception {
        Files.delete(testPath);
    }
}