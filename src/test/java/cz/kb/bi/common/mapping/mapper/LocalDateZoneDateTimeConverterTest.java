package cz.kb.bi.common.mapping.mapper;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;


@RunWith(MockitoJUnitRunner.class)
public class LocalDateZoneDateTimeConverterTest {

    @InjectMocks
    private LocalDateZoneDateTimeConverter converter;

    @Mock
    private LocalDateTimeZoneDateTimeConverter dateTimeZoneDateTimeConverter;
    private ZonedDateTime valueFrom = ZonedDateTime.now();
    private LocalDateTime valueTo = LocalDateTime.now();


    @Before
    public void setUp() throws Exception {
        Mockito.when(dateTimeZoneDateTimeConverter.convertFrom(Mockito.any(), Mockito.any()))
                .thenReturn(valueFrom);
        Mockito.when(dateTimeZoneDateTimeConverter.convertTo(Mockito.any(), Mockito.any()))
                .thenReturn(valueTo);
    }


    @Test
    public void checkConvertFrom() {
        LocalDate now = LocalDate.now();

        ZonedDateTime zonedDateTime = converter.convertFrom(LocalDate.now(), null);

        Mockito.verify(dateTimeZoneDateTimeConverter).convertFrom(
                LocalDateTime.of(
                        now,
                        LocalTime.MIDNIGHT
                ),
                null
        );
        Assert.assertEquals(valueFrom, zonedDateTime);
    }

    @Test
    public void checkConvertTo() {
        ZonedDateTime now = ZonedDateTime.now();
        LocalDate nowDate = LocalDate.now();

        LocalDate localDate = converter.convertTo(now, nowDate);

        Mockito.verify(dateTimeZoneDateTimeConverter).convertTo(now,
                LocalDateTime.of(
                        nowDate,
                        LocalTime.MIDNIGHT
                )
        );
        Assert.assertEquals(valueTo.toLocalDate(), localDate);
    }

    @Test
    public void checkConvertToNull() {
        Mockito.when(dateTimeZoneDateTimeConverter.convertTo(Mockito.any(), Mockito.any()))
                .thenReturn(null);

        LocalDate nowDate = LocalDate.now();

        LocalDate localDate = converter.convertTo(null, nowDate);

        Mockito.verify(dateTimeZoneDateTimeConverter).convertTo(null,
                LocalDateTime.of(
                        nowDate,
                        LocalTime.MIDNIGHT
                )
        );
        Assert.assertEquals(null, localDate);
    }

    @Test
    public void checkConvertFromNull() {
        ZonedDateTime zonedDateTime = converter.convertFrom(null, null);

        Mockito.verify(dateTimeZoneDateTimeConverter).convertFrom(
                null,
                null
        );
        Assert.assertEquals(valueFrom, zonedDateTime);
    }
}