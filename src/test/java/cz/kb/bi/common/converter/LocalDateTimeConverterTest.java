package cz.kb.bi.common.converter;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class LocalDateTimeConverterTest {

    private LocalDateTimeConverter localDateTimeConverter = new LocalDateTimeConverter();

    @Test
    public void checkThatNullIsOk() throws Exception {
        Assert.assertNull(localDateTimeConverter.convert(null));
    }

    @Test
    public void checkThatBlankIsOk() throws Exception {
        Assert.assertNull(localDateTimeConverter.convert(""));
    }

    @Test
    public void checkThatDateIsOk() throws Exception {
        Assert.assertEquals(
                LocalDateTime.of(2014, 1, 1, 23, 59, 56, 123000000),
                localDateTimeConverter.convert("2014-01-01T23:59:56.123")
        );
    }
}