package cz.kb.bi.common.converter;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class LocalDateConverterTest {
    private LocalDateConverter localDateConverter = new LocalDateConverter();

    @Test
    public void checkThatNullIsOk() throws Exception {
        Assert.assertNull(localDateConverter.convert(null));
    }

    @Test
    public void checkThatBlankIsOk() throws Exception {
        Assert.assertNull(localDateConverter.convert(""));
    }

    @Test
    public void checkThatDateIsOk() throws Exception {
        Assert.assertEquals(
                LocalDate.of(2014, 1, 1),
                localDateConverter.convert("2014-01-01")
        );
    }
}