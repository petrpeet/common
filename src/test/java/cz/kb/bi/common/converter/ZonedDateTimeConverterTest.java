package cz.kb.bi.common.converter;

import cz.kb.bi.common.mapping.mapper.LocalDateTimeZoneDateTimeConverter;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class ZonedDateTimeConverterTest {
    private ZonedDateTimeConverter zoneDateTimeConverter = new ZonedDateTimeConverter();

    @Test
    public void checkThatNullIsOk() throws Exception {
        Assert.assertNull(zoneDateTimeConverter.convert(null));
    }

    @Test
    public void checkThatBlankIsOk() throws Exception {
        Assert.assertNull(zoneDateTimeConverter.convert(""));
    }

    @Test
    public void checkThatDateIsOk() throws Exception {
        Assert.assertEquals(
                ZonedDateTime.of(LocalDateTime.of(2014, 1, 1, 23, 59, 56, 123000000), LocalDateTimeZoneDateTimeConverter.SUMMER_ZONE),
                zoneDateTimeConverter.convert("2014-01-01T23:59:56.123+02:00")
        );
    }
}