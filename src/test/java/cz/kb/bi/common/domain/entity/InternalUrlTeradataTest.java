package cz.kb.bi.common.domain.entity;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class InternalUrlTeradataTest {

    @InjectMocks
    private InternalUrlTeradata internalUrlTeradata;

    @Test
    public void getFormattedUserDetail() {
        internalUrlTeradata.setUserDetail("user %s");

        Assert.assertEquals(
                "user me",
                internalUrlTeradata.getFormattedUserDetail("me")
        );
    }

    @Test
    public void getFormattedUserRight() {
        internalUrlTeradata.setUserRight("right %s");

        Assert.assertEquals(
                "right all",
                internalUrlTeradata.getFormattedUserRight("all")
        );
    }

    @Test
    public void getFormattedDatabaseExists() {
        internalUrlTeradata.setDatabaseExists("database %s");

        Assert.assertEquals(
                "database grp_user",
                internalUrlTeradata.getFormattedDatabaseExists("grp_user")
        );
    }

    @Test
    public void getFormattedWarehouseObject() {
        internalUrlTeradata.setWarehouseObject("object %s");

        Assert.assertEquals(
                "object dwkb",
                internalUrlTeradata.getFormattedWarehouseObject("dwkb")
        );
    }

    @Test
    public void getFormattedOneWarehouseObject() {
        internalUrlTeradata.setOneWarehouseObject("object %s");

        Assert.assertEquals(
                "object party",
                internalUrlTeradata.getFormattedOneWarehouseObject("party")
        );
    }

    @Test
    public void getFormattedDdlWorkflow() {
        internalUrlTeradata.setDdlWorkflow("workflow %s %s");

        Assert.assertEquals(
                "workflow cbs icko",
                internalUrlTeradata.getFormattedDdlWorkflow("cbs", "icko")
        );
    }

    @Test
    public void getFormattedDdlScript() {
        internalUrlTeradata.setDdlScript("script %s %s");

        Assert.assertEquals(
                "script cmd_DD party",
                internalUrlTeradata.getFormattedDdlScript("cmd_DD", "party")
        );
    }

    @Test
    public void getFormattedDdlTable() {
        internalUrlTeradata.setDdlTable("table %s %s");

        Assert.assertEquals(
                "table dwkb agreement",
                internalUrlTeradata.getFormattedDdlTable("dwkb", "agreement")
        );
    }

    @Test
    public void getFormattedDdlWorkflowWrite() {
        internalUrlTeradata.setDdlWorkflowWrite("workflowWrite %s %s");

        Assert.assertEquals(
                "workflowWrite cbs hacko",
                internalUrlTeradata.getFormattedDdlWorkflowWrite("cbs", "hacko")
        );
    }

    @Test
    public void getFormattedDdlScriptWrite() {
        internalUrlTeradata.setDdlScriptWrite("scriptWrite %s %s");

        Assert.assertEquals(
                "scriptWrite cmd_DD agreement",
                internalUrlTeradata.getFormattedDdlScriptWrite("cmd_DD", "agreement")
        );
    }


}