package cz.kb.bi.common.domain.entity.sqlTransform;

import cz.kb.bi.domain.entity.BackendMessage;
import cz.kb.bi.domain.entity.Severity;
import cz.kb.bi.domain.entity.sqlTransform.DatabaseTable;
import cz.kb.bi.domain.entity.sqlTransform.EnvironmentEnum;
import cz.kb.bi.domain.entity.sqlTransform.ScriptOperationType;
import cz.kb.bi.domain.entity.common.TextFile;
import cz.kb.bi.domain.entity.sqlTransform.TransformationEvent;
import cz.kb.bi.domain.entity.sqlTransform.TransformationForEnum;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Test of clone constructor.
 */
public class TransformationEventTest {

    private TransformationEvent originalEvent;

    @Before
    public void setUp() {
        originalEvent = new TransformationEvent();
        originalEvent.setRules(Arrays.asList("abc", "def"));
        originalEvent.setOriginalScript(createScriptList());
        originalEvent.setNewScript(createScriptList2());
        originalEvent.setScriptObjects(createTableList());
        originalEvent.setProjectSuffix("xz");
        originalEvent.setTransformationFor(TransformationForEnum.PROJECT);
        originalEvent.setEnvironmentEnum(EnvironmentEnum.PROD);
        originalEvent.setOperationType(ScriptOperationType.ANALYZE);
        originalEvent.setTransformMessages(createMessageList());
    }

    private List<TextFile> createScriptList() {
        List<TextFile> list = new ArrayList<>();
        list.add(new TextFile("script1", "blablabla"));
        return list;
    }

    private List<TextFile> createScriptList2() {
        List<TextFile> list = new ArrayList<>();
        list.add(new TextFile("script2", "blebleble"));
        return list;
    }

    private List<DatabaseTable> createTableList() {
        List<DatabaseTable> list = new ArrayList<>();
        list.add(new DatabaseTable("dat1", "x"));
        return list;
    }

    private List<BackendMessage> createMessageList() {
        List<BackendMessage> messageList = new ArrayList<>();
        messageList.add(BackendMessage.builder().summary("sum").detail("det").severity(Severity.ERROR).build());
        return messageList;
    }

    @Test
    public void testClone() {
        TransformationEvent copy = new TransformationEvent(originalEvent);
        Assert.assertThat(copy, Matchers.equalTo(originalEvent));
    }

    private BackendMessage createMessage(String summary, String detail, Severity severity) {
        BackendMessage backendMessage = new BackendMessage();
        backendMessage.setSummary(summary);
        backendMessage.setDetail(detail);
        backendMessage.setSeverity(severity);
        return backendMessage;
    }

    @Test
    public void testNull() {
        TransformationEvent original = new TransformationEvent();
        TransformationEvent copy = new TransformationEvent(original);
        Assert.assertThat(copy, Matchers.equalTo(original));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testCloneHasDifferentObjects() {
        TransformationEvent copy = new TransformationEvent(originalEvent);
        originalEvent.getOriginalScript().add(new TextFile("aa", "sss"));
        originalEvent.getNewScript().remove(0);
        originalEvent.setScriptObjects(null);
        BackendMessage change = originalEvent.getTransformMessages().get(0);
        change.setSummary("s");
        change.setDetail("d");
        change.setSeverity(Severity.FATAL);
        Assert.assertThat(copy, Matchers.allOf(
                Matchers.hasProperty("rules", Matchers.equalTo(Arrays.asList("abc", "def"))),
                Matchers.hasProperty("originalScript",
                        Matchers.contains(
                                Matchers.allOf(
                                        Matchers.hasProperty("scriptName", Matchers.equalTo("script1")),
                                        Matchers.hasProperty("scriptContent", Matchers.equalTo("blablabla"))
                                )
                        )),
                Matchers.hasProperty("newScript",
                        Matchers.contains(
                                Matchers.allOf(
                                        Matchers.hasProperty("scriptName", Matchers.equalTo("script2")),
                                        Matchers.hasProperty("scriptContent", Matchers.equalTo("blebleble"))
                                )
                        )),
                Matchers.hasProperty("scriptObjects", Matchers.equalTo(Collections.singletonList(new DatabaseTable("dat1", "x")))),
                Matchers.hasProperty("projectSuffix", Matchers.equalTo("xz")),
                Matchers.hasProperty("transformationFor", Matchers.equalTo(TransformationForEnum.PROJECT)),
                Matchers.hasProperty("environmentEnum", Matchers.equalTo(EnvironmentEnum.PROD)),
                Matchers.hasProperty("operationType", Matchers.equalTo(ScriptOperationType.ANALYZE)),
                Matchers.hasProperty("transformMessages",
                        Matchers.contains(
                                Matchers.allOf(
                                        Matchers.hasProperty("summary", Matchers.equalTo("sum")),
                                        Matchers.hasProperty("detail", Matchers.equalTo("det")),
                                        Matchers.hasProperty("severity", Matchers.equalTo(Severity.ERROR))
                                )))
        ));
    }


}
