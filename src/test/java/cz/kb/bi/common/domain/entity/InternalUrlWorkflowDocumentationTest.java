package cz.kb.bi.common.domain.entity;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class InternalUrlWorkflowDocumentationTest {


    @InjectMocks
    private InternalUrlWorkflowDocumentation workflowDocumentation;

    @Test
    public void getFormattedWorkflowDocValid() {
        workflowDocumentation.setWorkflowDocValid("wf doc %s");

        Assert.assertEquals(
                "wf doc x",
                workflowDocumentation.getFormattedWorkflowDocValid("x")
        );
    }

    @Test
    public void getFormattedWorkflowDocDeleteByKey() throws Exception {
        workflowDocumentation.setWorkflowDocDelete("delete %s");

        Assert.assertEquals(
                "delete x",
                workflowDocumentation.getFormattedWorkflowDocDeleteByKey("x")
        );
    }

    @Test
    public void getFormattedWorkflowDocFindByKey() {
        workflowDocumentation.setWorkflowDocEvent("/event/%s");
        Assert.assertEquals(
                "/event/key",
                workflowDocumentation.getFormattedWorkflowDocFindByKey("key")
        );
    }

}