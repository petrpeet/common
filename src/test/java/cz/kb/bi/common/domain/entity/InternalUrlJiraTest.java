package cz.kb.bi.common.domain.entity;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class InternalUrlJiraTest {

    @InjectMocks
    private InternalUrlJira internalUrlJira;


    @Test
    public void getFormattedSdtIssueUpdate() {
        internalUrlJira.setSdtIssue("/sdt");

        Assert.assertEquals(
                "/sdt/sdt",
                internalUrlJira.getFormattedSdtIssue("sdt")
        );
    }


    @Test
    public void getFormattedInsertComment() {
        internalUrlJira.setInsertComment("comment %s");

        Assert.assertEquals(
                "comment all",
                internalUrlJira.getFormattedInsertComment("all")
        );
    }

    @Test
    public void attachment() throws Exception {
        internalUrlJira.setAttachments("attachments/%s");

        Assert.assertEquals(
                "attachments/KEY-123",
                internalUrlJira.getFormattedAttachment("KEY-123")
        );
    }

    @Test
    public void externalId() throws Exception {
        internalUrlJira.setExternalIdUrl("external/%s/%s");

        Assert.assertEquals(
                "external/wf_test_name/1234",
                internalUrlJira.getFormattedExternalId(1234L, "wf_test_name")
        );
    }

    @Test
    public void editComment() {
        internalUrlJira.setEditComment("comment/%s/%s");

        String formattedEditComment = internalUrlJira.getFormattedEditComment("key", 123L);

        Assert.assertEquals(
                "comment/key/123",
                formattedEditComment
        );
    }
}