package cz.kb.bi.common.domain.entity;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class InternalUrlBteqTest {

    @InjectMocks
    private InternalUrlBteq internalUrlBteq;


    @Test
    public void getFormattedRunStatus() throws Exception {
        internalUrlBteq.setRunStatus("%s");
        String uuid = internalUrlBteq.getFormattedRunStatus("uuid");
        Assert.assertEquals("uuid", uuid);
    }


}