package cz.kb.bi.common.domain.entity;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.nio.file.Path;
import java.nio.file.Paths;


@RunWith(MockitoJUnitRunner.class)
public class InternalUrlScriptTest {

    @InjectMocks
    private InternalUrlScript internalUrlScript;


    @Test
    public void getFormattedScriptFullPath() {
        internalUrlScript.setScriptFullPath("scriptPath %s");

        Assert.assertEquals(
                "scriptPath name",
                internalUrlScript.getFormattedScriptFullPath("name")
        );
    }

    @Test
    public void getFormattedScriptLock() {
        internalUrlScript.setScriptLock("scriptLock %s");

        Path path = Paths.get("/");
        Assert.assertEquals(
                "scriptLock " + path,
                internalUrlScript.getFormattedScriptLock(path)
        );
    }

    @Test
    public void getFormattedScript() {
        internalUrlScript.setScript("script %s");

        Path path = Paths.get("/");

        Assert.assertEquals(
                "script " + path,
                internalUrlScript.getFormattedScript(path)
        );
    }


}