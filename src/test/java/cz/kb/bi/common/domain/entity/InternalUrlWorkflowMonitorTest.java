package cz.kb.bi.common.domain.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.LocalDateTime;
import java.util.Collections;

public class InternalUrlWorkflowMonitorTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private InternalUrlWorkflowMonitor internalUrlWorkflowMonitor;

    @Before
    public void setUp() throws Exception {
        internalUrlWorkflowMonitor = new InternalUrlWorkflowMonitor();
        internalUrlWorkflowMonitor.setWorkflowStatusEndpoints(
                Collections.singletonMap("di", "https://localhost:8443/wf-monitor-di/")
        );
    }


    @Test
    public void checkNonExistingMonitorEndpoint() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Endpoint name test does not exists.");

        internalUrlWorkflowMonitor.getFormattedWorkflowStatus(LocalDateTime.now(), "test");
    }

    @Test
    public void checkFormatOfMonitorEndpoint() throws Exception {
        Assert.assertEquals(
                "https://localhost:8443/wf-monitor-di/2010/12/31/12/59",
                internalUrlWorkflowMonitor.getFormattedWorkflowStatus(LocalDateTime.of(2010, 12, 31, 12, 59), "di")
        );
    }
}