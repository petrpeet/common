package cz.kb.bi.common.domain.entity;

import org.junit.Assert;
import org.junit.Test;

public class InternalUrlOdisTest {

    private InternalUrlOdis odis = new InternalUrlOdis();

    @Test
    public void jobRunStatus() throws Exception {
        odis.setJobRunStatus("/status/%s");

        Assert.assertEquals(
                "/status/wf_test",
                odis.getFormattedJobRunStatus("wf_test")
        );
    }
}