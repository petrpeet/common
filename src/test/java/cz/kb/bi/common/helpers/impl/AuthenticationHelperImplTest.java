package cz.kb.bi.common.helpers.impl;

import cz.kb.bi.common.domain.entity.RolesConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@RunWith(PowerMockRunner.class)
@PrepareForTest({SecurityContextHolder.class})
public class AuthenticationHelperImplTest {

    @InjectMocks
    private AuthenticationHelperImpl authenticationHelper;

    @Mock
    private SecurityContext securityContext;

    @Mock
    private Authentication authentication;

    @Mock
    private RolesConfig rolesConfig;

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(SecurityContextHolder.class);
        Mockito.when(SecurityContextHolder.getContext()).thenReturn(securityContext);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);

        Set<String> supportRoles = new TreeSet<>(Collections.singletonList("role_os"));
        Set<String> developmentRoles = new TreeSet<>(Collections.singletonList("role_dev"));

        Mockito.when(rolesConfig.getOperationSupportRoles()).thenReturn(supportRoles);
        Mockito.when(rolesConfig.getDevelopmentRoles()).thenReturn(developmentRoles);
    }

    @Test
    public void checkThatNameIsGetFromContext() throws Exception {
        Mockito.when(authentication.getName()).thenReturn("testUser");

        Assert.assertEquals("testUser", authenticationHelper.getCurrentUserName());
        Mockito.verify(authentication).getName();
    }

    @Test
    public void checkNullContextWillReturnBlankUser() throws Exception {
        Mockito.when(SecurityContextHolder.getContext()).thenReturn(null);

        Assert.assertEquals("", authenticationHelper.getCurrentUserName());
        Mockito.verifyZeroInteractions(authentication);
        Mockito.verifyZeroInteractions(securityContext);
    }


    @Test
    public void checkNullContextWillReturnFalseOperational() throws Exception {
        Mockito.when(SecurityContextHolder.getContext()).thenReturn(null);

        Assert.assertFalse(authenticationHelper.isOperationSupportMember());
        Mockito.verifyZeroInteractions(authentication);
        Mockito.verifyZeroInteractions(securityContext);
    }

    @Test
    public void checkNullAuthenticationWillReturnBlankUser() throws Exception {
        Mockito.when(securityContext.getAuthentication()).thenReturn(null);

        Assert.assertEquals("", authenticationHelper.getCurrentUserName());
        Mockito.verifyZeroInteractions(authentication);
    }

    @Test
    public void checkNullAuthenticationWillReturnFalseOperational() throws Exception {
        Mockito.when(securityContext.getAuthentication()).thenReturn(null);

        Assert.assertFalse(authenticationHelper.isOperationSupportMember());
        Mockito.verifyZeroInteractions(authentication);
    }

    @Test
    public void checkThatOperationSupportIsResolved() throws Exception {
        Mockito.when(authentication.getAuthorities())
                .thenAnswer(invocationOnMock -> grantedAuthority("role_os", "role_none"));

        Assert.assertTrue(authenticationHelper.isOperationSupportMember());
    }


    private Collection<GrantedAuthority> grantedAuthority(String... names) {
        return Stream.of(names)
                .map(s -> (GrantedAuthority) () -> s)
                .collect(Collectors.toList());
    }
}