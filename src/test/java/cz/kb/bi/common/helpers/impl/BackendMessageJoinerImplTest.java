package cz.kb.bi.common.helpers.impl;

import cz.kb.bi.domain.entity.BackendMessage;
import cz.kb.bi.domain.entity.Severity;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class BackendMessageJoinerImplTest {

    private BackendMessageJoinerImpl messageAggregatorService = new BackendMessageJoinerImpl();

    private BackendMessage firstMessage;

    private BackendMessage secondMessage;

    private BackendMessage thirdMessage;

    @Before
    public void setUp() throws Exception {
        firstMessage = BackendMessage.builder()
                .summary("first summary")
                .detail("first detail")
                .severity(Severity.FATAL)
                .build();

        secondMessage = BackendMessage.builder()
                .summary("second summary")
                .detail("second detail")
                .order(0)
                .severity(Severity.ERROR)
                .build();

        thirdMessage = BackendMessage.builder()
                .summary("first summary")
                .detail("third detail")
                .severity(Severity.FATAL)
                .build();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testOrderingOfUnifiedMessages() throws Exception {
        List<BackendMessage> backendMessages =
                messageAggregatorService.groupBackendMessages(Arrays.asList(firstMessage, secondMessage));

        Assert.assertThat(
                backendMessages,
                Matchers.contains(
                        Matchers.hasProperty("summary", Matchers.equalTo("second summary")),
                        Matchers.hasProperty("summary", Matchers.equalTo("first summary"))
                )
        );
    }

    @SuppressWarnings("unchecked")
    @Test
    public void checkGroupingOfSingleMessage() throws Exception {
        List<BackendMessage> backendMessages =
                messageAggregatorService.groupBackendMessages(Arrays.asList(firstMessage, thirdMessage));

        Assert.assertThat(
                backendMessages,
                Matchers.contains(
                        Matchers.allOf(
                                Matchers.hasProperty("summary", Matchers.equalTo("first summary")),
                                Matchers.hasProperty("detail", Matchers.equalTo("first detail")),
                                Matchers.hasProperty("additionalDetails", Matchers.contains("third detail")),
                                Matchers.hasProperty("severity", Matchers.equalTo(Severity.FATAL))
                        )
                )
        );
    }

    @Test
    public void checkNullMessages() throws Exception {
        List<BackendMessage> backendMessages =
                messageAggregatorService.groupBackendMessages(Arrays.asList(null, null));

        Assert.assertThat(
                backendMessages,
                Matchers.emptyIterable()
        );
    }

    @SuppressWarnings("unchecked")
    @Test
    public void complexTest() throws Exception {
        List<BackendMessage> backendMessages =
                messageAggregatorService.groupBackendMessages(
                        Arrays.asList(firstMessage, secondMessage, thirdMessage)
                );

        Assert.assertThat(
                backendMessages,
                Matchers.contains(
                        Matchers.allOf(
                                Matchers.hasProperty("summary", Matchers.equalTo("second summary")),
                                Matchers.hasProperty("detail", Matchers.equalTo("second detail")),
                                Matchers.hasProperty("additionalDetails", Matchers.emptyIterable()),
                                Matchers.hasProperty("severity", Matchers.equalTo(Severity.ERROR))
                        ),
                        Matchers.allOf(
                                Matchers.hasProperty("summary", Matchers.equalTo("first summary")),
                                Matchers.hasProperty("detail", Matchers.equalTo("first detail")),
                                Matchers.hasProperty("additionalDetails", Matchers.contains("third detail")),
                                Matchers.hasProperty("severity", Matchers.equalTo(Severity.FATAL))
                        )
                )
        );
    }
}
