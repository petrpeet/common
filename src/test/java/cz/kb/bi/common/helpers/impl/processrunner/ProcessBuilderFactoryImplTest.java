package cz.kb.bi.common.helpers.impl.processrunner;

import cz.kb.bi.common.helpers.api.processrunner.ProcessBuilderFactoryException;
import cz.kb.bi.common.utils.OSUtils;
import org.apache.commons.collections4.ListUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ProcessBuilderFactoryImpl.class, OSUtils.class})
public class ProcessBuilderFactoryImplTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private ProcessBuilderFactoryImpl builderFactory;
    @Mock
    private ProcessBuilder processBuilder;
    private List<String> commands = Arrays.asList("1", "2");
    @Mock
    private Process process;

    @Before
    public void init() throws Exception {
        PowerMockito.mockStatic(OSUtils.class);
        PowerMockito.when(OSUtils.isWindows()).thenReturn(false);

        PowerMockito.whenNew(ProcessBuilder.class).withArguments(commands).thenReturn(processBuilder);

        builderFactory = new ProcessBuilderFactoryImpl();

        PowerMockito.when(processBuilder.start()).thenReturn(process);
        PowerMockito.when(process.waitFor()).thenReturn(1);
    }


    @Test
    public void checkStartAndReturnExitCode() throws InterruptedException {
        int i = builderFactory.startAndReturnExitCode("1", "2");

        Assert.assertEquals(1, i);
    }

    @Test
    public void checkStartAndReturnInTerminal() throws Exception {
        PowerMockito.whenNew(ProcessBuilder.class).withArguments(
                ListUtils.union(
                        Arrays.asList(
                                "/bin/sh",
                                "-c"
                        ),
                        commands
                )
        ).thenReturn(processBuilder);

        int i = builderFactory.startInTerminalAndReturnExitCode("1", "2");

        Assert.assertEquals(1, i);
    }

    @Test
    public void checkStartAndReturnInTerminalWindows() throws Exception {
        PowerMockito.when(OSUtils.isWindows()).thenReturn(true);

        builderFactory = new ProcessBuilderFactoryImpl();

        PowerMockito.whenNew(ProcessBuilder.class).withArguments(
                ListUtils.union(
                        Arrays.asList(
                                "cmd",
                                "/c"
                        ),
                        commands
                )
        ).thenReturn(processBuilder);

        int i = builderFactory.startInTerminalAndReturnExitCode("1", "2");

        Assert.assertEquals(1, i);
    }

    @Test
    public void checkThrowIOException() throws Exception {
        expectedException.expect(ProcessBuilderFactoryException.class);
        expectedException.expectCause(Matchers.isA(IOException.class));

        PowerMockito.when(processBuilder.start()).thenThrow(new IOException());


        builderFactory.startAndReturnExitCode("1", "2");
    }

    @Test
    public void checkThrowInterruptedException() throws Exception {
        expectedException.expect(InterruptedException.class);

        PowerMockito.when(process.waitFor()).thenThrow(new InterruptedException());


        builderFactory.startAndReturnExitCode("1", "2");
    }

}