package cz.kb.bi.common.users;

import org.springframework.security.test.context.support.WithMockUser;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 */
@Retention(RetentionPolicy.RUNTIME)
@WithMockUser(roles = {"AI_USERS_4800_FENIX_ADMIN"})
public @interface WithAdministratorMockUser {
}
