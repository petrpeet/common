package cz.kb.bi.common.audit;

import cz.kb.bi.common.domain.entity.InternalUrl;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

public abstract class CommonAbstractAuditLogTest {

    @MockBean
    protected RestTemplate restTemplate;

    @Autowired
    private InternalUrl internalUrl;


    protected void verifyThatAuditTraceHasBeenCalled() {
        final String AUDIT_LOG_URL = internalUrl.getAuditLog();
        Mockito.verify(restTemplate, Mockito.atLeast(1))
                .postForObject(Mockito.eq(AUDIT_LOG_URL), Mockito.any(), Mockito.eq(LocalDateTime.class));
    }

}
