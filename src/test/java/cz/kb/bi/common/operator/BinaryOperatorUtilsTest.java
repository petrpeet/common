package cz.kb.bi.common.operator;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class BinaryOperatorUtilsTest {

    public Object[][] getOrParameters() {
        return new Object[][]{
                {true, true, true},
                {false, true, true},
                {true, false, true},
                {false, false, false}
        };
    }

    public Object[][] getAndParameters() {
        return new Object[][]{
                {true, true, true},
                {false, true, false},
                {true, false, false},
                {false, false, false}
        };
    }

    @Parameters(method = "getOrParameters")
    @Test
    public void checkThatOrOperatorIsCorrect(Boolean prev, Boolean next, Boolean expected) {
        Assert.assertEquals(BinaryOperatorUtils.getOrBinaryOperator().apply(prev, next), expected);
    }

    @Parameters(method = "getAndParameters")
    @Test
    public void checkThatAndOperatorIsCorrect(Boolean prev, Boolean next, Boolean expected) {
        Assert.assertEquals(BinaryOperatorUtils.getAndBinaryOperator().apply(prev, next), expected);
    }

    @Test
    public void checkThatConcatenateIsCorrect() throws Exception {
        final String prev = "test";
        final String next = "dev";

        Assert.assertEquals(
                BinaryOperatorUtils.getLineConcatenation().apply(prev, next),
                prev + "\n" + next
        );

    }

    @Test
    public void checkThatWindowsConcatenateIsCorrect() throws Exception {
        final String prev = "test";
        final String next = "dev";

        Assert.assertEquals(
                BinaryOperatorUtils.getWindowsLineConcatenation().apply(prev, next),
                prev + "\r\n" + next
        );

    }
}