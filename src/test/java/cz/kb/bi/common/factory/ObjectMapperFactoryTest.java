package cz.kb.bi.common.factory;

import org.junit.Assert;
import org.junit.Test;

public class ObjectMapperFactoryTest {

    @Test
    public void checkCreation() throws Exception {
        Assert.assertNotNull(ObjectMapperFactory.createObjectMapper());
    }

    @Test
    public void checkMapType() throws Exception {
        Assert.assertNotNull(ObjectMapperFactory.OBJECT_MAP_TYPE);
    }

    @Test
    public void checkStringMapType() throws Exception {
        Assert.assertNotNull(ObjectMapperFactory.STRING_MAP_TYPE);
    }

    @Test
    public void checkArrayType() throws Exception {
        Assert.assertNotNull(ObjectMapperFactory.STRING_MAP_TYPE);
    }
}