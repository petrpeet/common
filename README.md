# Instructions for using git for BI Fenix project

## Common Git rules

* The master branch always contains production-quality code
* The development branch always contains almost-production-quality code which will be released in next release
* Each feature is done in separate branch, which should start with following name:
    * feature/ - for features
    * release/ - for releases
    * hotfix/ - for hotfixes
* Source of new branch is usually development for features and master for hotfixes. The source branch is usually also the target branch
* Every merge back to development is done via pull requests

### Sources

* http://nvie.com/posts/a-successful-git-branching-model/
* http://www.clock.co.uk/blog/release-management-with-gitflow-the-clock-way
* https://betterexplained.com/articles/aha-moments-when-learning-git/
* https://confluence.atlassian.com/bitbucketserver046/git-resources/basic-git-commands

### Configure Git for the first time

~~~~git
git config --global user.name "Surname Name"
git config --global user.email "name_surname@kb.cz"
git config --global credential.helper wincred
~~~~

### If you have problem with https connection:

* You can try: https://wiki.kb.cz/confluence/display/DCSLINE/004+-+Clone+git+repository

* or you can try:

~~~~git
git config --global http.sslverify false
~~~~


### Working with your repository

### I just want to clone this repository

If you want to simply clone this empty repository then run this command in your terminal.

~~~~git
git clone https://git.kb.cz/scm/bisdc/bi-api.git
~~~~

### My code is already tracked by Git

If your code is already tracked by Git then set this repository as your "origin" to push to.

~~~~git
cd existing-project
git remote set-url origin https://git.kb.cz/scm/bisdc/bi-api.git
git push -u origin master
~~~~

## Common Java rules

* Every problem in Sonar should be fixed, if it is possible (eg. some coverage problems cannot be fixed). If it is not possible to fix the problem in sonar, add @SuppressWarning into code. The @Suppress warning annotation can be added only after approval from Java activity lead. 
* Every implementation must contains:
    * Unit Tests. The coverage should be 100% of business logic. Do not tests, that getters and setters work
    * Integration Tests in controller layer for whole module. Integration tests have ITTest.java suffix.
    * Security Tests in controller layer for whole module. Security tests contains only behavior with different roles. Security tests have STest.java suffix.
    * Audit Log Tests in controller layer for whole module. Audit Log tests contains only verification, that audit log end point has been called. Audit Log tests have ATest.java suffix.
    * Javadoc for all methods, even private and protected ones. Getters and setters should be done with Lombok library. If it is not possible, add javadoc as well.
    * Javadoc for all classes and interfaces.
    * In more complex domain objects, add javadoc to properties as well.
    * Rest documentation using Swagger for Controller module
    * User documentation using site plugin, if new task has some front-end
* Connections between two layers of application is done only via Interfaces. We never use direct implementation in property of class
    * This rule has two reason. 1) It is easier for testing with Mockito framework. 2) Interfaces are easier for proxying, which is used by many external Spring libraries (eg. Transaction Management) 
* Every module must be able to run (e.g. for test or demonstration purposes) in offline mode.
* Avoid gold plating
    * Adding of unneeded enhancements to a product or service in order to inflate the final cost to its buyer and the associated profit realized by its producer
* Avoid using logic in lambda function
    * Lambda function is great way how to simplified code, but debug option and stacktrace does not not properly. Due to this reason, always use method reference (eg. this::method or i -> method(i, param2) ...)
* Use Java 1.7 and 1.8 types where it is possible. Do not use old data types just because you are used to it. The only exception is when 3rd party library needs the old java type.
    * LocalDate, LocalDateTime, ZonedDateTime instead of Date
    * Path instead of File ...
* All Paths in tests and test data must be in Windows format (eg. UCN)

# Backend Module Rules

## Common module rules

Every micro service is realized by one module in Maven project. One module should always take care for one business object (e.g. Audit logs, Informatica workflows ...). The only exception is microservice for Teradata, where all objects is stored in one Module and separated by java packages. This exception is due to limitation of connections we can open on Teradata database.

## Creating new module

For creating new module, you should use ModuleStarter project in BI API. Go into folder moduleStarter and run program

~~~~
mvn exec:java -Dexec.mainClass="cz.kb.bi.starter.ModuleStarter" -Dexec.args="folderName [moduleName]"
~~~~

* Folder Name
     * Mandatory: Yes
     * Description: Name of the folder which should be created
* Module name
     * Mandatory: No
     * Description: Name of the module, which should be created. If Module Name parameter is not set by user, than name of the folder and name of the module is the same.
     
After using ModuleStarter project, you have to add module into parent pom.xml.

All basic libraries are set by default with ModuleStarter project. If you need some new, consult the libraries usage with your Java activity lead.

## Database Access Layer

For accessing database, we use MyBatis library and MyBatis Spring Boot Starter. The common usage for accessing database looks like:

~~~~java
@Mapper
public interface TestDatabaseMapper {

    @Select("select to1.Object_ID, to1.Object_Name, to1.Object_Surname, to1.Date_Birth from Test_Object to1")
    @Results(id = "testObjectResultRM", value = {
            @Result(property = "id", column = "Object_Id", id = true),
            @Result(property = "name", column = "Object_Name"),
            @Result(property = "surname", column = "Object_Surname"),
            @Result(property = "dateBirth", column = "Date_Birth", typeHandler = LocalDateTypeHandler.class),
    })
    List<TestDatabaseObject> findAll();

    @Select("select to1.Object_ID, to1.Object_Name, to1.Object_Surname, to1.Date_Birth from Test_Object to1 where to1.Object_Id = #{id}")
    @ResultMap("testObjectResultRM")
    TestDatabaseObject findById(Integer id);
}
~~~~

Annotation @Mapper is used for Spring boot to register Mapper as new Bean.  All database connections are set in application.yml file.


### Offline data

All modules must be testable in offline mode using in memory database. For this purpose, we use two files: schema.sql and data.sql stored in resources of each module. These two files are used for Integration tests and on Dev server as well.

## Service Layer

Service layer contains logic of every module. The class is annotated with @Service annotation which will automatically load the class as a Bean into Spring config. If class does not have any other configuration, than Configuration file is needed. The connection with mapper is done via @Autowired property.

~~~~java
@Service
class TestDatabaseObjectReadServiceImpl implements TestDatabaseObjectReadService {

    private final TestDatabaseMapper testDatabaseMapper;

    @Autowired
    public TestDatabaseObjectReadServiceImpl(TestDatabaseMapper testDatabaseMapper) {
        this.testDatabaseMapper = testDatabaseMapper;
    }
}
~~~~

Service Implementation class must always have package private so controller layer cannot access it directly, but via interface.
 
## Controller layer

Controller layer provides mapping from entity object stored in domain package to Data Transfer Objects (aka DTO). Every entity object must have it's DTO representation and vice versa. Even if they have same properties. For mapping object pro entity to DTO, we user MapperUtilities in Common module (explicitly Dozer library.). All DTO objects are stored in common module in package cz.kb.bi.dto.domain.* corresponding to modules.

Controller class should have @RestController annotation which will tell spring that it is REST controller. Spring will automatically create handlers for HTTP requests.

~~~~java
@RestController
@RequestMapping(value = "/api")
@Secured({"ROLE_AI_USERS_4800"})
public class TestDatabaseObjectController {

    private final TestDatabaseObjectReadService testDatabaseObjectReadService;

    @Autowired
    public TestDatabaseObjectController(TestDatabaseObjectReadService testDatabaseObjectReadService) {
        this.testDatabaseObjectReadService = testDatabaseObjectReadService;
    }

    @ApiOperation(
            value = "Find all objects",
            notes = "Find all testing object stored in database.",
            nickname = "findAll",
            tags = {"testAPI"})
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<TestDatabaseObjectDTO> findAll() {
        return MapperUtilities.map(testDatabaseObjectReadService.findAll(), TestDatabaseObjectDTO.class);
    }
}
~~~~

Controller class also provides security validation with Spring security framework via annotation @Secured (any other Spring security annotations are valid as well).

Developer documentation is done by Swagger2 library with SpringFox extension, which will automatically create Swagger documentation from @ApiDocumentation annotation.

### Endpoint

Every controller must have endpoints listed according following rules:

 * Standard micro service
      * Mustn't be with prefixed _/pub/_ nor _/internal/_ 
      * Users can access this micro service via Zuul proxy server
      * Authentication is required
 * Internal micro service
     * Must be with prefix _/internal/_
     * Users cannot access this micro service
     * No authentication is required
     * Accessible only from localhost
 * Public micro service
     * Must be with prefix _/pub/_
     * Users can access this micro service
     * No authentication is required
     * Use only as a last solution, because we have no audit trace from this micro service

## HTTP method for REST API

| HTTP Verb | CRUD | Entire Collection (e.g. /customers) | Specific Item (e.g. /customers/{id}) |
| --- | --- | --- | ---  |
| POST | Create | 201 (Created), 'Location' header with link to /customers/{id} containing new ID. | 404 (Not Found), 409 (Conflict) if resource already exists.. |
| GET | Read | 200 (OK), list of customers. Use pagination, sorting and filtering to navigate big lists. | 200 (OK), single customer. 404 (Not Found), if ID not found or invalid. |
| PUT | Update/Replace | 404 (Not Found), unless you want to update/replace every resource in the entire collection. | 200 (OK) or 204 (No Content). 404 (Not Found), if ID not found or invalid. |
| PATCH | Update/Modify | 404 (Not Found), unless you want to modify the collection itself. | 200 (OK) or 204 (No Content). 404 (Not Found), if ID not found or invalid. |
| DELETE | Delete | 404 (Not Found), unless you want to delete the whole collection—not often desirable. | 200 (OK). 404 (Not Found), if ID not found or invalid. |

 The HTTP method and CRUD were taken from http://www.restapitutorial.com/lessons/httpmethods.html

## Running micro service on local machine

 Running micro service on local machine is possible, but you have to follow these rules:
 
 * Comment in pom.xml provided in tomcat starter.  
 ~~~~
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-tomcat</artifactId>
    <scope>provided</scope>
</dependency>
 ~~~~
 * Add property bi.api.user.details.url to https://fenix.ds.kb.cz:8443/proxy/user
 * Start the application
 
 Never forget to return back provided scope before merge request. 
 
## Proxy

* https://github.com/Netflix/zuul
* https://www.slideshare.net/MikeyCohen1/zuul-netflix-springone-platform
* http://www.baeldung.com/spring-rest-with-zuul-proxy

![Proxy architecture](./resources/images/proxy.png)

For accessing every micro service, we use Zuul proxy server. Every user request must go through Zuul proxy. 
* If the micro service is registered in Zuul proxy, than it is external micro service which can be used by every user in KB (if he has appropriate rights). On picture above you can see, than services A and B are external micro services.
* If the micro service is not registered in Zuul proxy, than it is internal micro service. Internal micro service can only be used by other micro services, but not directly by user. On picture above C, D and E are internal micro services.  

## OAuth2

* https://www.digitalocean.com/community/tutorials/an-introduction-to-oauth-2#grant-type-authorization-code
 
![Oauth2 validation workflow](./resources/images/oauth2.png)

### Servers

* Dev server: https://fenix.ds.kb.cz:8443/proxy/oauth
   * User:password
      * user:user (With role ROLE_USER)
      * admin:admin (With role ROLE_ADMIN)
      * dev:dev (With role ROLE_AI_USERS_4800)
      * os:os (With role ROLE_AI_USERS_4800_DIC)
* Prod server: https://fenix.ds.kb.cz/proxy/oauth
    * Not online right now
    * Connected to real LDAP  

### Endpoints
* Authorization: /oauth/authorize
    * Parameters
        * response_type=code
        * client_id={client id from auth server}
        * redirect_uri={loopback url to application}
* Token: /oauth/token
    * Parameters:
        * client_id={client id from auth server}
        * redirect_uri={loopback url to application}
        * code={autorization code value}
        * grant_type= authorization_code
* Update access token by refresh token: /oauth/token
    * Parameters:
        * client_id={client id from auth server}
        * redirect_uri={loopback url to application}
        * refresh_token={value of refresh token}
        * grant_type=refresh_token

### Resource application.yml settings
~~~~yaml
security:
    oauth2:
        resource:
            user-info-uri: ${bi.api.user.details.url}
        client:
            client-authentication-scheme: query
            authentication-scheme: query
~~~~

# Jenkins jobs

## Jenkins deploy job

 During deployment on Tomcat servers, development and production, we use shell script to determine, which modules should be deployed. Modules are determined by following command
~~~~bash
git diff --name-only ${GIT_COMMIT} ${GIT_PREVIOUS_COMMIT}  | sed -E 's/\/.*//' | sed -E '/moduleStarter/d' | sed -E '/\S+\.\S+/d' | sort | uniq | tr '\n' ','
~~~~

* git diff --name-only ${GIT_COMMIT} ${GIT_PREVIOUS_COMMIT}
    * Get all files changed between current commit and previous commit
* sed -E 's/\/.*//'
    * Remove everything after first /
    * That that only root folders/files are in result of file
* sed -E '/moduleStarter/d'
    * Remove moduleStarter from result
    * Module started does not belong to BI API and should never be in result
*  sed -E '/\S+\.\S+/d'
    * Remove all files (something with extension)
* sort
    * Sort the input
    * This step is needed for next step
* uniq
    * Make all entries unique
* tr '\n' ','
    * Remove all end lines for comma, which is used for module separation in maven
    
## Jenkins quick deploy job

 Only team leader can deploy directly on Tomcat servers. All other developers should not do it, but in rare case (eg. developing of frontend and need to do some fixes), developer can deploy to tomcat indirectly. If you need to deploy into Tomcat server during development, tag your commit with following syntax
 
~~~~bash
dev-{module}-{yyyyMMdd}{N}
~~~~

* dev- 
    * prefix for all tags that indicates what should be deployed
* {module}
    * replace this part for name of the module, which you wish to redeploy
* {yyyyMMdd}
    * current timestamp
* {N}
    * Sequence number in case, that you will need more redeploys during one day
    * If this tag is first one in the day, you can omit this part